# DZG Study List Exchange

## Preview deploys

Current preview deploys can be found here: https://idcohorts.gitlab.io/DZG-IT/dzg-study-list-exchange/

## data_pipeline

The backend aka data_pipeline uses [pydantic](https://docs.pydantic.dev/) so that manual JSON validation is not necessary.
There are currently 2 schema files in `schemas`.

1. DZG.json

   This is our format schema

2. MDS.json

   This is the nfdi4health schema file (no idea why it is named `MDS.json`. This is how they named it).

To generate pydantic models, run:
```
docker-compose --profile codegen up dzg-codegen
docker-compose --profile codegen up nfdi4health-codegen
```

To run the tests locally, run:
```
docker-compose --profile tests up --build
```
