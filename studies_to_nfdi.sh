#!/bin/bash

# add timestamp
timestamp=`date "+%Y-%m-%d %H:%M:%S"`
(echo "Date of last conversion: $timestamp"; echo "") >> error_log.txt
# Loop through all files in the /source/ directory
for file in studies/*; do
  # Check if it is a file (not a directory)
  if [ -f "$file" ]; then
    echo "Converting file: $file"
    filename=$(basename "$file")
    data-pipeline nfdi4health $file "gammabox/$filename" --pretty >>error_log.txt 2>&1
  fi
done