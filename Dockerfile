ARG test_base_image=build

# do not use alpine here because alpine uses musl and pip will compile everything from source
# which takes AGES
FROM debian:bookworm as build

# Install Debian packages
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y --no-install-recommends \
    python3 pipx \
    nodejs npm \
    curl git rsync \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install poetry
RUN pipx install "poetry~=1.8.3"
RUN pipx inject poetry poetry-setuptools-scm-plugin
ENV PATH="/root/.local/bin:$PATH"

# Install yarn
RUN npm i -g corepack
RUN corepack prepare yarn@stable --activate

WORKDIR /opt/dzg-study-list-exchange

# install yarn dependencies
COPY app/ /opt/dzg-study-list-exchange/app/
RUN cd app/ && yarn install --immutable

COPY . .

# Install Python packages
RUN poetry install --without dev --sync
ENV PATH="/opt/dzg-study-list-exchange/.venv/bin:$PATH"

# Generate version information file
RUN python -m setuptools_scm --force-write-version-files

FROM ${test_base_image} as unit-tests

# run tests
RUN pytest --cov=data_pipeline


FROM ${test_base_image} as app-tests

RUN cd app/ && yarn test


FROM ${test_base_image} as integration-tests

RUN data-pipeline dzg exampleDZG example/chatgpt-nonsense.xlsx example/studies/
RUN app/build.sh example/studies/ example/site/


FROM build as final
