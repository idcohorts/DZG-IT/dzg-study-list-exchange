#!/bin/sh -ex


poetry run data-pipeline dzg DKTK tables/DKTK/*.xlsx studies/
#poetry run data-pipeline dzg DZD tables/DZD/*.xlsx studies/
poetry run data-pipeline dzg DZHK tables/DZHK/*.xlsx studies/
#poetry run data-pipeline dzg DZIF tables/DZIF/*.xlsx studies/DZIF/
poetry run data-pipeline dzg DZL tables/DZL/*.xlsx studies/
#poetry run data-pipeline dzg DZNE tables/DZNE/*.xlsx studies/DZNE/
#poetry run data-pipeline dzg DZPG tables/DZPG/*.xlsx studies/DZPG/
