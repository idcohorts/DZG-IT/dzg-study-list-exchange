import pandas as pd 
import os
import uuid
from yaml import safe_load

# specify file
with open("./betabox/local.yaml") as f:
    local_vars = safe_load(f)
    
input_path = local_vars["path_data"]
input_suffix = ".xlsx"
output_suffix = ".json"

slot_dict = {
    "Studienregisternummer" : "id",
    "Titel" : "title",
    "Ansprechpartner:in" : "contacts",
    "E-Mail Kontakt Ansprechpartner:innen" : "emails",
    "Status" : "status",
    "Start der Studie" : "date_start",
    "Veröffentlichung in einem DZG Register" : "publish",
    "Einschlusskriterien" : "inclusion_criteria",
    "Ausschlusskriterien" : "exclusion_criteria"    
} 
# "Weiterführende Links" : "external_links"

def generate_uuid():
    return str(uuid.uuid4())

def exception():
    raise Exception(f"Studienregisternummer is empty")

def prepare_file(df, filename, validate_id=False):
    print("Preparing: " + filename + "...")
    # find line starting with "Studienregisternummer"
    first_col = df[df.columns[0]]
    start_ind = first_col[first_col.astype(str).str.startswith("Studienregistern")].index[0]

    # debug
    # print("Index: ", start_ind)

    # delete obsolete rows and reset index
    df = df.iloc[start_ind:]
    df.reset_index(drop=True, inplace=True)

    # set first row as header, drop index row
    df.columns = df.iloc[0]
    df = df[1:]

    df.rename(columns=lambda x: x.strip(), inplace=True)
    df.rename(columns=slot_dict, inplace=True)

    if validate_id:
        # raise Exception if study is missing id
        df['id'] = df['id'].apply(lambda x: exception() if pd.isnull(x) else x)
    else:
        # generate uuid for studies without id
        df['id'] = df['id'].apply(lambda x: generate_uuid() if pd.isnull(x) else x)

    # remove example rows
    df = df[~df.id.str.startswith("XYZ123")]  
    df['title'] = df['title'].apply(lambda x: "" if pd.isnull(x) else x)
    df = df[~df.title.str.startswith("Mustertitel")]

    # debug
    # print(df)
    return df


def prepare_data(sourcedir, outdir, validate_id):
    file_list = []
    [file_list.append(file) for file in os.listdir(sourcedir) if file.endswith(".xlsx")]

    for file in file_list:
        
        filepath = sourcedir + file
        df = pd.read_excel(filepath, header=None)
    
        # clean data
        data = prepare_file(df, file, validate_id)

        # dump to json
        output_path = outdir + file.split(".")[0] + output_suffix
        data.to_json(output_path, orient='records', index=False, indent=2) 


#TODO 
# uuid bei leeren ids -> DONE
# musterzeilen entfernen -> DONE
# direkt zu json konvertieren, im besten fall direkt mit info über dzg -> DONE, dzg info wird in aggregator.py hinzugefügt
# evtl mittels python class (https://linkml.io/linkml/generators/python.html)
# model anpassen: source soll name und pfad der datei sein