from yaml import safe_load

with open("./conversions/local.yaml") as f:
    local_vars = safe_load(f)

path_data = local_vars["path_data"]
dzg = "DKTK"
file_xlsx = dzg + ".xlsx"
file_csv = dzg + ".csv"

full_path = path_data + "/" + file_xlsx

command = f"linkml-convert '{full_path}' -f csv -t json -s data_pipeline/schema/DZG.yaml -C Container -S Study -o conversions/" + dzg + ".json"

print(command)