from linkml.validator import validate_file
from conversions.command_printer import dzg

filename = "conversions/" + dzg + ".csv"
schema = "data_pipeline/schema/DZG.yaml"
report = validate_file(filename, schema)

if not report.results:
    print('The instance is valid!')
else:
    for result in report.results:
        print(result.message)