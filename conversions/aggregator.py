from json import load, dump
import os

wd = "./conversions/"


dzg_list = ["DKTK", "DZHK", "DZL", "DZD", "DZIF", "DZNE", "DZPG", "DZKJ"]

def add_source(outfile):
    file_list = []
    [file_list.append(file) for file in os.listdir(wd) if file.endswith(".json")]

    aggregated_data = {}

    # Option 1: No parent key for each dzg: uncomment following line
    aggregated_data["studies"] = []

    for file in file_list:
        # debug
        print(file)

        # check if any dzg is in filename and add to data if so    
        for dzg in dzg_list:
            if dzg in file:
                with open(wd + file) as f:
                    data = load(f)

                for entry in data:
                    entry["source"] = dzg

                    # Option 1: No parent key for each dzg: deactivate following two lines and replace line "aggregated_data[dzg].append(entry)" with:
                    # aggregated_data["studies"].append(entry)
                    # if dzg not in aggregated_data:
                    #     aggregated_data[dzg] = []

                    aggregated_data["studies"].append(entry)

                with open(wd + file, 'w', encoding='utf-8') as x:
                    dump(data, x, ensure_ascii=False, indent=2)

    with open(wd + outfile, 'w', encoding='utf-8') as r:
        dump(aggregated_data, r, ensure_ascii=False, indent=2)

    print("--- Donezo! ---")
    