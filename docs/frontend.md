# DZG Study List Exchange – Frontend

## Adding a new DZG

Configuration files, themes and additional content for different DZGs live in the `app/themes` directory. To add a new DZG, you should create a new directory in `app/themes` with the name of your DZG. This directory should contain the following files:

- `config.json`: The configuration file for your DZG. See the [Configuration section](#configuration) for more information.
- `theme.scss`: The SCSS file for customizing the Bulma theme. See the [Styling section](#styling) for more information.
- `content/`: A directory for custom Markdown content files. In most cases, you want to provide at least a `home.md` and an `impressum.md` file. See the [Custom pages section](#custom-pages) for more information.
- `components/` (optional): A directory for custom Vue components. See the [Layout section](#layout) for more information.

The active DZG is selected by setting the `VITE_THEME` environment variable to the name of the DZG directory. During development, you can do this by creating a `.env` file in the root of the project with the following content:

```env
VITE_THEME=my-dzg
```

The documentation will be updated on how to deploy the frontend with different DZGs.

## Configuration

The main configuration file for the frontend will be imported from `<THEME_DIR>/config.json`. The file format is documented through a JSON schema file at `app/config.schema.json`.

Once you have added a configuration file for your DZG, you should add a test case for your file to `app/tests/validateSchema.spec.js`. Then, you can run `yarn test` to validate your configuration file. You can also run `yarn generate-schema-markdown` to generate a markdown version of the JSON schema.

The primary purpose of the configuration file is to specify how to query study JSON files and how to display their information in the frontend. This is generally facilitated through JMESPath queries. The following sections describe the configuration options in detail.

### Section `general`

The `general` section contains general configuration options like the site name.

### Section `data`

Some data in the JSON file needs to be parsed to be useful, specifically dates. The `data` section contains configuration options for parsing data. Currently, only dates are supported. In the future, custom parsers could be implemented to allow for more complex data transformations.

### Section `list`

The `list` section contains configuration options for the study list page. The study list is the main page of the frontend. It displays a list of studies and allows the user to filter the studies. Refer to the JSON schema for a detailed description of the configuration options.

Some filters like the `enum` filter and the table column configurations allow for the configuration of using specific components to render their contents. See the section on [Custom components](#custom-components) for more information.

### Section `detail`

The `detail` section contains configuration options for the study detail page. The study detail page displays detailed information about a study. Refer to the JSON schema for a detailed description of the configuration options.

This section also has some options for using custom components. See the section on [Custom components](#custom-components) for more information.

## Customization

Besides configuring how the data is accessed, displayed and filtered, it is also possible to apply some basic style and layout customizations. These files should be provided in your theme directory.

### Styling

The frontend uses [Bulma](https://versions.bulma.io/0.9.4/) as the CSS framework, configurable through Sass/SCSS variables. You can customize the styling like colors and fonts by overriding any Bulma variables through the file at `<THEME_DIR>/theme.scss`. You can find a list of all available variables in the [Bulma documentation](https://versions.bulma.io/0.9.4/documentation/customize/variables/).

Custom fonts are loaded via [Nuxt Fonts](https://fonts.nuxt.com/). By default, simply including a reference to the font in your `theme.scss` file will load the font from Google Fonts, cache it locally and redistribute it to the client. You can also load fonts from other sources (including local fonts) by following the [Nuxt Fonts documentation](https://fonts.nuxt.com/).

### Layout

The frontend uses [Vue.js](https://vuejs.org/) as the JavaScript framework. You can override the entire page layout by placing your custom components in the `<THEME_DIR>/components` subdirectory. The following components are available for customization:

- `Logo.vue`: The logo component. It is used by the default header component.
- `Layout.vue`: The main layout component. It is used by the main `app.vue` file and needs to provide three slots:
  - `header`: The header of the page.
  - Default slot: The main content of the page.
  - `footer`: The footer of the page.
- `Header.vue`: The header component to be mounted to the `header` slot of the layout component.
- `Footer.vue`: The footer component to be mounted to the `footer` slot of the layout component.
- `Homepage.vue`: The homepage component to be mounted to the default slot of the layout component. By default, it renders the contents of `<THEME_DIR>/content/home.md`.

### Custom pages

You can easily add simple custom pages by creating Markdown ([or other](https://content.nuxtjs.org/guide/writing/markdown)) files in the `<THEME_DIR>/content` directory. These are rendered by [Nuxt Content](https://content.nuxtjs.org/) using their name as the route. For example, a file `<THEME_DIR>/content/foobar.md` will be rendered at `/foobar`.

There are two default pages you should provide if you use the default layout components:

- `home.md`: This file is used to render the home page. See the section on [Homepage](#homepage) for more information.
- `impressum.md`: This file is used to render the "Impressum" page. It is linked in the footer of the default layout.

The resulting markdown is styled using Bulma classes using best effort.

### Homepage

The home page is a special page that is displayed when the user navigates to the root URL of the frontend. The home page is a Vue component that is mounted to the default slot of the layout component. By default, it renders the contents of `<THEME_DIR>/content/home.md`. You can customize it by editing this `home.md` file, or by providing a custom `Homepage.vue` component in the `<THEME_DIR>/components` directory if you need more extensive customization.

### Custom components

As mentioned in the [Configuration section](#configuration), you can sometimes use specific components in the study list and study detail pages to render data from the study JSON files. This is done by specifying the component name in the configuration file. By default, there are two components available:

- `DateDisplay.vue`: Displays a date in a human-readable format.
- `StatusTag.vue`: A tag that displays the `status` of a study.

You can create additional components and place them in the `<THEME_DIR>/components` directory. Then, you can use them in the configuration file by specifying the component name without the `.vue` extension.

A custom display component should receive a single `value` prop and display it in some way. The `value` prop will be the result of the JMESpath query for that item as defined in the configuration file.
