#!/bin/bash
set -e

data-pipeline dzpg-load-from-redcap tables/DZPG/api-response.json
data-pipeline dzpg tables/DZPG/api-response.json studies/
app/build.sh studies/ site/
