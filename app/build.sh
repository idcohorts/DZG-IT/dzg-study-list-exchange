#!/bin/bash

# exit on error
set -e

# get the studies json files directory
STUDIES_DIR="$(realpath "$1")"

# get the target directory
TARGET_DIR="$(realpath "$2")"

# get app directory and change to it
DIR="$( dirname "$(realpath "$0")" )"
echo "Changing to $DIR"
cd "$DIR"

# copy the studies json files to the content directory
echo "Copying studies json files to content directory"
cp -r "$STUDIES_DIR"/* content/studies/

# generate the static website
echo "Generating static website"
yarn generate

# copy the generated files to the target directory
echo "Copying generated files to $TARGET_DIR"
mkdir -p "$TARGET_DIR"
cp -a .output/public/* "$TARGET_DIR"

# remove the studies json files from the content directory and delete the output directory
echo "Cleaning up"
rm -rf content/studies/* .output
