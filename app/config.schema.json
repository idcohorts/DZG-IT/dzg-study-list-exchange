{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "title": "Main configuration file schema",
  "description": "This schema describes the structure of the main configuration file 'dzg.config.json'. It is used to validate the configuration file and to document the available settings.",
  "type": "object",
  "properties": {
    "general": {
      "type": "object",
      "description": "General, sitewide settings.",
      "properties": {
        "name_short": {
          "type": "string",
          "description": "Short name of the institution."
        },
        "name_long": {
          "type": "string",
          "description": "Long name of the institution."
        }
      },
      "required": ["name_short", "name_long"]
    },
    "data": {
      "type": "object",
      "description": "Data processing related settings.",
      "properties": {
        "parse_fields": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "from_path": {
                "type": "string",
                "description": "Path to the field to be parsed."
              },
              "to_path": {
                "type": "string",
                "description": "Path to the field to be created."
              },
              "type": {
                "type": "string",
                "enum": ["date"],
                "description": "Type of the parser to use. Currently only 'date' is supported."
              }
            },
            "required": ["from_path", "to_path", "type"]
          }
        }
      },
      "required": ["parse_fields"]
    },
    "list": {
      "type": "object",
      "description": "Settings for the study list view.",
      "properties": {
        "sort": {
          "type": "array",
          "description": "Default sort order for the study list.",
          "items": {
            "type": "object",
            "patternProperties": {
              "^.*$": {
                "description": "Sort by field. Key must be a valid path. Value must be -1 for descending or 1 for ascending order.",
                "type": "integer",
                "enum": [-1, 1]
              }
            }
          }
        },
        "filters": {
          "type": "array",
          "description": "List of filters to be displayed.",
          "items": {
            "oneOf": [
              {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the filter."
                  },
                  "type": {
                    "const": "text",
                    "description": "Type of the filter."
                  },
                  "threshold": {
                    "type": "string",
                    "description": "Threshold for the match sorter. See https://github.com/kentcdodds/match-sorter?tab=readme-ov-file#threshold-number",
                    "enum": [
                      "CASE_SENSITIVE_EQUAL",
                      "EQUAL",
                      "STARTS_WITH",
                      "WORD_STARTS_WITH",
                      "CONTAINS",
                      "ACRONYM",
                      "MATCHES",
                      "NO_MATCH"
                    ],
                    "default": "MATCHES"
                  },
                  "placeholder": {
                    "type": "string",
                    "description": "Placeholder text for the filter input."
                  },
                  "paths": {
                    "type": "array",
                    "description": "List of paths to the fields to be filtered.",
                    "items": {
                      "type": "string"
                    }
                  }
                },
                "required": ["label", "type", "placeholder", "paths"],
                "additionalProperties": false
              },
              {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the filter."
                  },
                  "type": {
                    "const": "enum",
                    "description": "Type of the filter."
                  },
                  "path": {
                    "type": "string",
                    "description": "Path to the field to be filtered."
                  },
                  "component": {
                    "type": "string",
                    "description": "Name of the component to be used to render the filter options."
                  }
                },
                "required": ["label", "type", "path"],
                "additionalProperties": false
              },
              {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the filter."
                  },
                  "type": {
                    "const": "enum_multiple",
                    "description": "Type of the filter."
                  },
                  "path": {
                    "type": "string",
                    "description": "Path to the field to be filtered. Should be an array."
                  },
                  "component": {
                    "type": "string",
                    "description": "Name of the component to be used to render the filter options."
                  }
                },
                "required": ["label", "type", "path"],
                "additionalProperties": false
              },
              {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the filter."
                  },
                  "type": {
                    "const": "date_range",
                    "description": "Type of the filter."
                  },
                  "path": {
                    "type": "string",
                    "description": "Path to the field to be filtered."
                  }
                },
                "required": ["label", "type", "path"],
                "additionalProperties": false
              },
              {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the filter."
                  },
                  "type": {
                    "const": "multi_enum",
                    "description": "Type of the filter."
                  },
                  "fields": {
                    "type": "array",
                    "description": "List of the fields to be filtered. Can be a mix of arrays and single values.",
                    "items": {
                      "type": "object",
                      "properties": {
                        "label": {
                          "type": "string",
                          "description": "Label of the filter."
                        },
                        "path": {
                          "type": "string",
                          "description": "Path to the field to be filtered."
                        },
                        "subfields": {
                          "type": "array",
                          "description": "Dependent fields to show conditionally, based on the current selection.",
                          "items": {
                            "type": "object",
                            "properties": {
                              "if": {
                                "type": "string",
                                "description": "Value of the field to be filtered."
                              },
                              "label": {
                                "type": "string",
                                "description": "Label of the subfilter."
                              },
                              "path": {
                                "type": "string",
                                "description": "Path to the field to be filtered."
                              }
                            },
                            "required": ["if", "label", "path"],
                            "additionalProperties": false
                          }
                        }
                      },
                      "required": ["label", "path"],
                      "additionalProperties": false
                    }
                  }
                },
                "required": ["label", "type", "fields"],
                "additionalProperties": false
              },
              {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the filter."
                  },
                  "type": {
                    "const": "mesh",
                    "description": "Type of the filter."
                  },
                  "path": {
                    "type": "string",
                    "description": "Path to the field to be filtered."
                  }
                },
                "required": ["label", "type", "path"],
                "additionalProperties": false
              }
            ]
          }
        },
        "table": {
          "type": "object",
          "description": "Settings for the study list table.",
          "properties": {
            "items_per_page": {
              "type": "integer",
              "description": "Number of items per page."
            },
            "columns": {
              "type": "array",
              "description": "List of columns to be displayed.",
              "items": {
                "type": "object",
                "properties": {
                  "label": {
                    "type": "string",
                    "description": "Label of the column."
                  },
                  "path": {
                    "type": "string",
                    "description": "Path to the field to be displayed."
                  },
                  "is_link": {
                    "type": "boolean",
                    "description": "Whether the column should be a link to the study detail view.",
                    "default": false
                  },
                  "is_nowrap": {
                    "type": "boolean",
                    "description": "Whether the cell contents for this column should be able to wrap or not.",
                    "default": false
                  },
                  "component": {
                    "type": "string",
                    "description": "Name of the component to be used to render the cell content for this column."
                  }
                },
                "required": ["label", "path"]
              }
            }
          },
          "required": ["items_per_page", "columns"]
        }
      },
      "required": ["sort", "filters", "table"]
    },
    "detail": {
      "type": "object",
      "description": "Settings for the study detail view.",
      "properties": {
        "meta": {
          "type": "object",
          "description": "Settings for the page metadata.",
          "properties": {
            "title_path": {
              "type": "string",
              "description": "Path to the field to be used as the page title."
            }
          },
          "required": ["title_path"]
        },
        "head": {
          "type": "object",
          "description": "Settings for the page head. This is mostly for RDFa support.",
          "properties": {
            "htmlAttrs": {
              "type": "object",
              "description": "Attributes to be added to the html tag.",
              "patternProperties": {
                "^.*$": {
                  "type": "string"
                }
              }
            },
            "bodyAttrs": {
              "type": "object",
              "description": "Attributes to be added to the body tag.",
              "patternProperties": {
                "^.*$": {
                  "type": "string"
                }
              }
            }
          }
        },
        "heading": {
          "type": "object",
          "description": "Settings for the page heading.",
          "properties": {
            "path": {
              "type": "string",
              "description": "Path to the field to be used as the page heading."
            },
            "rdfa": {
              "type": "object",
              "description": "RDFa settings for the page heading.",
              "properties": {
                "property": {
                  "type": "string",
                  "description": "RDFa property type of the page heading."
                },
                "content": {
                  "type": "string",
                  "description": "Path to be used for the RDFa content attribute of the page heading."
                }
              },
              "required": ["property"]
            }
          },
          "required": ["path"]
        },
        "top": {
          "type": "array",
          "description": "List of elements to be displayed at the top of the page.",
          "items": {
            "type": "object",
            "properties": {
              "label": {
                "type": "string",
                "description": "Label of the element."
              },
              "path": {
                "type": "string",
                "description": "Path to the field to be displayed."
              },
              "href": {
                "type": "string",
                "description": "Path to the field to be used as the href attribute of the element. If this is set and the path is not empty, the element will be rendered as a link."
              },
              "rdfa": {
                "type": "object",
                "description": "RDFa settings for this element.",
                "properties": {
                  "property": {
                    "type": "string",
                    "description": "RDFa property type of this element."
                  },
                  "content": {
                    "type": "string",
                    "description": "Path to be used for the RDFa content attribute of this element."
                  }
                },
                "required": ["property"]
              },
              "component": {
                "type": "string",
                "description": "Name of the component to be used to render this element."
              }
            },
            "required": ["label", "path"]
          }
        },
        "panels": {
          "type": "array",
          "description": "List of panels to be displayed.",
          "items": {
            "type": "object",
            "properties": {
              "label": {
                "type": "string",
                "description": "Label of the panel."
              },
              "fields": {
                "type": "array",
                "description": "List of fields to be displayed in the panel.",
                "items": {
                  "type": "object",
                  "properties": {
                    "label": {
                      "type": "string",
                      "description": "Label of the field."
                    },
                    "path": {
                      "type": "string",
                      "description": "Path to the field to be displayed."
                    },
                    "is_list": {
                      "type": "boolean",
                      "description": "Whether the field is a list or not.",
                      "default": false
                    },
                    "type": {
                      "type": "string",
                      "enum": ["string", "email", "url"],
                      "description": "Type of the field. Emails and URLs will be rendered as a link."
                    },
                    "rdfa": {
                      "type": "object",
                      "description": "RDFa settings for this element.",
                      "properties": {
                        "property": {
                          "type": "string",
                          "description": "RDFa property type of this element."
                        },
                        "content": {
                          "type": "string",
                          "description": "Path to be used for the RDFa content attribute of this element."
                        }
                      },
                      "required": ["property"]
                    }
                  },
                  "required": ["label", "path", "type"]
                }
              }
            },
            "required": ["label", "fields"]
          }
        }
      },
      "required": ["meta", "heading", "top", "panels"]
    }
  },
  "required": ["general", "data", "list", "detail"]
}
