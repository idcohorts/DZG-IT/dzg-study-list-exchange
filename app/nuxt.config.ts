import { THEME } from "./lib/env";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  compatibilityDate: "2024-08-12",
  modules: [
    "@nuxt/content",
    "@nuxt/fonts",
    "@nuxt/image",
    "vue-history-state/nuxt",
  ],
  plugins: [{ src: "plugins/oruga.ts" }, { src: "plugins/polyfills.js" }],
  routeRules: {
    "/api/v0/**": {
      static: true,
      prerender: false,
    },
  },
  components: {
    dirs: [
      // Theme-components should override global (site) components
      {
        path: `~/themes/${THEME}/components`,
        global: true,
      },
      {
        path: "~/components/global",
        global: true,
      },
      {
        path: "~/components/site",
        global: true,
      },
      {
        path: "~/components",
        global: false,
      },
    ],
  },
  vite: {
    build: {
      target: "esnext",
    },
    optimizeDeps: {
      exclude: ["@osaris/vue-mesh-components"],
    },
    css: {
      preprocessorOptions: {
        scss: {
          // ignore mixed-decls warnings
          quietDeps: true,
          // Prepends theme-specific styles to `main.scss`
          additionalData: `@import "~/themes/${THEME}/theme.scss";`,
        },
      },
    },
  },
  content: {
    documentDriven: false,
    sources: {
      theme: {
        name: "theme",
        driver: "fs",
        base: `themes/${THEME}/content`,
      },
    },
    markdown: {
      rehypePlugins: {
        "rehype-add-classes": {
          h1: "title is-1",
          h2: "title is-2",
          h3: "title is-3",
          h4: "title is-4",
          h5: "title is-5",
          h6: "title is-6",
          p: "content",
          ul: "content",
          ol: "content",
          dl: "content",
          blockquote: "content",
          table: "content",
          tr: "content",
          td: "content",
          th: "content",
          img: "image",
        },
      },
    },
  },
  experimental: {
    payloadExtraction: false,
  },
  css: [`assets/styles/main.scss`, "@fortawesome/fontawesome-free/css/all.css"],
});
