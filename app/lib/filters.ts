// @ts-expect-error Types for jmespath are broken
import jmespath from "jmespath";
// @ts-ignore Types are currently broken in vue-mesh-components
import type { MeSHFilter } from "@osaris/vue-mesh-components";

import type { Study } from "./types";
import type { MainConfigurationFileSchema } from "~/types/config";
import { matchSorter } from "match-sorter";

export type FilterDef =
  MainConfigurationFileSchema["list"]["filters"][number] & { idx: number };

export type FilterDefDateRange = Extract<FilterDef, { type: "date_range" }>;

export type FilterDefMesh = Extract<FilterDef, { type: "mesh" }>;

export type FilterDefText = Extract<FilterDef, { type: "text" }>;

export type FilterDefEnum = Extract<FilterDef, { type: "enum" }>;

export type FilterDefEnumMultiple = Extract<
  FilterDef,
  { type: "enum_multiple" }
>;

export type FilterDefMultiEnum = Extract<FilterDef, { type: "multi_enum" }>;

class FilterBase {
  computedPre(studies: Study[]) {
    return {};
  }

  initialState(studies: Study[], computedPre: any, historyState?: any) {
    if (historyState) {
      return historyState;
    }

    return {};
  }

  computedPost(studies: Study[], state: any, computedPre: any) {
    return {};
  }

  apply(studies: Study[], state: any, computedPre: any) {
    return studies;
  }

  saveHistory(state: any) {
    return state;
  }
}

export type PrecomputedDateRange = {
  first: Date;
  last: Date;
};

export type StateDateRange = {
  min: Date;
  max: Date;
};

export type ComputedPostDateRange = {
  firstChoosable: Date;
  lastChoosable: Date;
};

export type HistoryStateDateRange = {
  min: string;
  max: string;
};

export class FilterDateRange extends FilterBase {
  constructor(public def: FilterDefDateRange) {
    super();
  }

  override computedPre(studies: Study[]): PrecomputedDateRange {
    const first = new Date(
      Math.min(
        ...studies
          ?.map((study) => jmespath.search(study, this.def.path))
          .filter(Boolean),
      ),
    );
    const last = new Date(
      Math.max(
        ...studies
          ?.map((study) => jmespath.search(study, this.def.path))
          .filter(Boolean),
      ),
    );

    return {
      first,
      last,
    };
  }

  override initialState(
    studies: Study[],
    computedPre: PrecomputedDateRange,
    historyState?: HistoryStateDateRange,
  ): StateDateRange {
    if (historyState) {
      return {
        min: new Date(historyState.min),
        max: new Date(historyState.max),
      };
    }

    return {
      min: computedPre.first,
      max: computedPre.last,
    };
  }

  override computedPost(
    studies: Study[],
    state: StateDateRange,
    computedPre: PrecomputedDateRange,
  ): ComputedPostDateRange {
    return {
      firstChoosable: new Date(
        Math.max(Number(state.min), Number(computedPre.first)),
      ),
      lastChoosable: new Date(
        Math.min(Number(state.max), Number(computedPre.last)),
      ),
    };
  }

  override apply(studies: Study[], state: StateDateRange) {
    return studies.filter((study) => {
      const value = jmespath.search(study, this.def.path) as Date;
      return !value || (value >= state.min && value <= state.max);
    });
  }

  override saveHistory(state: StateDateRange): HistoryStateDateRange {
    return {
      min: state.min.toISOString(),
      max: state.max.toISOString(),
    };
  }
}

export type StateMesh = {
  filter: MeSHFilter;
  ref: (el: any) => void;
  itemUIs: string[];
};

export type HistoryStateMesh = {
  selectedDirectly: any;
};

export class FilterMesh extends FilterBase {
  constructor(public def: FilterDefMesh) {
    super();
  }

  override initialState(
    studies: Study[],
    computedPre: any,
    historyState?: HistoryStateMesh,
  ): StateMesh {
    const filterRef = ref<MeSHFilter>(null);

    return {
      filter: filterRef,
      // This gets passed to the MeSHFilter component
      ref: (el: MeSHFilter) => {
        if (filterRef.value === el) return;

        filterRef.value = el;

        // Restore state from history once the filter is mounted
        if (el && historyState?.selectedDirectly) {
          el.meshDescriptorsSelectedDirectly = Object.values(
            historyState.selectedDirectly,
          );
        }
      },
      itemUIs: studies.map((study) => jmespath.search(study, this.def.path)),
    };
  }

  override apply(studies: Study[], state: StateMesh) {
    return studies.filter((study) => {
      const studyUIs = jmespath.search(study, this.def.path) as string[];
      return (
        state.filter === null ||
        state.filter.meshUIsSelectedTotal.size === 0 ||
        studyUIs.some((studyUI) => {
          return state.filter.meshUIsSelectedTotal.has(studyUI);
        })
      );
    });
  }

  override saveHistory(state: StateMesh) {
    return {
      selectedDirectly: state.filter?.meshDescriptorsSelectedDirectly,
    };
  }
}

export type StateText = {
  text: string;
};

export class FilterText extends FilterBase {
  constructor(public def: FilterDefText) {
    super();
  }

  override initialState(
    studies: Study[],
    computedPre: {},
    historyState?: StateText,
  ): StateText {
    return {
      text: historyState?.text ?? "",
    };
  }

  override apply(studies: Study[], state: StateText) {
    if (state.text === "") return studies;

    return matchSorter(studies, state.text, {
      keys: this.def.paths,
      threshold: matchSorter.rankings[this.def.threshold ?? "MATCHES"],
    });
  }
}

function findAvailableValuesSet(studies: Study[], path: string) {
  return studies.reduce<Set<string>>((acc, study) => {
    const value: string[] | undefined | null = jmespath.search(study, path);
    if (value === undefined || value === null) return acc;

    if (Array.isArray(value)) {
      value.forEach((val) => acc.add(val));
    } else {
      acc.add(value);
    }

    return acc;
  }, new Set<string>());
}

function findAvailableValues(studies: Study[], path: string) {
  return Array.from(findAvailableValuesSet(studies, path)).toSorted();
}

function buildInitialEnumState(studies: Study[], path: string) {
  return Object.fromEntries(
    findAvailableValues(studies, path).map((value) => [value, true]),
  );
}

export type StateEnum = {
  [key: string]: boolean;
};

export class FilterEnum extends FilterBase {
  constructor(public def: FilterDefEnum) {
    super();
  }

  override initialState(
    studies: Study[],
    computedPre: {},
    historyState?: StateEnum,
  ): StateEnum {
    return {
      ...buildInitialEnumState(studies, this.def.path),
      ...historyState,
    };
  }

  override apply(studies: Study[], state: StateEnum) {
    return studies.filter((study) => {
      if (Object.keys(state).length === 0) {
        return true;
      }

      const value: string = jmespath.search(study, this.def.path);
      return state[value];
    });
  }
}

export type StateEnumMultiple = StateEnum;

export class FilterEnumMultiple extends FilterBase {
  constructor(public def: FilterDefEnumMultiple) {
    super();
  }

  override initialState(
    studies: Study[],
    computedPre: {},
    historyState?: StateEnumMultiple,
  ): StateEnumMultiple {
    return {
      ...buildInitialEnumState(studies, this.def.path),
      ...historyState,
    };
  }

  override apply(studies: Study[], state: StateEnumMultiple) {
    return studies.filter((study) => {
      const value: string[] | undefined = jmespath.search(study, this.def.path);
      if (value === undefined || value.length === 0) return true;
      return value.some((val) => state[val]);
    });
  }
}

export type Subfield = {
  if: string;
  label: string;
  path: string;
};

export type Field = {
  label: string;
  path: string;
  subfields?: Subfield[];
};

export type PrecomputedMultiEnum = {
  fieldOptions: {
    [field: string]: string[];
  };
  allFields: (Field | Subfield)[];
};

export type StateMultiEnum = {
  configuredFields: {
    [field: string]: string[];
  };
};

export type ComputedPostMultiEnum = {
  unconfiguredFields: string[];
};

export class FilterMultiEnum extends FilterBase {
  /**
   * This filter allows to filter by multiple (enum or enum_multiple) fields at once.
   *
   * @param def The filter definition
   */

  constructor(public def: FilterDefMultiEnum) {
    super();
  }

  override computedPre(studies: Study[]): PrecomputedMultiEnum {
    const allFields = this.def.fields
      .map((field) => {
        if (field.subfields) {
          return [field, ...field.subfields];
        }
        return [field];
      })
      .flat();

    const fieldOptions = Object.fromEntries(
      allFields.map((field) => {
        const values = findAvailableValues(studies, field.path);
        return [field.label, values];
      }),
    );

    return {
      fieldOptions,
      allFields,
    };
  }

  override initialState(
    studies: Study[],
    computedPre: {},
    historyState?: StateMultiEnum,
  ): StateMultiEnum {
    return { configuredFields: {}, ...historyState };
  }

  override computedPost(
    studies: Study[],
    state: StateMultiEnum,
    computedPre: PrecomputedMultiEnum,
  ): ComputedPostMultiEnum {
    const unconfiguredFields = Array.from(
      new Set(Object.keys(computedPre.fieldOptions)).difference(
        new Set(Object.keys(state.configuredFields)),
      ),
    );

    return {
      unconfiguredFields,
    };
  }

  override apply(
    studies: Study[],
    state: StateMultiEnum,
    computedPre: PrecomputedMultiEnum,
  ): Study[] {
    return studies.filter((study) => {
      const values = computedPre.allFields.map((field) => {
        // Ignore subfields if their condition is not met
        if ("if" in field) {
          const parentField = this.def.fields.find((parentField) =>
            parentField.subfields?.includes(field),
          );
          if (!parentField) {
            return true;
          }
          if (!state.configuredFields[parentField.label]?.includes(field.if)) {
            return true;
          }
        }

        const value: string[] | undefined = jmespath.search(study, field.path);
        const selectedValues = state.configuredFields[field.label];

        // Undefined filter state means "no filter applied"
        if (selectedValues === undefined || selectedValues.length === 0)
          return true;

        // Reject studies with no value for a field, if the filter is configured
        if (value === null || value === undefined || value.length === 0)
          return false;

        if (Array.isArray(value)) {
          return value.some((val) => selectedValues.includes(val));
        } else {
          return selectedValues.includes(value);
        }
      });

      return values.every((v) => v);
    });
  }
}

type Filter =
  | FilterDateRange
  | FilterMesh
  | FilterText
  | FilterEnum
  | FilterEnumMultiple
  | FilterMultiEnum;

export function createFilter(def: FilterDef): Filter {
  switch (def.type) {
    case "date_range":
      return new FilterDateRange(def);
    case "mesh":
      return new FilterMesh(def);
    case "text":
      return new FilterText(def);
    case "enum":
      return new FilterEnum(def);
    case "enum_multiple":
      return new FilterEnumMultiple(def);
    case "multi_enum":
      return new FilterMultiEnum(def);
    default:
      // @ts-expect-error `def.type` should be `never` here
      throw new Error(`Unknown filter type: ${def.type}`);
  }
}
