import jmespath from "jmespath";

import config from "~/lib/config";
import { setValueByPath } from "./util";

export function parseFields(study) {
  for (const parseField of config.data.parse_fields) {
    switch (parseField.type) {
      case "date":
        const from_value = jmespath.search(study, parseField.from_path);

        if (from_value) {
          setValueByPath(study, parseField.to_path, new Date(from_value));
        } else {
          setValueByPath(study, parseField.to_path, null);
        }
        break;
      default:
        break;
    }
  }
}
