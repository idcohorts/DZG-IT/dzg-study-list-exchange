import type { ParsedContent } from "@nuxt/content";

export type Study = Pick<ParsedContent, any>;
