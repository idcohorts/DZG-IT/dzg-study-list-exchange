// Basic implementation of a JMESPath-esque function to set a value at a path
export function setValueByPath(obj, path, value) {
  const pathComponents = path.split(".");
  let current = obj;
  let next = null;

  for (let i = 0; i < pathComponents.length; i++) {
    const pathComponent = pathComponents[i];

    next = current[pathComponent];

    if (!next && i < pathComponents.length - 1) {
      // Create the next object if it doesn't exist
      current[pathComponent] = {};
      next = current[pathComponent];
    } else if (i === pathComponents.length - 1) {
      // Set the value at the end of the path
      current[pathComponent] = value;
    }

    current = next;
    next = null;
  }
}

export function getUserLocale() {
  if (typeof window === "undefined") return "en-US";
  return navigator.languages && navigator.languages.length
    ? navigator.languages[0]
    : navigator.language;
}
