import type { MainConfigurationFileSchema } from "~/types/config";
import { THEME } from "~/lib/env";

export const config: MainConfigurationFileSchema = await import(
  `../themes/${THEME}/config.json`
);

export default config;
