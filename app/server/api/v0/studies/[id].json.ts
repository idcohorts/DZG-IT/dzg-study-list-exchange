import { serverQueryContent } from "#content/server";

export default defineEventHandler(async (event) => {
  // Matches .../ABC.json as {"id.json": "ABC.json"}
  const studyFileName = event.context.params?.["id.json"];

  if (!studyFileName) {
    return null;
  }

  const studyId = studyFileName.replace(".json", "");

  const study = await serverQueryContent(event, `/studies`)
    .where({ id: studyId })
    .findOne();

  return study;
});
