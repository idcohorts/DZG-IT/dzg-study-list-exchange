import { serverQueryContent } from "#content/server";

export default defineEventHandler(async (event) => {
  const studies = await serverQueryContent(event, "/studies")
    .sort({ id: 1 })
    .only(["id"])
    .find();

  if (!studies) {
    return [];
  }

  return studies.map((study) => ({
    id: study.id,
    url: `studies/${study.id}.json`,
  }));
});
