<script setup lang="ts">
import { useHistoryState, onBackupState } from "vue-history-state";
// @ts-expect-error Types for jmespath are broken
import jmespath from "jmespath";

import config from "~/lib/config";
import { parseFields } from "~/lib/parsing";
import { getUserLocale } from "~/lib/util";
import {
  createFilter,
  type PrecomputedMultiEnum,
  type StateMultiEnum,
  type ComputedPostMultiEnum,
} from "~/lib/filters";

import "@osaris/vue-mesh-components/dist/vue-mesh-components.css";

const { MeSHFilter, meshDescriptorsFlat } = await import(
  // @ts-ignore Type exports are broken
  "@osaris/vue-mesh-components"
);

// Sort filters by how fast they are to apply, so that the faster ones are
// applied first and the slower ones are applied last
const FILTER_ORDER = [
  "enum",
  "enum_multiple",
  "multi_enum",
  "date_range",
  "mesh",
  "text",
];

const filtersWithIndex = config.list.filters.map((filter, idx) => ({
  ...filter,
  idx,
}));
const filters = filtersWithIndex.map((filter) => createFilter(filter));

useHead({
  title: `Study directory`,
});

const userLocale = getUserLocale();

function* getRequiredKeysFromFilters() {
  for (const filter of filters) {
    if ("path" in filter.def) {
      yield jmespath.tokenize(filter.def.path)[0].value as string;
    } else if ("paths" in filter.def) {
      yield filter.def.paths.map(
        (path) => jmespath.tokenize(path)[0].value as string,
      );
    } else if ("fields" in filter.def) {
      yield filter.def.fields.map(
        (field) => jmespath.tokenize(field.path)[0].value as string,
      );
      for (const field of filter.def.fields) {
        if ("subfields" in field && field.subfields) {
          yield field.subfields.map(
            (subfield) => jmespath.tokenize(subfield.path)[0].value as string,
          );
        }
      }
    } else {
      throw new Error("Unknown filter type");
    }
  }
}

const requiredKeys = [
  ...getRequiredKeysFromFilters(),
  config.list.table.columns.map(
    (column) => jmespath.tokenize(column.path)[0].value as string,
  ),
].flat(2);

const { data: studies } = await useAsyncData(`studies/search`, async () => {
  const content = await queryContent("studies")
    .sort(config.list.sort.reduce((acc, val) => ({ ...acc, ...val }), {}))
    .only(requiredKeys)
    .find();

  // For each field in config.list.sort, move items with empty values to the end
  config.list.sort.forEach((sort) => {
    content.sort((a, b) => {
      const path = Object.keys(sort)[0];

      const aVal = jmespath.search(a, path);
      const bVal = jmespath.search(b, path);

      if (aVal === null || aVal === undefined) return 1;
      if (bVal === null || bVal === undefined) return -1;
      return 0;
    });
  });

  return content;
});

const selectRandomItemFromArray = <T>(array: T[]): T =>
  array[Math.floor(Math.random() * array.length)];

watchEffect(() => {
  // Parse fields
  studies.value?.forEach((study) => {
    parseFields(study);
  });

  // Add random MeSH descriptors for testing
  studies.value?.forEach((study) => {
    const count = Math.floor(Math.random() * 5);
    const uids = Object.keys(meshDescriptorsFlat);

    study.meshUIs = {
      value: [],
    };
    for (let i = 0; i < count; i++) {
      study.meshUIs.value.push(selectRandomItemFromArray(uids));
    }
  });
});

const historyState = useHistoryState();

// Track this here so we can restore it with the history state
const currentPageInTable = ref(historyState.data?.currentPageInTable ?? 1);

// Data for filters required by state initialization
const filterComputedPre = computed<any[]>(() =>
  filters.map((filter) => {
    const studiesValue = studies.value;
    if (studiesValue === null) return {};
    return filter.computedPre(studiesValue);
  }),
);

// State for filters
const filterState = ref<any[]>(
  filters.map((filter, idx) => {
    // Restore state from history, if available
    if (studies.value === null) return {};

    const historyStateFilter = historyState.data?.filters?.[idx];
    const computedPre = filterComputedPre.value[idx];

    // Initialize state with defaults, or restore from history
    return filter.initialState(studies.value, computedPre, historyStateFilter);
  }),
);

// Backup filter state to history
onBackupState(() => ({
  filters: filters.map((filter, idx) => {
    return filter.saveHistory(filterState.value[idx]);
  }),
  currentPageInTable: currentPageInTable.value,
}));

// Computed data for filters calculated from state
const filterComputedPost = computed<any[]>(() =>
  filters.map((filter, idx) => {
    if (studies.value === null) return {};

    return filter.computedPost(
      studies.value,
      filterState.value[idx],
      filterComputedPre.value[idx],
    );
  }),
);

function toggleFilterEnum(idx: number, value: string) {
  filterState.value[idx][value] = !filterState.value[idx][value];
}

// Filter studies
const filteredStudies = computed(() =>
  filters
    .toSorted(
      (a, b) =>
        FILTER_ORDER.indexOf(a.def.type) - FILTER_ORDER.indexOf(b.def.type),
    )
    .reduce((studies, filter) => {
      if (studies === null) return [];
      const idx = filter.def.idx; // Restore idx from sorted filters for access to state
      return filter.apply(
        studies,
        filterState.value[idx],
        filterComputedPre.value[idx],
      );
    }, studies.value),
);
</script>

<template>
  <div>
    <h1 class="title">Study directory</h1>
    <div class="columns custom-flex-wrap">
      <client-only>
        <div
          v-for="({ def: filter }, idx) in filters"
          class="column"
          :class="{
            'is-full': filter.type === 'mesh',
            'is-two-thirds-fullhd is-full-tablet': filter.type === 'multi_enum',
            'is-half-tablet is-one-third-fullhd': ![
              'mesh',
              'multi_enum',
            ].includes(filter.type),
          }"
        >
          <div class="panel is-primary custom-panel">
            <p class="panel-heading">
              {{ filter.label }}

              <button
                v-if="filter.type === 'mesh'"
                @click="() => filterState[idx].filter.clear()"
              >
                <span class="icon">
                  <i class="fas fa-rotate-left" aria-hidden="false"></i>
                </span>
              </button>
            </p>

            <div class="panel-block">
              <div v-if="filter.type === 'text'" class="control has-icons-left">
                <input
                  class="input"
                  type="text"
                  :placeholder="filter.placeholder"
                  v-model="filterState[idx].text"
                />
                <span class="icon is-left">
                  <i class="fas fa-search" aria-hidden="true"></i>
                </span>
              </div>

              <div
                v-else-if="
                  filter.type === 'enum' || filter.type === 'enum_multiple'
                "
                class="control is-flex is-flex-wrap-wrap custom-gap"
              >
                <div
                  v-for="[key, checked] in Object.entries(filterState[idx])"
                  :key="key"
                >
                  <label class="checkbox">
                    <input
                      type="checkbox"
                      :checked="Boolean(checked)"
                      @change="toggleFilterEnum(idx, key)"
                    />
                    <component
                      v-if="filter.component"
                      :is="resolveComponent(filter.component)"
                      :value="key"
                      :class="{
                        'is-medium': ['StatusTag'].includes(filter.component),
                      }"
                    />
                    <span v-else>{{ key }}</span>
                  </label>
                </div>
              </div>

              <FilterMultiEnum
                v-else-if="filter.type === 'multi_enum'"
                :filter-fields="filter.fields"
                :filter-state="(filterState[idx] as StateMultiEnum)"
                :filter-computed-pre="(filterComputedPre[idx] as PrecomputedMultiEnum)"
                :filter-computed-post="(filterComputedPost[idx] as ComputedPostMultiEnum)"
              />

              <div v-else-if="filter.type === 'date_range'" class="columns">
                <div class="column">
                  <o-field label="From">
                    <o-datepicker
                      icon="calendar"
                      icon-right="rotate-left"
                      icon-right-clickable
                      @icon-right-click="
                        filterState[idx].min = filterComputedPre[idx].first
                      "
                      :locale="userLocale"
                      :min-date="filterComputedPre[idx].first"
                      :max-date="filterComputedPost[idx].lastChoosable"
                      v-model="filterState[idx].min"
                      :mobileNative="false"
                    >
                    </o-datepicker>
                  </o-field>
                </div>

                <div class="column">
                  <o-field label="To">
                    <o-datepicker
                      icon="calendar"
                      icon-right="rotate-left"
                      icon-right-clickable
                      @icon-right-click="
                        filterState[idx].max = filterComputedPre[idx].last
                      "
                      :locale="userLocale"
                      :min-date="filterComputedPost[idx].firstChoosable"
                      :max-date="filterComputedPre[idx].last"
                      v-model="filterState[idx].max"
                      :mobileNative="false"
                    >
                    </o-datepicker>
                  </o-field>
                </div>
              </div>

              <div
                v-if="filter.type === 'mesh'"
                class="is-flex-grow-1"
                :style="{ 'max-width': '100%' }"
              >
                <MeSHFilter
                  :ref="filterState[idx].ref"
                  lang="en"
                  :itemUIs="filterState[idx].itemUIs"
                />
              </div>
            </div>
          </div>
        </div>
      </client-only>
    </div>
    <StudyTable
      :studies="filteredStudies ?? []"
      v-model:currentPage="currentPageInTable"
    />
    <NuxtLink to="/studies/all"> List of all studies </NuxtLink>
  </div>
</template>

<style scoped lang="scss">
@import "bulma/sass/utilities/mixins.sass";

.custom-panel .panel-heading button {
  float: right;
  background: none;
  border: none;
  padding: 0;
  margin: 0;
  cursor: pointer;
  font-size: inherit;
  color: inherit;
}

.custom-flex-wrap {
  flex-wrap: wrap;
}

.custom-gap {
  gap: 0.5rem 1.5rem;
}
.custom-nowrap {
  white-space: nowrap;
}
</style>
