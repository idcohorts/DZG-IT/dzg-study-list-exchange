const parse = require("json-schema-to-markdown");

const schema = require("../config.schema.json");

const markdown = parse(schema);
process.stdout.write(markdown);
