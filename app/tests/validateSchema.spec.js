import { describe, it, expect } from "vitest";
import { Validator } from "@cfworker/json-schema";

import schema from "../config.schema.json";
import dzgConfig from "../themes/dzg/config.json";
import dzpgConfig from "../themes/dzpg/config.json";

describe("Validate DZG config.json against schema", () => {
  it("is valid", () => {
    const validator = new Validator(schema);
    const result = validator.validate(dzgConfig);

    expect(
      result.valid,
      JSON.stringify(result.errors, undefined, 2),
    ).toBeTruthy();
  });
});

describe("Validate DZPG config.json against schema", () => {
  it("is valid", () => {
    const validator = new Validator(schema);
    const result = validator.validate(dzpgConfig);

    expect(
      result.valid,
      JSON.stringify(result.errors, undefined, 2),
    ).toBeTruthy();
  });
});
