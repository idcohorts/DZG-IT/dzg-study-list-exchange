import toSorted from "array.prototype.tosorted";

export default defineNuxtPlugin((nuxtApp) => {
  toSorted.shim();

  return nuxtApp;
});
