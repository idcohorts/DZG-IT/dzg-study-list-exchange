import Oruga from "@oruga-ui/oruga-next";
import { bulmaConfig } from "@oruga-ui/theme-bulma";

const orugaConfig = {
  ...bulmaConfig,
  iconPack: "fas",
};

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.use(Oruga, orugaConfig);
});
