#!/usr/bin/env python3

import sys
import json
import jsonschema

returncode = 0


def validate(schema, entry, filename):
    try:
        jsonschema.validate(instance=entry, schema=schema)
    except jsonschema.exceptions.ValidationError as err:
        # output first line of raised error
        shorterror = str(err).split('\n')[0]
        print(filename + ": " + shorterror)
        # TODO: define some kind of verbose flag, and possibly print the whole error
        returncode = 1


"""
   check the input JSON file against the MDS schema from NFDI4Health
"""
def main():
    with open('schema/MDS.json', 'r') as schemafile:
      schema = json.load(schemafile)

    for filename in sys.argv[1:]:
        with open(filename, 'r') as datafile:
            data = json.load(datafile)

        if isinstance(data, list):
            # the MDS schema in its current incarnation only defines an individual record;
            # so loop over a top-level array and check
            lineno = 1
            for entry in data:
                validate(schema, entry, f"{filename}#{lineno}")
                lineno += 1
        else:
            validate(schema, data, filename)
    sys.exit(returncode)


if __name__ == "__main__":
    main()
