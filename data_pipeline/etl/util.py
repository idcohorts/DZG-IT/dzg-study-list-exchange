import math


def semicolon_separated(data: str):
    data = data.strip()
    if len(data) == 0:
        return []

    return list(map(lambda s: s.strip(), data.split(";")))


def split_string(string):
    for delim in ',;':
        string = string.replace(delim, '|')
    
    return string.split("|")


def highlight(string):
    return "\x1b[1;35;40m" + "\033[1m" + string + "\033[0m" + "\x1b[0m"


def is_empty(value):
    if value is None or value == '' or (isinstance(value, float) and math.isnan(value)):
        return True
    return False
