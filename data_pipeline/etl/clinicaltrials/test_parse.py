from .parse import parse_eligibility


def test_eligibility_not_specified():
    study = {}
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 0
    assert len(exclusion_criteria) == 0


def test_eligibility_empty():
    study = {"ProtocolSection": {"EligibilityModule": {"EligibilityCriteria": ""}}}
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 0
    assert len(exclusion_criteria) == 0


def test_eligibility_invalid():
    study = {
        "ProtocolSection": {
            "EligibilityModule": {"EligibilityCriteria": "der fuchs mag pilze"}
        }
    }
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 0
    assert len(exclusion_criteria) == 0


def test_eligibility_only_inclusion():
    study = {
        "ProtocolSection": {
            "EligibilityModule": {
                "EligibilityCriteria": """Inclusion criteria: test
                """
            }
        }
    }
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 0
    assert len(exclusion_criteria) == 0


def test_eligibility_simple():
    study = {
        "ProtocolSection": {
            "EligibilityModule": {
                "EligibilityCriteria": """Inclusion criteria: test
Exclusion criteria: test2
                """
            }
        }
    }
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 1
    assert inclusion_criteria[0] == "test"
    assert len(exclusion_criteria) == 1
    assert exclusion_criteria[0] == "test2"


def test_eligibility_simple_with_whitespaces():
    study = {
        "ProtocolSection": {
            "EligibilityModule": {
                "EligibilityCriteria": """

                
Inclusion criteria: test


Exclusion criteria: test2



"""
            }
        }
    }
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 1
    assert inclusion_criteria[0] == "test"
    assert len(exclusion_criteria) == 1
    assert exclusion_criteria[0] == "test2"


def test_eligibility_simple_no_colons():
    study = {
        "ProtocolSection": {
            "EligibilityModule": {
                "EligibilityCriteria": """

                
Inclusion criteria: test


Exclusion criteria.    test2



"""
            }
        }
    }
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 1
    assert inclusion_criteria[0] == "test"
    assert len(exclusion_criteria) == 1
    assert exclusion_criteria[0] == "test2"


def test_eligibility_simple_semicolon():
    study = {
        "ProtocolSection": {
            "EligibilityModule": {
                "EligibilityCriteria": """

                
Inclusion criteria: test;test3


Exclusion criteria.    test2



"""
            }
        }
    }
    inclusion_criteria, exclusion_criteria = parse_eligibility(study)
    assert len(inclusion_criteria) == 2
    assert inclusion_criteria[0] == "test"
    assert inclusion_criteria[1] == "test3"
    assert len(exclusion_criteria) == 1
    assert exclusion_criteria[0] == "test2"
