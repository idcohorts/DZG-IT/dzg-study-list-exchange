from ..util import semicolon_separated
from .jmes import *

from typing import Tuple
import regex as re
import structlog

logger = structlog.get_logger()


re_eligibility = re.compile(
    r"^\s*Inclusion Criteria[:\.]\s*(.*)\n+Exclusion Criteria[:\.]\s*(.*)",
    re.MULTILINE | re.DOTALL | re.IGNORECASE,
)


def parse_title(study) -> str:
    title = JMES_TITLE.search(study)
    if title is not None:
        return title
    return search_or_default(JMES_BRIEF_TITLE, study, "")


def parse_eligibility(study) -> Tuple[list[str], list[str]]:
    eligibility = JMES_ELIGIBILITY.search(study)
    inclusion_criteria = []
    exclusion_criteria = []
    if eligibility is None:
        return inclusion_criteria, exclusion_criteria

    result = re_eligibility.match(eligibility)
    if result is None:
        logger.warn(f"Couldn't parse eligibility: {eligibility[:32]}...")
        return inclusion_criteria, exclusion_criteria

    inclusion_criteria = semicolon_separated(result.group(1))
    exclusion_criteria = semicolon_separated(result.group(2))

    return inclusion_criteria, exclusion_criteria
