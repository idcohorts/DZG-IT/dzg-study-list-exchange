from urllib.parse import quote
from .jmes import JMES_ID
import requests
import jmespath

import structlog

logger = structlog.get_logger()


def create_study_tuple(study):
    id = JMES_ID.search(study)
    if id is None:
        logger.warn(f"clinicaltrials id not found: {study}. Ignoring")
        return None
    return id, study


def download_studies(expr):
    r = requests.get(
        f"https://clinicaltrials.gov/api/query/full_studies?fmt=JSON&expr={quote(expr)}&min_rnk=1&max_rnk=100",
    )
    r.raise_for_status()

    result = r.json()
    return filter(
        lambda study: study is not None,
        map(
            create_study_tuple,
            jmespath.search("FullStudiesResponse.FullStudies[*].Study", result),
        ),
    )
