from .jmes import *
from ..dzg import *
from .parse import *
import dateparser

CLINICAL_TRIALS_JSON_URL = (
    "https://clinicaltrials.gov/api/query/full_studies?expr={}&fmt=json"
)


def to_dzg_status(status) -> StudyStatus:
    if status is None:
        return StudyStatus.unknown
    if status == "Completed":
        return StudyStatus.closed
    elif status.startswith("Active"):
        return StudyStatus.active
    else:
        return StudyStatus.unknown


def to_dzg(study) -> Study:
    id = search_not_none(JMES_ID, study)
    central_contacts = search_or_default(JMES_CENTRAL_CONTACTS, study, [])

    contacts = list(
        map(
            lambda contact: contact["CentralContactName"],
            filter(lambda contact: "CentralContactName" in contact, central_contacts),
        )
    )
    emails = list(
        map(
            lambda contact: contact["CentralContactEMail"],
            filter(lambda contact: "CentralContactEMail" in contact, central_contacts),
        )
    )

    date_start = JMES_DATE_START.search(study)
    if date_start is not None:
        date_start = dateparser.parse(
            date_start, locales=["en"], settings={"PREFER_DAY_OF_MONTH": "first"}
        )

    inclusion_criteria, exclusion_criteria = parse_eligibility(study)

    result = create_study(
        id=id,
        title=create_value(Title, parse_title(study)),
        contacts=create_value(Contacts, contacts),
        emails=create_value(Emails, emails),
        status=create_value(Status, to_dzg_status(JMES_STATUS.search(study))),
        date_start=create_value(DateStart, date_start),
        publish=create_value(Publish, True),
        inclusion_criteria=create_value(InclusionCriteria, inclusion_criteria),
        exclusion_criteria=create_value(ExclusionCriteria, exclusion_criteria),
    )
    return result
