import jmespath

JMES_ID = jmespath.compile("ProtocolSection.IdentificationModule.NCTId")
JMES_TITLE = jmespath.compile("ProtocolSection.IdentificationModule.OfficialTitle")
JMES_BRIEF_TITLE = jmespath.compile("ProtocolSection.IdentificationModule.BriefTitle")

JMES_DATE_START = jmespath.compile(
    "ProtocolSection.StatusModule.StartDateStruct.StartDate"
)
JMES_STATUS = jmespath.compile("ProtocolSection.StatusModule.OverallStatus")
JMES_CENTRAL_CONTACTS = jmespath.compile(
    "ProtocolSection.ContactsLocationsModule.CentralContactList.CentralContact"
)
JMES_ELIGIBILITY = jmespath.compile(
    "ProtocolSection.EligibilityModule.EligibilityCriteria"
)


def search_not_none(expr, data):
    r = expr.search(data)
    if r is None:
        raise Exception(f"expression {expr} not found")
    return r


def search_or_default(expr, data, default):
    r = expr.search(data)
    return r or default
