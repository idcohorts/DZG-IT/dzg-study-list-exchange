FIELD_ID = "Studienregisternummer"
FIELD_TITLE = "Titel"
FIELD_CONTACT = "Ansprechpartner:in"
FIELD_EMAILS = "E-Mail Kontakt Ansprechpartner:innen"
FIELD_STATUS = "Status"
FIELD_DATE_START = "Start der Studie"
FIELD_PUBLISH = "Veröffentlichung in einem DZG Register"
FIELD_INCLUSION_CRITERIA = "Einschlusskriterien"
FIELD_EXCLUSION_CRITERIA = "Ausschlusskriterien"

NUM_DUMMY_ROWS = 5
