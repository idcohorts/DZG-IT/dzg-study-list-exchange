import datetime
from enum import Enum
from urllib.parse import quote


class Status(Enum):
    ACTIVE = 1
    CLOSED = 2
    UNKNOWN = 3


class DzgXlsStudy:
    def __init__(
        self,
        id: str = "",
        title: str = "",
        contacts: list[str] = [],
        emails: list[str] = [],
        status: Status = Status.UNKNOWN,
        date_start: datetime.date = datetime.date.today(),
        publish: bool = True,
        inclusion_criteria: list[str] = [],
        exclusion_criteria: list[str] = [],
    ):
        self.id = id
        self.title = title
        self.contacts = contacts
        self.emails = emails
        self.status = status
        self.date_start = date_start
        self.publish = publish
        self.inclusion_criteria = inclusion_criteria
        self.exclusion_criteria = exclusion_criteria
