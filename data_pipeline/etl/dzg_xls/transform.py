from .dzg_xls_study import DzgXlsStudy, Status as DzgXlsStatus
from ..dzg import *
from datetime import date


def to_dzg_status(status: DzgXlsStatus) -> StudyStatus:
    if status == DzgXlsStatus.ACTIVE:
        return StudyStatus.active
    elif status == DzgXlsStatus.CLOSED:
        return StudyStatus.closed
    else:
        return StudyStatus.unknown


def to_dzg(dzg_name: str, filepath: str, study: DzgXlsStudy) -> Study:
    id = study.id.strip()
    if len(id) == 0:
        raise Exception(f"Studienregisternummer is empty")

    result = create_study(
        id=id,
        title=create_value(Title, study.title),
        contacts=create_value(Contacts, study.contacts),
        emails=create_value(Emails, study.emails),
        status=create_value(Status, to_dzg_status(study.status)),
        date_start=create_value(DateStart, study.date_start),
        publish=create_value(Publish, study.publish),
        inclusion_criteria=create_value(InclusionCriteria, study.inclusion_criteria),
        exclusion_criteria=create_value(ExclusionCriteria, study.exclusion_criteria),
    )
    return result
