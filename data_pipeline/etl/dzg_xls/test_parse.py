import pytest
import pandas as pd

from .parse import *


def test_parse_single_person():
    persons = parse_person_names("Hans Meiser")
    assert len(persons) == 1
    assert persons[0] == "Hans Meiser"


def test_parse_empty_person():
    persons = parse_person_names("")
    assert len(persons) == 0


def test_parse_multiple_persons():
    persons = parse_person_names("Hans Meiser;Ulrike van der Groeben")
    assert len(persons) == 2
    assert persons[0] == "Hans Meiser"
    assert persons[1] == "Ulrike van der Groeben"


def test_parse_multiple_persons_ws_separated():
    persons = parse_person_names("Hans Meiser ; Ulrike van der Groeben")
    assert len(persons) == 2
    assert persons[0] == "Hans Meiser"
    assert persons[1] == "Ulrike van der Groeben"


def test_parse_emoji_nonsense():
    persons = parse_person_names("💥")
    assert persons[0] == "💥"
    assert len(persons) == 1


def test_parse_emails_empty():
    emails = parse_emails("")
    assert len(emails) == 0


def test_parse_emails_single():
    emails = parse_emails("hans@wurst.de")
    assert len(emails) == 1
    assert emails[0] == "hans@wurst.de"


def test_parse_emails_multi():
    emails = parse_emails("hans@wurst.de; maike@muster.de")
    assert len(emails) == 2
    assert emails[0] == "hans@wurst.de"
    assert emails[1] == "maike@muster.de"


def test_parse_emails_dangling_semicolon():
    emails = parse_emails("hans@wurst.de; maike@muster.de;")
    assert len(emails) == 2
    assert emails[0] == "hans@wurst.de"
    assert emails[1] == "maike@muster.de"


def test_parse_emails_extra_space():
    emails = parse_emails("hans@wurst.de; maike@muster.de ;")
    assert len(emails) == 2
    assert emails[0] == "hans@wurst.de"
    assert emails[1] == "maike@muster.de"


def test_parse_invalid():
    with pytest.raises(Exception):
        parse_date_start("nan")


def test_parse_invalid_number():
    with pytest.raises(Exception):
        parse_date_start(17)


def test_parse_empty_date():
    dt = parse_date_start("      ")
    assert dt is None


def test_parse_simple_date():
    dt = parse_date_start("2010-05-17")
    assert dt.year == 2010
    assert dt.month == 5
    assert dt.day == 17


def test_parse_panda_nat():
    dt = parse_date_start(pd.NaT)
    assert dt is None


def test_parse_year():
    # not sure if the int is coming from excel directly or if pandas does magic but we seem to have this case
    dt = parse_date_start(2015)
    assert dt.year == 2015
    assert dt.month == 1
    assert dt.day == 1


def test_parse_date_start_datetime():
    dt = parse_date_start(datetime.datetime.fromisoformat("2010-05-17 00:00:00"))
    assert dt.year == 2010
    assert dt.month == 5
    assert dt.day == 17


def test_parse_status_active():
    status = parse_status("aktiv")
    assert status == Status.ACTIVE


def test_parse_status_closed():
    status = parse_status("geschlossen")
    assert status == Status.CLOSED


def test_parse_status_nonsense():
    status = parse_status("bummsfallera")
    assert status == Status.UNKNOWN


def test_parse_publish_with_spaces():
    publish = " gewünscht "
    p = parse_publish(publish)
    assert p == True


def test_parse_publish_with_extra():
    publish = " gewünscht und wirklich auch richtig gut "
    p = parse_publish(publish)
    assert p == True


def test_parse_publish_upper():
    publish = "GEwünscht"
    p = parse_publish(publish)
    assert p == True


def test_parse_publish_upper():
    publish = "something"
    p = parse_publish(publish)
    assert p == False
