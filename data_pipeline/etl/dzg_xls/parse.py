import datetime
from .dzg_xls_study import DzgXlsStudy, Status
from . import table_layout
from ..util import semicolon_separated
import pandas as pd
from email.utils import parseaddr
import re

import structlog
import dateparser
import numpy as np

logger = structlog.get_logger()

expected_header = [
    table_layout.FIELD_ID,
    table_layout.FIELD_TITLE,
    table_layout.FIELD_CONTACT,
    table_layout.FIELD_EMAILS,
    table_layout.FIELD_STATUS,
    table_layout.FIELD_DATE_START,
    table_layout.FIELD_PUBLISH,
    table_layout.FIELD_INCLUSION_CRITERIA,
    table_layout.FIELD_EXCLUSION_CRITERIA,
]


def parse_person_names(s: str) -> list[str]:
    return semicolon_separated(s)


def parse_emails(emails: str) -> list[(str, str)]:
    return list(
        filter(
            lambda email: email != "",
            map(
                lambda email: email[1],
                map(
                    lambda possible_email: parseaddr(possible_email),
                    re.split("[,;]", emails),
                ),
            ),
        )
    )


def parse_date_start(date_start: str | int | datetime.datetime) -> datetime.date | None:
    if pd.isnull(date_start):
        return None

    if isinstance(date_start, datetime.datetime):
        return date_start.date()

    if isinstance(date_start, int):
        if date_start >= 1900 and date_start <= 2400:
            return datetime.date(date_start, 1, 1)
        else:
            raise Exception(f"Couldn't convert date_start: {date_start}")

    date_start = date_start.strip()
    if date_start == "":
        return None

    dt = dateparser.parse(
        date_start, locales=["en", "de"], settings={"PREFER_DAY_OF_MONTH": "first"}
    )
    if dt is None:
        raise Exception(f"Couldn't convert date_start: {date_start}")
    return dt


def parse_status(status: str) -> Status:
    if status == "aktiv":
        return Status.ACTIVE
    elif status == "geschlossen":
        return Status.CLOSED
    else:
        return Status.UNKNOWN


def parse_publish(publish: str) -> bool:
    publish = publish.strip().lower()
    if publish.startswith("gewünscht"):
        return True
    elif publish == "ok":
        return True
    else:
        logger.warn(
            f"publish has an invalid value (`{publish}`). interpreting as False"
        )
        return False


def series_to_dzg_xls(row: pd.Series) -> DzgXlsStudy:
    id = getattr(row, table_layout.FIELD_ID)
    title = getattr(row, table_layout.FIELD_TITLE)
    contacts = semicolon_separated(getattr(row, table_layout.FIELD_CONTACT))
    emails = parse_emails(getattr(row, table_layout.FIELD_EMAILS))
    status = parse_status(getattr(row, table_layout.FIELD_STATUS))
    date_start = parse_date_start(getattr(row, table_layout.FIELD_DATE_START))
    publish = parse_publish(getattr(row, table_layout.FIELD_PUBLISH))
    inclusion_criteria = semicolon_separated(
        getattr(row, table_layout.FIELD_INCLUSION_CRITERIA)
    )
    exclusion_criteria = semicolon_separated(
        getattr(row, table_layout.FIELD_EXCLUSION_CRITERIA)
    )

    study = DzgXlsStudy(
        id,
        title,
        contacts,
        emails,
        status,
        date_start,
        publish,
        inclusion_criteria,
        exclusion_criteria,
    )
    return study


def parse_dzg_xls(filepath):
    structlog.contextvars.bind_contextvars(filepath=filepath)

    df = pd.read_excel(filepath, skiprows=table_layout.NUM_DUMMY_ROWS, header=0)
    df.rename(columns={name: name.strip() for name in df.columns}, inplace=True)
    for expected_col in expected_header:
        if not expected_col in df.columns.values:
            raise Exception(f"Column `{expected_col}` not found in table")

    # remove nan values
    df.fillna("", inplace=True)
    df.apply(
        lambda series: series.replace(np.nan, ""),
        axis=1,
    )
    return df
