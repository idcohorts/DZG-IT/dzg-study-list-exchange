from collections.abc import Callable, Sequence
from enum import Enum
from typing import Any, TypeVar

from pydantic import BaseModel, ValidationError


class RedcapEnum(str, Enum):
    """
    Base class for REDCAP enums.
    An extra mapping is required to translate the response ID/key to the enum value.
    """

    __key_map__: dict

    @classmethod
    def from_key(cls, key: str) -> "RedcapEnum":
        return cls(cls.__key_map__[key])


TEnum = TypeVar("TEnum", bound="RedcapEnum")


def make_text_validator(required: bool) -> Callable[[str | None], str | None]:
    """Generates a Pydantic validator for text/notes fields."""

    def validate(v: str | None):
        if not v and not required:
            return None

        return v

    return validate


def make_bool_validator(required: bool) -> Callable[[str | None], bool | None]:
    """Generates a Pydantic validator for boolean fields."""

    def validate(v: str | None):
        if v is None and not required:
            return None

        return v == "1"

    return validate


def make_enum_validator(
    enum_class: type[TEnum],
) -> Callable[[str | None], TEnum | None]:
    """Generates a Pydantic validator for fields based on REDCAP enums."""

    def validate(v: str | None):
        if not v:
            return None

        return enum_class.from_key(v)

    return validate


TModel = TypeVar("TModel", bound=BaseModel)


def parse_optional_survey_module(
    model_class: type[TModel],
    values: dict[str, Any],
):
    """Tries to parse the values as a model class."""

    try:
        validated = model_class.validate(values)
    except ValidationError:
        return None

    # Ignore empty models
    if any(bool(value) for value in validated.dict().values()):
        return validated
    else:
        return None


def parse_enum_list(
    enum_class: type[TEnum],
    field_name: str,
    values: dict[str, str | Sequence[Enum]],
) -> Sequence[TEnum]:
    """Parsing function for fields based on REDCAP enums with multiple values."""

    results: list[TEnum] = []
    multi_fields = [
        field for field in values.keys() if field.startswith(f"{field_name}___")
    ]

    for field in multi_fields:
        value = values.pop(field, None)
        assert isinstance(
            value, str
        ), f"Value for {field} is not a string, got {type(value)}"

        if value == "1":
            key = field.partition("___")[2]
            results.append(enum_class.from_key(key))

    return results
