from typing import Any
from .dzpg_redcap_study import (
    DzpgRedcapStudy,
    TitlePi,
)


def serialize(study: DzpgRedcapStudy) -> dict[str, Any]:
    return {
        "id": study.record_id,  # Frontend requires an `id` field at the top level
        **study.dict(exclude_none=True),
        "pi_full": f"{study.title_pi.value if study.title_pi != TitlePi.OTHER else ''} {study.given_name_pi} {study.family_name_pi}".strip(),
    }
