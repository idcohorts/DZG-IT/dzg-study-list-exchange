from collections.abc import Iterable, Mapping
from dataclasses import dataclass
from typing import Any, Literal
import xml.etree.ElementTree

from pydantic import BaseModel, validator
import humps
import slugify

from ..api import get_metadata


class ColumnMetadata(BaseModel):
    field_name: str
    form_name: str
    section_header: str
    field_type: Literal[
        "text",
        "descriptive",
        "notes",
        "yesno",
        "radio",
        "dropdown",
        "checkbox",
    ]
    field_label: str
    select_choices_or_calculations: str
    field_note: str
    text_validation_type_or_show_slider_number: str
    text_validation_min: str
    text_validation_max: str
    identifier: str
    branching_logic: str
    required_field: bool
    custom_alignment: str
    question_number: str
    matrix_group_name: str
    matrix_ranking: str
    field_annotation: str

    @validator("required_field", pre=True)
    def parse_required_field(cls, v: str):
        return v.lower().strip() == "y"


def decode_choices(choices_encoded: str):
    choices_numbered = [c.partition(",") for c in choices_encoded.split("|")]
    return {c[0].strip(): c[2].strip() for c in choices_numbered}


def remove_tags(text: str):
    text = text.replace("<br>", "\n")
    try:
        return "".join(xml.etree.ElementTree.fromstring(text).itertext()).strip()
    except Exception:
        return text.strip()


@dataclass
class Column:
    name: str
    label: str
    required: bool
    form_name: str


@dataclass
class TextColumn(Column): ...


@dataclass
class BooleanColumn(Column): ...


@dataclass
class EnumColumn(Column):
    choices: dict[str, str]


@dataclass
class EnumListColumn(Column):
    choices: dict[str, str]


def enum_name(field_name: str):
    return humps.pascalize(field_name)


@dataclass
class OptionalSurveyModuleColumn(Column):
    model_name: str


def generate_validator(column: TextColumn | BooleanColumn | EnumColumn) -> str:
    match column:
        case TextColumn(name, _, required):
            return f"_parse_{name} = validator('{name}', pre=True, allow_reuse=True,)(make_text_validator({required}))\n"
        case BooleanColumn(name, _, required):
            return f"_parse_{name} = validator('{name}', pre=True, allow_reuse=True,)(make_bool_validator({required}))\n"
        case EnumColumn(name):
            return f"_parse_{name} = validator('{name}', pre=True, allow_reuse=True,)(make_enum_validator({enum_name(name)}))\n"


def build_model(name: str, columns: list[Column]):
    model_str = f"class {name}(BaseModel):\n"
    validators: list[str] = []

    for column in columns:
        required_type_str = "" if column.required else " | None"

        match column:
            case TextColumn(name, label):
                model_str += f"    {name}: str{required_type_str} = Field(description={repr(label)})\n"
                validators.append(generate_validator(column))
            case BooleanColumn(name, label):
                model_str += f"    {name}: bool{required_type_str} = Field(description={repr(label)})\n"
                validators.append(generate_validator(column))
            case EnumColumn(name, label):
                model_str += f"    {name}: {enum_name(name)}{required_type_str} = Field(description={repr(label)})\n"
                validators.append(generate_validator(column))
            case EnumListColumn(name, label):
                model_str += f"    {name}: list[{enum_name(name)}]{required_type_str} = Field(description={repr(label)})\n"
            case OptionalSurveyModuleColumn(name, label, model_name=model_name):
                model_str += f"    {name}: {model_name}{required_type_str} = Field(description={repr(label)})\n"

    model_str += "\n"

    for validator_ in validators:
        model_str += f"    {validator_}\n"

    model_str += "\n"

    model_str += generate_root_validator(columns)

    return model_str


def generate_root_validator(columns):
    val_str = "@root_validator(pre=True)\n"
    val_str += "def validate_enum_lists(cls, values):\n"

    for column in columns:
        match column:
            case EnumListColumn(name):
                val_str += f"    values['{name}'] = parse_enum_list({enum_name(name)}, '{name}', values)\n"
            case OptionalSurveyModuleColumn(name, model_name=model_name):
                val_str += f"    values['{name}'] = parse_optional_survey_module({model_name}, values)\n"

    val_str += "\n"
    val_str += "    return values\n"

    return "\n".join([f"    {line}" for line in val_str.split("\n")])


def enum_field_name(value: str):
    name = slugify.slugify(value).replace("-", "_").upper()

    if name[0].isdigit():
        name = f"_{name}"

    return name


def build_enum(field_name: str, choices: dict[str, str]):
    name = enum_name(field_name)
    enum_str = f"class {name}(RedcapEnum):\n"

    enum_str += "\n".join(
        [f"    {enum_field_name(value)} = '{value}'" for _, value in choices.items()]
    )

    enum_str += "\n\n"

    enum_str += f"    __key_map__ = {repr(choices)}"

    return enum_str


def parse_columns_from_metadata(metadata: Iterable[Mapping[str, Any]]):
    columns: list[Column] = []

    for meta_dict in metadata:
        meta = ColumnMetadata.parse_obj(meta_dict)

        label_stripped = remove_tags(meta.field_label)

        match meta.field_type:
            case "text" | "notes":
                columns.append(
                    TextColumn(
                        name=meta.field_name,
                        label=label_stripped,
                        required=meta.required_field,
                        form_name=meta.form_name,
                    )
                )
            case "yesno":
                columns.append(
                    BooleanColumn(
                        name=meta.field_name,
                        label=label_stripped,
                        required=meta.required_field,
                        form_name=meta.form_name,
                    )
                )
            case "radio" | "dropdown":
                columns.append(
                    EnumColumn(
                        name=meta.field_name,
                        label=label_stripped,
                        required=meta.required_field,
                        choices=decode_choices(meta.select_choices_or_calculations),
                        form_name=meta.form_name,
                    )
                )
            case "checkbox":
                columns.append(
                    EnumListColumn(
                        name=meta.field_name,
                        label=label_stripped,
                        required=meta.required_field,
                        choices=decode_choices(meta.select_choices_or_calculations),
                        form_name=meta.form_name,
                    )
                )

    return columns


def _filter_columns_by_form_name(columns: list[Column], form_name: str):
    return [column for column in columns if column.form_name == form_name]


def generate_models():
    metadata = get_metadata()
    columns = parse_columns_from_metadata(metadata)

    enums = []

    for column in columns:
        match column:
            case EnumColumn() | EnumListColumn():
                enums.append(build_enum(column.name, column.choices))

    MODEL_PY_IMPORTS = """
from pydantic import BaseModel, Field, validator, root_validator

from .model_helpers import RedcapEnum, make_enum_validator, parse_enum_list, make_text_validator, make_bool_validator, parse_optional_survey_module
    """.strip()

    base_columns = _filter_columns_by_form_name(columns, "kohorten_abfrage")
    imaging_columns = _filter_columns_by_form_name(columns, "imaging_modul")
    biobank_columns = _filter_columns_by_form_name(columns, "biobank")

    base_columns.append(
        OptionalSurveyModuleColumn(
            name="imaging",
            label="Imaging Module",
            required=False,
            form_name="imaging_modul",
            model_name="DzpgRedcapStudyImaging",
        )
    )
    base_columns.append(
        OptionalSurveyModuleColumn(
            name="biobank",
            label="Biobank Module",
            required=False,
            form_name="biobank",
            model_name="DzpgRedcapStudyBiobank",
        )
    )

    return "\n\n\n".join(
        [
            MODEL_PY_IMPORTS,
            *enums,
            build_model("DzpgRedcapStudyBiobank", biobank_columns),
            build_model("DzpgRedcapStudyImaging", imaging_columns),
            build_model("DzpgRedcapStudy", base_columns),
        ]
    )


def main():
    generate_models()


if __name__ == "__main__":
    main()
