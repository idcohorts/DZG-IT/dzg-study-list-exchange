from pydantic import BaseModel, Field, validator, root_validator

from .model_helpers import (
    RedcapEnum,
    make_enum_validator,
    parse_enum_list,
    make_text_validator,
    make_bool_validator,
    parse_optional_survey_module,
)


class DzpgSite(RedcapEnum):
    BERLIN_POTSDAM = "Berlin/Potsdam"
    BOCHUM_MARBURG = "Bochum/Marburg"
    JENA_MAGDEBURG_HALLE = "Jena/Magdeburg/Halle"
    MANNHEIM_HEIDELBERG_ULM = "Mannheim/Heidelberg/Ulm"
    MUNCHEN_AUGSBURG = "München/Augsburg"
    TUBINGEN = "Tübingen"
    EXTERNAL = "External"

    __key_map__ = {
        "1": "Berlin/Potsdam",
        "2": "Bochum/Marburg",
        "3": "Jena/Magdeburg/Halle",
        "4": "Mannheim/Heidelberg/Ulm",
        "5": "München/Augsburg",
        "6": "Tübingen",
        "7": "External",
    }


class TitlePi(RedcapEnum):
    MR = "Mr."
    MS = "Ms."
    DR = "Dr."
    PROF = "Prof."
    PROF_DR = "Prof. Dr."
    OTHER = "Other"

    __key_map__ = {
        "1": "Mr.",
        "2": "Ms.",
        "3": "Dr.",
        "4": "Prof.",
        "5": "Prof. Dr.",
        "6": "Other",
    }


class StudyStatus(RedcapEnum):
    AT_THE_PLANNING_STAGE = "At the planning stage"
    ONGOING_I_RECRUITMENT_ONGOING_BUT_DATA_COLLECTION_NOT_YET_STARTED = (
        "Ongoing (I): Recruitment ongoing, but data collection not yet started"
    )
    ONGOING_II_RECRUITMENT_AND_DATA_COLLECTION_ONGOING = (
        "Ongoing (II): Recruitment and data collection ongoing"
    )
    ONGOING_III_RECRUITMENT_COMPLETED_BUT_DATA_COLLECTION_ONGOING = (
        "Ongoing (III): Recruitment completed, but data collection ongoing"
    )
    ONGOING_IV_RECRUITMENT_AND_DATA_COLLECTION_COMPLETED_BUT_DATA_QUALITY_MANAGEMENT_ONGOING = "Ongoing (IV): Recruitment and data collection completed, but data quality management ongoing"
    SUSPENDED_RECRUITMENT_DATA_COLLECTION_OR_DATA_QUALITY_MANAGEMENT_HALTED = (
        "Suspended: Recruitment, data collection, or data quality management halted"
    )
    TERMINATED_RECRUITMENT_DATA_COLLECTION_DATA_AND_QUALITY_MANAGEMENT_HALTED = (
        "Terminated: Recruitment, data collection, data and quality management halted"
    )
    COMPLETED_RECRUITMENT_DATA_COLLECTION_AND_DATA_QUALITY_MANAGEMENT_COMPLETED = (
        "Completed: Recruitment, data collection, and data quality management completed"
    )
    OTHER = "Other"

    __key_map__ = {
        "1": "At the planning stage",
        "2": "Ongoing (I): Recruitment ongoing, but data collection not yet started",
        "3": "Ongoing (II): Recruitment and data collection ongoing",
        "4": "Ongoing (III): Recruitment completed, but data collection ongoing",
        "5": "Ongoing (IV): Recruitment and data collection completed, but data quality management ongoing",
        "6": "Suspended: Recruitment, data collection, or data quality management halted",
        "7": "Terminated: Recruitment, data collection, data and quality management halted",
        "8": "Completed: Recruitment, data collection, and data quality management completed",
        "9": "Other",
    }


class TypeIntervention(RedcapEnum):
    INTERVENTIONAL = "Interventional"
    NON_INTERVENTIONAL = "Non-interventional"

    __key_map__ = {"1": "Interventional", "2": "Non-interventional"}


class TypeSpecificationNonInt(RedcapEnum):
    CASE_CONTROL = "Case-control"
    NESTED_CASE_CONTROL = "Nested case-control"
    CASE_ONLY = "Case-only"
    CASE_CROSSOVER = "Case-crossover"
    ECOLOGICAL_OR_COMMUNITY_STUDIES = "Ecological or community studies"
    FAMILY_BASED = "Family-based"
    TWIN_STUDY = "Twin study"
    COHORT = "Cohort"
    CASE_COHORT = "Case-cohort"
    BIRTH_COHORT = "Birth cohort"
    TREND = "Trend"
    PANEL = "Panel"
    LONGITUDINAL = "Longitudinal"
    CROSS_SECTIONAL = "Cross-sectional"
    CROSS_SECTIONAL_AD_HOC_FOLLOW_UP = "Cross-sectional ad-hoc follow-up"
    TIME_SERIES = "Time series"
    QUALITY_CONTROL = "Quality control"
    REGISTRY = "Registry"
    OTHER = "Other"
    UNKNOWN = "Unknown"

    __key_map__ = {
        "1": "Case-control",
        "2": "Nested case-control",
        "3": "Case-only",
        "4": "Case-crossover",
        "5": "Ecological or community studies",
        "6": "Family-based",
        "7": "Twin study",
        "8": "Cohort",
        "9": "Case-cohort",
        "10": "Birth cohort",
        "11": "Trend",
        "12": "Panel",
        "13": "Longitudinal",
        "14": "Cross-sectional",
        "15": "Cross-sectional ad-hoc follow-up",
        "16": "Time series",
        "17": "Quality control",
        "18": "Registry",
        "19": "Other",
        "20": "Unknown",
    }


class TypeSpecificationInt(RedcapEnum):
    SINGLE_GROUP = "Single group"
    PARALLEL = "Parallel"
    CROSSOVER = "Crossover"
    FACTORIAL = "Factorial"
    SEQUENTIAL = "Sequential"
    OTHER = "Other"
    UNKNOWN = "Unknown"

    __key_map__ = {
        "1": "Single group",
        "2": "Parallel",
        "3": "Crossover",
        "4": "Factorial",
        "5": "Sequential",
        "6": "Other",
        "7": "Unknown",
    }


class Setting(RedcapEnum):
    MONOCENTRIC = "Monocentric"
    MULTICENTRIC = "Multicentric"

    __key_map__ = {"1": "Monocentric", "2": "Multicentric"}


class SettingCountry(RedcapEnum):
    SINGLE = "Single"
    MULTI = "Multi"

    __key_map__ = {"1": "Single", "2": "Multi"}


class Participants(RedcapEnum):
    GENERAL_POPULATION = "General population"
    PATIENTS_WITH_SPECIFIC_DISEASES = "Patients with specific diseases"

    __key_map__ = {"1": "General population", "2": "Patients with specific diseases"}


class SpecificDiseases(RedcapEnum):
    MENTAL_BEHAVIOURAL_OR_NEURODEVELOPMENTAL_DISORDERS_06 = (
        "Mental, behavioural or neurodevelopmental disorders (06)"
    )
    OTHER = "Other"
    NOT_APPLICABLE = "Not applicable"
    UNKNOWN = "Unknown"

    __key_map__ = {
        "1": "Mental, behavioural or neurodevelopmental disorders (06)",
        "2": "Other",
        "3": "Not applicable",
        "4": "Unknown",
    }


class MentalDisorders(RedcapEnum):
    NEURODEVELOPMENTAL_DISORDERS = "Neurodevelopmental disorders"
    SCHIZOPHRENIA_OR_OTHER_PRIMARY_PSYCHOTIC_DISORDERS = (
        "Schizophrenia or other primary psychotic disorders"
    )
    CATATONIA = "Catatonia"
    MOOD_DISORDERS = "Mood disorders"
    ANXIETY_OR_FEAR_RELATED_DISORDERS = "Anxiety or fear-related disorders"
    OBSESSIVE_COMPULSIVE_OR_RELATED_DISORDERS = (
        "Obsessive-compulsive or related disorders"
    )
    DISORDERS_SPECIFICALLY_ASSOCIATED_WITH_STRESS = (
        "Disorders specifically associated with stress"
    )
    DISSOCIATIVE_DISORDERS = "Dissociative disorders"
    FEEDING_OR_EATING_DISORDERS = "Feeding or eating disorders"
    ELIMINATION_DISORDERS = "Elimination disorders"
    DISORDERS_OF_BODILY_DISTRESS_OR_BODILY_EXPERIENCE = (
        "Disorders of bodily distress or bodily experience"
    )
    DISORDERS_DUE_TO_SUBSTANCE_USE_OR_ADDICTIVE_BEHAVIOURS = (
        "Disorders due to substance use or addictive behaviours"
    )
    IMPULSE_CONTROL_DISORDERS = "Impulse control disorders"
    DISRUPTIVE_BEHAVIOUR_OR_DISSOCIAL_DISORDERS = (
        "Disruptive behaviour or dissocial disorders"
    )
    PERSONALITY_DISORDERS_AND_RELATED_TRAITS = (
        "Personality disorders and related traits"
    )
    PARAPHILIC_DISORDERS = "Paraphilic disorders"
    FACTITIOUS_DISORDERS = "Factitious disorders"
    NEUROCOGNITIVE_DISORDERS = "Neurocognitive disorders"
    MENTAL_OR_BEHAVIOURAL_DISORDERS_ASSOCIATED_WITH_PREGNANCY_CHILDBIRTH_OR_THE_PUERPERIUM = "Mental or behavioural disorders associated with pregnancy, childbirth or the puerperium"
    OTHER = "Other"
    NOT_APPLICABLE = "Not applicable"
    UNKNOWN = "Unknown"

    __key_map__ = {
        "1": "Neurodevelopmental disorders",
        "2": "Schizophrenia or other primary psychotic disorders",
        "3": "Catatonia",
        "4": "Mood disorders",
        "5": "Anxiety or fear-related disorders",
        "6": "Obsessive-compulsive or related disorders",
        "7": "Disorders specifically associated with stress",
        "8": "Dissociative disorders",
        "9": "Feeding or eating disorders",
        "10": "Elimination disorders",
        "11": "Disorders of bodily distress or bodily experience",
        "12": "Disorders due to substance use or addictive behaviours",
        "13": "Impulse control disorders",
        "14": "Disruptive behaviour or dissocial disorders",
        "15": "Personality disorders and related traits",
        "16": "Paraphilic disorders",
        "17": "Factitious disorders",
        "18": "Neurocognitive disorders",
        "19": "Mental or behavioural disorders associated with pregnancy, childbirth or the puerperium",
        "20": "Other",
        "21": "Not applicable",
        "22": "Unknown",
    }


class SomaticDisorders(RedcapEnum):
    CERTAIN_INFECTIOUS_OR_PARASITIC_DISEASES_01 = (
        "Certain infectious or parasitic diseases (01)"
    )
    NEOPLASMS_02 = "Neoplasms (02)"
    DISEASES_OF_THE_BLOOD_OR_BLOOD_FORMING_ORGANS_03 = (
        "Diseases of the blood or blood-forming organs (03)"
    )
    DISEASES_OF_THE_IMMUNE_SYSTEM_04 = "Diseases of the immune system (04)"
    ENDOCRINE_NUTRITIONAL_OR_METABOLIC_DISEASES_05 = (
        "Endocrine, nutritional or metabolic diseases (05)"
    )
    SLEEP_WAKE_DISORDERS_07 = "Sleep-wake disorders (07)"
    DISEASES_OF_THE_NERVOUS_SYSTEM_08 = "Diseases of the nervous system (08)"
    DISEASES_OF_THE_VISUAL_SYSTEM_09 = "Diseases of the visual system (09)"
    DISEASES_OF_THE_EAR_OR_MASTOID_PROCESS_10 = (
        "Diseases of the ear or mastoid process (10)"
    )
    DISEASES_OF_THE_CIRCULATORY_SYSTEM_11 = "Diseases of the circulatory system (11)"
    DISEASES_OF_THE_RESPIRATORY_SYSTEM_12 = "Diseases of the respiratory system (12)"
    DISEASES_OF_THE_DIGESTIVE_SYSTEM_13 = "Diseases of the digestive system (13)"
    DISEASES_OF_THE_SKIN_14 = "Diseases of the skin (14)"
    DISEASES_OF_THE_MUSCULOSKELETAL_SYSTEM_OR_CONNECTIVE_TISSUE_15 = (
        "Diseases of the musculoskeletal system or connective tissue (15)"
    )
    DISEASES_OF_THE_GENITOURINARY_SYSTEM_16 = (
        "Diseases of the genitourinary system (16)"
    )
    CONDITIONS_RELATED_TO_SEXUAL_HEALTH_17 = "Conditions related to sexual health (17)"
    PREGNANCY_CHILDBIRTH_OR_THE_PUERPERIUM_18 = (
        "Pregnancy, childbirth or the puerperium (18)"
    )
    CERTAIN_CONDITIONS_ORIGINATING_IN_THE_PERINATAL_PERIOD_19 = (
        "Certain conditions originating in the perinatal period (19)"
    )
    DEVELOPMENTAL_ANOMALIES_20 = "Developmental anomalies (20)"
    SYMPTOMS_SIGNS_OR_CLINICAL_FINDINGS_NOT_ELSEWHERE_CLASSIFIED_21 = (
        "Symptoms, signs or clinical findings, not elsewhere classified (21)"
    )
    INJURY_POISONING_OR_CERTAIN_OTHER_CONSEQUENCES_OF_EXTERNAL_CAUSES_22 = (
        "Injury, poisoning or certain other consequences of external causes (22)"
    )
    EXTERNAL_CAUSES_OF_MORBIDITY_OR_MORTALITY_23 = (
        "External causes of morbidity or mortality (23)"
    )
    FACTORS_INFLUENCING_HEALTH_STATUS_OR_CONTACT_WITH_HEALTH_SERVICES_24 = (
        "Factors influencing health status or contact with health services (24)"
    )
    OTHER = "Other"
    NOT_APPLICABLE = "Not applicable"
    UNKNOWN = "Unknown"

    __key_map__ = {
        "1": "Certain infectious or parasitic diseases (01)",
        "2": "Neoplasms (02)",
        "3": "Diseases of the blood or blood-forming organs (03)",
        "4": "Diseases of the immune system (04)",
        "5": "Endocrine, nutritional or metabolic diseases (05)",
        "7": "Sleep-wake disorders (07)",
        "8": "Diseases of the nervous system (08)",
        "9": "Diseases of the visual system (09)",
        "10": "Diseases of the ear or mastoid process (10)",
        "11": "Diseases of the circulatory system (11)",
        "12": "Diseases of the respiratory system (12)",
        "13": "Diseases of the digestive system (13)",
        "14": "Diseases of the skin (14)",
        "15": "Diseases of the musculoskeletal system or connective tissue (15)",
        "16": "Diseases of the genitourinary system (16)",
        "17": "Conditions related to sexual health (17)",
        "18": "Pregnancy, childbirth or the puerperium (18)",
        "19": "Certain conditions originating in the perinatal period (19)",
        "20": "Developmental anomalies (20)",
        "21": "Symptoms, signs or clinical findings, not elsewhere classified (21)",
        "22": "Injury, poisoning or certain other consequences of external causes (22)",
        "23": "External causes of morbidity or mortality (23)",
        "24": "Factors influencing health status or contact with health services (24)",
        "25": "Other",
        "26": "Not applicable",
        "27": "Unknown",
    }


class Recruited(RedcapEnum):
    GENERAL_POPULATION = "General population"
    HOSPITAL = "Hospital"
    OUTPATIENT = "Outpatient"
    OTHER = "Other"

    __key_map__ = {
        "1": "General population",
        "2": "Hospital",
        "3": "Outpatient",
        "4": "Other",
    }


class DataSources(RedcapEnum):
    BIOLOGICAL_SAMPLES = "Biological samples"
    ADMINISTRATIVE_DATABASES = "Administrative databases"
    COGNITIVE_MEASUREMENTS = "Cognitive measurements"
    GENEALOGICAL_RECORDS = "Genealogical records"
    IMAGING_DATA = "Imaging data"
    MEDICAL_RECORDS = "Medical records"
    REGISTRIES = "Registries"
    INTERVIEW = "Interview"
    QUESTIONNAIRE = "Questionnaire"
    PHYSIOLOGICAL_BIOCHEMICAL_MEASUREMENTS = "Physiological/biochemical measurements"
    OMICS_TECHNOLOGY = "Omics technology"
    SMARTPHONE_OR_WEARABLE_BASED_DATA = "Smartphone or wearable based data"
    OTHER = "Other"

    __key_map__ = {
        "1": "Biological samples",
        "2": "Administrative databases",
        "3": "Cognitive measurements",
        "4": "Genealogical records",
        "5": "Imaging data",
        "6": "Medical records",
        "7": "Registries",
        "8": "Interview",
        "9": "Questionnaire",
        "10": "Physiological/biochemical measurements",
        "11": "Omics technology",
        "12": "Smartphone or wearable based data",
        "13": "Other",
    }


class DataSharing(RedcapEnum):
    YES_THERE_IS_A_PLAN_TO_MAKE_DATA_AVAILABLE = (
        "Yes, there is a plan to make data available"
    )
    NO_THERE_IS_NO_PLAN_TO_MAKE_DATA_AVAILABLE = (
        "No, there is no plan to make data available"
    )
    UNDECIDED_IT_IS_NOT_YET_KNOWN_IF_DATA_WILL_BE_MADE_AVAILABLE = (
        "Undecided, it is not yet known if data will be made available"
    )

    __key_map__ = {
        "1": "Yes, there is a plan to make data available",
        "2": "No, there is no plan to make data available",
        "3": "Undecided, it is not yet known if data will be made available",
    }


class PlannedFollowUps(RedcapEnum):
    YES = "Yes"
    NO = "No"
    NOT_PLANNED_BUT_POSSIBLE = "Not planned, but possible"

    __key_map__ = {"1": "Yes", "0": "No", "2": "Not planned, but possible"}


class ConsentRecontact(RedcapEnum):
    YES = "Yes"
    NO = "No"

    __key_map__ = {"1": "Yes", "0": "No"}


class FurtherComponents(RedcapEnum):
    YES = "Yes"
    NO = "No"

    __key_map__ = {"1": "Yes", "2": "No"}


class DataEntity(RedcapEnum):
    MRI = "MRI"
    EEG = "EEG"
    FNIRS = "fNIRS"
    PHYSIOLOGICAL_DATA = "Physiological Data"
    OTHER_MODALITIES = "Other modalities"

    __key_map__ = {
        "1": "MRI",
        "2": "EEG",
        "3": "fNIRS",
        "4": "Physiological Data",
        "5": "Other modalities",
    }


class MrN(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrScannerType1(RedcapEnum):
    SIEMENS_PRISMA = "SIEMENS PRISMA"
    SIEMENS_SKYRA = "SIEMENS Skyra"
    SIEMENS_TRIO = "SIEMENS Trio"
    SIEMENS_TERRA = "SIEMENS TERRA"
    GE = "GE"
    PHILIPPS = "Philipps"
    OTHER = "Other"

    __key_map__ = {
        "1": "SIEMENS PRISMA",
        "2": "SIEMENS Skyra",
        "3": "SIEMENS Trio",
        "4": "SIEMENS TERRA",
        "5": "GE",
        "6": "Philipps",
        "7": "Other",
    }


class MrFieldStrength1(RedcapEnum):
    _3_T = "3 T"
    _7_T = "7 T"
    _9_4_T = "9.4 T"
    OTHER = "Other"

    __key_map__ = {"1": "3 T", "2": "7 T", "3": "9.4 T", "4": "Other"}


class MrHeadCoil1(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "Other"}


class MrSequences1(RedcapEnum):
    TASKFMRI = "taskfMRI"
    T1 = "T1"
    RSFMRI = "rsfMRI"
    MRS = "MRS"
    DTI = "DTI"
    OTHER = "Other"

    __key_map__ = {
        "1": "taskfMRI",
        "2": "T1",
        "3": "rsfMRI",
        "4": "MRS",
        "5": "DTI",
        "6": "Other",
    }


class MrNTasks1(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrTaskConstruct11(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrTaskConstruct12(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrTaskConstruct13(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrPreproSoftware1(RedcapEnum):
    CAT12_VBM = "CAT12 VBM"
    SPM_VBM = "SPM VBM"
    FREESURFER = "FreeSurfer"
    FSL_FIRST = "FSL FIRST"
    BRAIN_VOYAGER = "Brain Voyager"
    MANUAL_SEGMENTATION = "Manual Segmentation"
    OTHER = "Other"

    __key_map__ = {
        "1": "CAT12 VBM",
        "2": "SPM VBM",
        "3": "FreeSurfer",
        "4": "FSL FIRST",
        "5": "Brain Voyager",
        "6": "Manual Segmentation",
        "7": "Other",
    }


class MrCondition1(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class MrRsPreproInfo1(RedcapEnum):
    FMRIPREP = "fmriprep"
    SPM = "SPM"
    CONN = "CONN"
    DPABI = "DPABI"
    HCP_PIPELINE = "HCP pipeline"
    UK_PIPELINE = "UK pipeline"
    OTHER = "Other"

    __key_map__ = {
        "1": "fmriprep",
        "2": "SPM",
        "3": "CONN",
        "4": "DPABI",
        "5": "HCP pipeline",
        "6": "UK pipeline",
        "7": "Other",
    }


class MrNVoxels1(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrMrsSequence1(RedcapEnum):
    PRESS = "PRESS"
    MEGA_PRESS = "Mega-PRESS"
    STEAM = "STEAM"
    LASER = "LASER"
    SEMI_LASER = "Semi-LASER"
    OTHER = "Other"

    __key_map__ = {
        "1": "PRESS",
        "2": "Mega-PRESS",
        "3": "STEAM",
        "4": "LASER",
        "5": "Semi-LASER",
        "6": "Other",
    }


class MrMrsPreproInfo1(RedcapEnum):
    LCMODEL = "LCModel"
    JMRUI = "jMRUI"
    TARQUIN = "Tarquin"
    VESPA = "VESPA"
    FID_A = "FID-A"
    FS_MRS = "FS-MRS"
    OTHER = "Other"

    __key_map__ = {
        "1": "LCModel",
        "2": "jMRUI",
        "3": "Tarquin",
        "4": "VESPA",
        "5": "FID-A",
        "6": "FS-MRS",
        "7": "Other",
    }


class MrDataType1(RedcapEnum):
    RAW = "Raw"
    RDA = "RDA"
    DAT = "DAT"
    OTHER = "Other"

    __key_map__ = {"1": "Raw", "2": "RDA", "3": "DAT", "4": "Other"}


class MrDistortionWhich1(RedcapEnum):
    FIELD_MAP = "field-map"
    REVERSE_PHASE_ENCODING = "reverse-phase encoding"
    OTHER = "Other"

    __key_map__ = {"1": "field-map", "2": "reverse-phase encoding", "3": "Other"}


class MrScannerType2(RedcapEnum):
    SIEMENS_PRISMA = "SIEMENS PRISMA"
    SIEMENS_SKYRA = "SIEMENS Skyra"
    SIEMENS_TRIO = "SIEMENS Trio"
    SIEMENS_TERRA = "SIEMENS TERRA"
    GE = "GE"
    PHILIPPS = "Philipps"
    OTHER = "Other"

    __key_map__ = {
        "1": "SIEMENS PRISMA",
        "2": "SIEMENS Skyra",
        "3": "SIEMENS Trio",
        "4": "SIEMENS TERRA",
        "5": "GE",
        "6": "Philipps",
        "7": "Other",
    }


class MrFieldStrength2(RedcapEnum):
    _3_T = "3 T"
    _7_T = "7 T"
    _9_4_T = "9.4 T"
    OTHER = "Other"

    __key_map__ = {"1": "3 T", "2": "7 T", "3": "9.4 T", "4": "Other"}


class MrHeadCoil2(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "Other"}


class MrSequences2(RedcapEnum):
    TASKFMRI = "taskfMRI"
    T1 = "T1"
    RSFMRI = "rsfMRI"
    MRS = "MRS"
    DTI = "DTI"
    OTHER = "Other"

    __key_map__ = {
        "1": "taskfMRI",
        "2": "T1",
        "3": "rsfMRI",
        "4": "MRS",
        "5": "DTI",
        "6": "Other",
    }


class MrNTasks2(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrTaskConstruct21(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrTaskConstruct22(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrTaskConstruct23(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrPreproSoftware2(RedcapEnum):
    CAT12_VBM = "CAT12 VBM"
    SPM_VBM = "SPM VBM"
    FREESURFER = "FreeSurfer"
    FSL_FIRST = "FSL FIRST"
    BRAIN_VOYAGER = "Brain Voyager"
    MANUAL_SEGMENTATION = "Manual Segmentation"
    OTHER = "Other"

    __key_map__ = {
        "1": "CAT12 VBM",
        "2": "SPM VBM",
        "3": "FreeSurfer",
        "4": "FSL FIRST",
        "5": "Brain Voyager",
        "6": "Manual Segmentation",
        "7": "Other",
    }


class MrCondition2(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class MrRsPreproInfo2(RedcapEnum):
    FMRIPREP = "fmriprep"
    SPM = "SPM"
    CONN = "CONN"
    DPABI = "DPABI"
    HCP_PIPELINE = "HCP pipeline"
    UK_PIPELINE = "UK pipeline"
    OTHER = "Other"

    __key_map__ = {
        "1": "fmriprep",
        "2": "SPM",
        "3": "CONN",
        "4": "DPABI",
        "5": "HCP pipeline",
        "6": "UK pipeline",
        "7": "Other",
    }


class MrNVoxels2(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrMrsSequence2(RedcapEnum):
    PRESS = "PRESS"
    MEGA_PRESS = "Mega-PRESS"
    STEAM = "STEAM"
    LASER = "LASER"
    SEMI_LASER = "Semi-LASER"
    OTHER = "Other"

    __key_map__ = {
        "1": "PRESS",
        "2": "Mega-PRESS",
        "3": "STEAM",
        "4": "LASER",
        "5": "Semi-LASER",
        "6": "Other",
    }


class MrMrsPreproInfo2(RedcapEnum):
    LCMODEL = "LCModel"
    JMRUI = "jMRUI"
    TARQUIN = "Tarquin"
    VESPA = "VESPA"
    FID_A = "FID-A"
    FS_MRS = "FS-MRS"
    OTHER = "Other"

    __key_map__ = {
        "1": "LCModel",
        "2": "jMRUI",
        "3": "Tarquin",
        "4": "VESPA",
        "5": "FID-A",
        "6": "FS-MRS",
        "7": "Other",
    }


class MrDataType2(RedcapEnum):
    RAW = "Raw"
    RDA = "RDA"
    DAT = "DAT"
    OTHER = "Other"

    __key_map__ = {"1": "Raw", "2": "RDA", "3": "DAT", "4": "Other"}


class MrDistortionWhich2(RedcapEnum):
    FIELD_MAP = "field-map"
    REVERSE_PHASE_ENCODING = "reverse-phase encoding"
    OTHER = "Other"

    __key_map__ = {"1": "field-map", "2": "reverse-phase encoding", "3": "Other"}


class MrScannerType3(RedcapEnum):
    SIEMENS_PRISMA = "SIEMENS PRISMA"
    SIEMENS_SKYRA = "SIEMENS Skyra"
    SIEMENS_TRIO = "SIEMENS Trio"
    SIEMENS_TERRA = "SIEMENS TERRA"
    GE = "GE"
    PHILIPPS = "Philipps"
    OTHER = "Other"

    __key_map__ = {
        "1": "SIEMENS PRISMA",
        "2": "SIEMENS Skyra",
        "3": "SIEMENS Trio",
        "4": "SIEMENS TERRA",
        "5": "GE",
        "6": "Philipps",
        "7": "Other",
    }


class MrFieldStrength3(RedcapEnum):
    _3_T = "3 T"
    _7_T = "7 T"
    _9_4_T = "9.4 T"
    OTHER = "Other"

    __key_map__ = {"1": "3 T", "2": "7 T", "3": "9.4 T", "4": "Other"}


class MrHeadCoil3(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "Other"}


class MrSequences3(RedcapEnum):
    TASKFMRI = "taskfMRI"
    T1 = "T1"
    RSFMRI = "rsfMRI"
    MRS = "MRS"
    DTI = "DTI"
    OTHER = "Other"

    __key_map__ = {
        "1": "taskfMRI",
        "2": "T1",
        "3": "rsfMRI",
        "4": "MRS",
        "5": "DTI",
        "6": "Other",
    }


class MrNTasks3(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrTaskConstruct31(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrTaskConstruct32(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrTaskConstruct33(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class MrPreproSoftware3(RedcapEnum):
    CAT12_VBM = "CAT12 VBM"
    SPM_VBM = "SPM VBM"
    FREESURFER = "FreeSurfer"
    FSL_FIRST = "FSL FIRST"
    BRAIN_VOYAGER = "Brain Voyager"
    MANUAL_SEGMENTATION = "Manual Segmentation"
    OTHER = "Other"

    __key_map__ = {
        "1": "CAT12 VBM",
        "2": "SPM VBM",
        "3": "FreeSurfer",
        "4": "FSL FIRST",
        "5": "Brain Voyager",
        "6": "Manual Segmentation",
        "7": "Other",
    }


class MrCondition3(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class MrRsPreproInfo3(RedcapEnum):
    FMRIPREP = "fmriprep"
    SPM = "SPM"
    CONN = "CONN"
    DPABI = "DPABI"
    HCP_PIPELINE = "HCP pipeline"
    UK_PIPELINE = "UK pipeline"
    OTHER = "Other"

    __key_map__ = {
        "1": "fmriprep",
        "2": "SPM",
        "3": "CONN",
        "4": "DPABI",
        "5": "HCP pipeline",
        "6": "UK pipeline",
        "7": "Other",
    }


class MrNVoxels3(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class MrMrsSequence3(RedcapEnum):
    PRESS = "PRESS"
    MEGA_PRESS = "Mega-PRESS"
    STEAM = "STEAM"
    LASER = "LASER"
    SEMI_LASER = "Semi-LASER"
    OTHER = "Other"

    __key_map__ = {
        "1": "PRESS",
        "2": "Mega-PRESS",
        "3": "STEAM",
        "4": "LASER",
        "5": "Semi-LASER",
        "6": "Other",
    }


class MrMrsPreproInfo3(RedcapEnum):
    LCMODEL = "LCModel"
    JMRUI = "jMRUI"
    TARQUIN = "Tarquin"
    VESPA = "VESPA"
    FID_A = "FID-A"
    FS_MRS = "FS-MRS"
    OTHER = "Other"

    __key_map__ = {
        "1": "LCModel",
        "2": "jMRUI",
        "3": "Tarquin",
        "4": "VESPA",
        "5": "FID-A",
        "6": "FS-MRS",
        "7": "Other",
    }


class MrDataType3(RedcapEnum):
    RAW = "Raw"
    RDA = "RDA"
    DAT = "DAT"
    OTHER = "Other"

    __key_map__ = {"1": "Raw", "2": "RDA", "3": "DAT", "4": "Other"}


class MrDistortionWhich3(RedcapEnum):
    FIELD_MAP = "field-map"
    REVERSE_PHASE_ENCODING = "reverse-phase encoding"
    OTHER = "Other"

    __key_map__ = {"1": "field-map", "2": "reverse-phase encoding", "3": "Other"}


class EegN(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class EegNChannels1(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    _128 = "128"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "128", "5": "Other"}


class EegElectrodes1(RedcapEnum):
    ACTIVE = "active"
    PASSIVE = "passive"
    SPONGE = "sponge"
    DRY = "dry"
    OTHER = "other"

    __key_map__ = {
        "1": "active",
        "2": "passive",
        "3": "sponge",
        "4": "dry",
        "5": "other",
    }


class EegParadigm1(RedcapEnum):
    TASK = "task"
    RESTING_STATE = "resting-state"

    __key_map__ = {"1": "task", "2": "resting-state"}


class EegCondition1(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class EegNTasks1(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class EegTaskConstruct11(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegTaskConstruct12(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegTaskConstruct13(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegPreproSoftware1(RedcapEnum):
    BRAIN_VISION_ANALYZER = "Brain Vision Analyzer"
    NEUROSCAN = "Neuroscan"
    EEGLAB = "EEGLab"
    OTHER = "Other"

    __key_map__ = {
        "1": "Brain Vision Analyzer",
        "2": "Neuroscan",
        "3": "EEGLab",
        "4": "Other",
    }


class EegNChannels2(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    _128 = "128"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "128", "5": "Other"}


class EegElectrodes2(RedcapEnum):
    ACTIVE = "active"
    PASSIVE = "passive"
    SPONGE = "sponge"
    DRY = "dry"
    OTHER = "other"

    __key_map__ = {
        "1": "active",
        "2": "passive",
        "3": "sponge",
        "4": "dry",
        "5": "other",
    }


class EegParadigm2(RedcapEnum):
    TASK = "task"
    RESTING_STATE = "resting-state"

    __key_map__ = {"1": "task", "2": "resting-state"}


class EegCondition2(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class EegNTasks2(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class EegTaskConstruct21(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegTaskConstruct22(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegTaskConstruct23(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegPreproSoftware2(RedcapEnum):
    BRAIN_VISION_ANALYZER = "Brain Vision Analyzer"
    NEUROSCAN = "Neuroscan"
    EEGLAB = "EEGLab"
    OTHER = "Other"

    __key_map__ = {
        "1": "Brain Vision Analyzer",
        "2": "Neuroscan",
        "3": "EEGLab",
        "4": "Other",
    }


class EegNChannels3(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    _128 = "128"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "128", "5": "Other"}


class EegElectrodes3(RedcapEnum):
    ACTIVE = "active"
    PASSIVE = "passive"
    SPONGE = "sponge"
    DRY = "dry"
    OTHER = "other"

    __key_map__ = {
        "1": "active",
        "2": "passive",
        "3": "sponge",
        "4": "dry",
        "5": "other",
    }


class EegParadigm3(RedcapEnum):
    TASK = "task"
    RESTING_STATE = "resting-state"

    __key_map__ = {"1": "task", "2": "resting-state"}


class EegCondition3(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class EegNTasks3(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class EegTaskConstruct31(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegTaskConstruct32(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegTaskConstruct33(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class EegPreproSoftware3(RedcapEnum):
    BRAIN_VISION_ANALYZER = "Brain Vision Analyzer"
    NEUROSCAN = "Neuroscan"
    EEGLAB = "EEGLab"
    OTHER = "Other"

    __key_map__ = {
        "1": "Brain Vision Analyzer",
        "2": "Neuroscan",
        "3": "EEGLab",
        "4": "Other",
    }


class FnirsN(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class FnirsNChannels1(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    _128 = "128"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "128", "5": "Other"}


class FnirsParadigm1(RedcapEnum):
    TASK = "task"
    RESTING_STATE = "resting-state"

    __key_map__ = {"1": "task", "2": "resting-state"}


class FnirsCondition1(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class FnirsNTasks1(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class FnirsTaskConstruct11(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsTaskConstruct12(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsTaskConstruct13(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsPreproSoftware1(RedcapEnum):
    NIRS_TOOLBOX = "NIRS Toolbox"
    HOMER2 = "Homer2"
    HOMER3 = "Homer3"
    NIRSLAB = "nirsLAB"
    OTHER = "Other"

    __key_map__ = {
        "1": "NIRS Toolbox",
        "2": "Homer2",
        "3": "Homer3",
        "4": "nirsLAB",
        "5": "Other",
    }


class FnirsNChannels2(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    _128 = "128"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "128", "5": "Other"}


class FnirsParadigm2(RedcapEnum):
    TASK = "task"
    RESTING_STATE = "resting-state"

    __key_map__ = {"1": "task", "2": "resting-state"}


class FnirsCondition2(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class FnirsNTasks2(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class FnirsTaskConstruct21(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsTaskConstruct22(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsTaskConstruct23(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsPreproSoftware2(RedcapEnum):
    NIRS_TOOLBOX = "NIRS Toolbox"
    HOMER2 = "Homer2"
    HOMER3 = "Homer3"
    NIRSLAB = "nirsLAB"
    OTHER = "Other"

    __key_map__ = {
        "1": "NIRS Toolbox",
        "2": "Homer2",
        "3": "Homer3",
        "4": "nirsLAB",
        "5": "Other",
    }


class FnirsNChannels3(RedcapEnum):
    _16 = "16"
    _32 = "32"
    _64 = "64"
    _128 = "128"
    OTHER = "Other"

    __key_map__ = {"1": "16", "2": "32", "3": "64", "4": "128", "5": "Other"}


class FnirsParadigm3(RedcapEnum):
    TASK = "task"
    RESTING_STATE = "resting-state"

    __key_map__ = {"1": "task", "2": "resting-state"}


class FnirsCondition3(RedcapEnum):
    EYES_OPEN = "Eyes open"
    EYES_CLOSED = "Eyes closed"
    FIXATION_CROSS = "Fixation cross"
    OTHER = "Other"

    __key_map__ = {
        "1": "Eyes open",
        "2": "Eyes closed",
        "3": "Fixation cross",
        "4": "Other",
    }


class FnirsNTasks3(RedcapEnum):
    _1 = "1"
    MORE = "more"

    __key_map__ = {"1": "1", "2": "more"}


class FnirsTaskConstruct31(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsTaskConstruct32(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsTaskConstruct33(RedcapEnum):
    REWARD_PROCESSING = "Reward processing"
    EMOTION_PROCESSING = "Emotion processing"
    COGNITIVE_TASK = "Cognitive task"
    OTHER = "Other"

    __key_map__ = {
        "1": "Reward processing",
        "2": "Emotion processing",
        "3": "Cognitive task",
        "4": "Other",
    }


class FnirsPreproSoftware3(RedcapEnum):
    NIRS_TOOLBOX = "NIRS Toolbox"
    HOMER2 = "Homer2"
    HOMER3 = "Homer3"
    NIRSLAB = "nirsLAB"
    OTHER = "Other"

    __key_map__ = {
        "1": "NIRS Toolbox",
        "2": "Homer2",
        "3": "Homer3",
        "4": "nirsLAB",
        "5": "Other",
    }


class PhysModality(RedcapEnum):
    PPG = "PPG"
    ECG = "ECG"
    RESPIRATION = "Respiration"
    EDA = "EDA"
    OTHER = "Other"

    __key_map__ = {"1": "PPG", "2": "ECG", "3": "Respiration", "4": "EDA", "5": "Other"}


class BiobankMaterial(RedcapEnum):
    BLOOD = "Blood"
    PBMC = "PBMC"
    URIN = "Urin"
    SALIVA = "Saliva"
    OTHER = "Other"

    __key_map__ = {"1": "Blood", "2": "PBMC", "3": "Urin", "4": "Saliva", "5": "Other"}


class BiobankBlood(RedcapEnum):
    SERUM = "Serum"
    EDTA = "EDTA"
    PAXGENE_TEMPUS = "PAXgene/Tempus"
    OTHER = "Other"

    __key_map__ = {"1": "Serum", "2": "EDTA", "3": "PAXgene/Tempus", "4": "Other"}


class BiobankSerum(RedcapEnum):
    CLIN_CHEM = "clin-chem"
    PROTEOMICS = "Proteomics"
    METABOLOMICS = "Metabolomics"
    OTHER = "Other"

    __key_map__ = {
        "1": "clin-chem",
        "2": "Proteomics",
        "3": "Metabolomics",
        "4": "Other",
    }


class BiobankEdta(RedcapEnum):
    CLIN_CHEM = "clin-chem"
    PROTEOMICS = "Proteomics"
    GENETIC = "Genetic"
    EPIGENETIC = "Epigenetic"
    OTHER = "Other"

    __key_map__ = {
        "1": "clin-chem",
        "2": "Proteomics",
        "3": "Genetic",
        "4": "Epigenetic",
        "5": "Other",
    }


class BiobankPaxgene(RedcapEnum):
    GENE_EXPRESSION = "Gene expression"
    OTHER = "Other"

    __key_map__ = {"1": "Gene expression", "2": "Other"}


class BiobankPbmc(RedcapEnum):
    IPSC = "iPSC"
    EPIGENETIC = "Epigenetic"
    OTHER = "Other"

    __key_map__ = {"1": "iPSC", "2": "Epigenetic", "3": "Other"}


class BiobankUrin(RedcapEnum):
    CORTISOL = "Cortisol"
    OTHER = "Other"

    __key_map__ = {"1": "Cortisol", "2": "Other"}


class BiobankSaliva(RedcapEnum):
    GENETIC = "Genetic"
    EPIGENETIC = "Epigenetic"
    OTHER = "Other"

    __key_map__ = {"1": "Genetic", "2": "Epigenetic", "3": "Other"}


class DzpgRedcapStudyBiobank(BaseModel):
    biobank_material: list[BiobankMaterial] | None = Field(
        description="Which material did you collect? Multiple answers possible"
    )
    biobank_material_other: str | None = Field(
        description="Which other matirial did you collect?"
    )
    biobank_blood: list[BiobankBlood] | None = Field(
        description="What type of blood collection tubes did you use? Multiple answers possible"
    )
    biobank_blood_other: str | None = Field(
        description="Which other type of blood collection tube did you use?"
    )
    biobank_serum: list[BiobankSerum] | None = Field(
        description="Which type of data did you collect from Serum? Multiple answers possible"
    )
    biobank_serum_other: str | None = Field(
        description="Which other data did you collect from Serum?"
    )
    biobank_edta: list[BiobankEdta] | None = Field(
        description="Which type of data did you collect from EDTA? Multiple answers possible"
    )
    biobank_edta_other: str | None = Field(
        description="Which other data did you collect from EDTA?"
    )
    biobank_paxgene: list[BiobankPaxgene] | None = Field(
        description="Which data did you collect from PAXgene/Tempus? Multiple answers possible"
    )
    biobank_paxgene_other: str | None = Field(
        description="Which other data did you collect from PAXgene/Tempus?"
    )
    biobank_pbmc: list[BiobankPbmc] | None = Field(
        description="Which type of data did you collect from PBMC? Multiple answers possible"
    )
    biobank_pbmc_other: str | None = Field(
        description="Which other data did you collect from PBMC?"
    )
    biobank_urin: list[BiobankUrin] | None = Field(
        description="Which type of data did you collect from urin? Multiple answers possible"
    )
    biobank_urin_other: str | None = Field(
        description="Which other data did you collect from urin?"
    )
    biobank_saliva_other: str | None = Field(
        description="Which other data did you collect from saliva?"
    )
    biobank_saliva: list[BiobankSaliva] | None = Field(
        description="Which type of data did you collect from saliva? Multiple answers possible"
    )

    _parse_biobank_material_other = validator(
        "biobank_material_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_blood_other = validator(
        "biobank_blood_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_serum_other = validator(
        "biobank_serum_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_edta_other = validator(
        "biobank_edta_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_paxgene_other = validator(
        "biobank_paxgene_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_pbmc_other = validator(
        "biobank_pbmc_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_urin_other = validator(
        "biobank_urin_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_biobank_saliva_other = validator(
        "biobank_saliva_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    @root_validator(pre=True)
    def validate_enum_lists(cls, values):
        values["biobank_material"] = parse_enum_list(
            BiobankMaterial, "biobank_material", values
        )
        values["biobank_blood"] = parse_enum_list(BiobankBlood, "biobank_blood", values)
        values["biobank_serum"] = parse_enum_list(BiobankSerum, "biobank_serum", values)
        values["biobank_edta"] = parse_enum_list(BiobankEdta, "biobank_edta", values)
        values["biobank_paxgene"] = parse_enum_list(
            BiobankPaxgene, "biobank_paxgene", values
        )
        values["biobank_pbmc"] = parse_enum_list(BiobankPbmc, "biobank_pbmc", values)
        values["biobank_urin"] = parse_enum_list(BiobankUrin, "biobank_urin", values)
        values["biobank_saliva"] = parse_enum_list(
            BiobankSaliva, "biobank_saliva", values
        )

        return values


class DzpgRedcapStudyImaging(BaseModel):
    data_entity: list[DataEntity] = Field(
        description="1. Data Entity Multiple answers possible"
    )
    data_entity_other: str | None = Field(
        description="Which other modalities? (e.g. PET)"
    )
    mr_n: MrN = Field(description="Number of used devices")
    mr_n_more: str = Field(description="How many devices were used?")
    mr_scanner_type_1: MrScannerType1 | None = Field(
        description="Scanner Type MR device 1"
    )
    mr_scanner_type_other_1: str | None = Field(
        description="Which Scanner Type? MR device 1"
    )
    mr_field_strength_1: MrFieldStrength1 | None = Field(
        description="Field Strenght MR device 1"
    )
    mr_field_strength_other_1: str | None = Field(
        description="Which field strength? MR device 1"
    )
    mr_software_info_1: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. MR XA31) MR device 1"
    )
    mr_software_update_1: bool | None = Field(
        description="Was the device updated within the data acquisition? MR device 1"
    )
    mr_software_update_when_1: str | None = Field(
        description="When was the device updated? MR device 1"
    )
    mr_head_coil_1: MrHeadCoil1 | None = Field(
        description="Head coil channel number MR device 1"
    )
    mr_head_coil_other_1: str | None = Field(
        description="Which other head coil channel number? MR device 1"
    )
    mr_head_coil_replace_1: bool | None = Field(
        description="Was the head coil replaced during the course of the study? MR device 1"
    )
    mr_head_coil_when_1: str | None = Field(
        description="When was the head coil replaced? MR device 1"
    )
    mr_sequences_1: list[MrSequences1] = Field(
        description="Which sequences were collected? MR device 1 Multiple answers possible"
    )
    mr_sequences_1_other: str | None = Field(
        description="Which other sequence(s)? MR device 1"
    )
    mr_n_tasks_1: MrNTasks1 = Field(description="Number of tasks MR device 1")
    mr_n_tasks_1_more: str = Field(description="How many tasks? MR device 1")
    mr_task_construct_1_1: MrTaskConstruct11 | None = Field(
        description="Task construct MR device 1 Task 1"
    )
    mr_task_construct_other_1_1: str | None = Field(
        description="Which task construct? MR device 1 Task 1"
    )
    mr_task_name_1_1: str | None = Field(
        description="Task name (e.g. Hariri) MR device 1 Taks 1"
    )
    mr_task_construct_1_2: MrTaskConstruct12 | None = Field(
        description="Task construct MR device 1 Task 2"
    )
    mr_task_construct_other_1_2: str | None = Field(
        description="Which task construct?\nMR device 1 Task 2"
    )
    mr_task_name_1_2: str | None = Field(
        description="Task name (e.g. Hariri) MR device 1 Taks 2"
    )
    mr_task_construct_1_3: MrTaskConstruct13 | None = Field(
        description="Task construct MR device 1 Task 3"
    )
    mr_task_construct_other_1_3: str | None = Field(
        description="Which task construct? MR device 1 Task 3"
    )
    mr_task_name_1_3: str | None = Field(
        description="Task name (e.g. Hariri) MR device 1 Task 3"
    )
    mr_acq_protocol_task_1: bool | None = Field(
        description="Acquisition protocol published and available? MR device 1"
    )
    mr_acq_protocol_task_link_1: str | None = Field(
        description="Copy link to source MR device 1"
    )
    mr_resolution_t1_1: str | None = Field(
        description="Resolution (in mm3, e.g. 1x1x1) MR device 1"
    )
    mr_sequence_1: str | None = Field(description="Sequence (e.g. MPRAGE) MR device 1")
    mr_prepro_software_1: list[MrPreproSoftware1] | None = Field(
        description="Preprocessing software info MR device 1 Multiple answers possible"
    )
    mr_prepro_software_other_1: str | None = Field(
        description="Which preprocessing software? MR device 1"
    )
    mr_acq_protocol_t1_1: bool | None = Field(
        description="Acquisition protocol published and available? MR device 1"
    )
    mr_acq_protocol_t1_link_1: str | None = Field(
        description="Copy link to source MR device 1"
    )
    mr_resolution_rs_1: str | None = Field(
        description="Resolution (in mm3, e.g. 1x1x1) MR device 1"
    )
    mr_duration_1: str | None = Field(
        description="Duration (in min, e.g. 6) MR device 1"
    )
    mr_condition_1: MrCondition1 | None = Field(description="Condition MR device 1")
    mr_condition_other_1: str | None = Field(description="Which condition? MR device 1")
    mr_rs_prepro_info_1: list[MrRsPreproInfo1] | None = Field(
        description="Preprocessing software info MR device 1 Multiple answers possible"
    )
    mr_rs_prepro_other_1: str | None = Field(
        description="Which preprocessing software? MR device 1"
    )
    mr_acq_protocol_rs_1: bool | None = Field(
        description="Acquisition protocol published and available? MR device 1"
    )
    mr_acq_protocol_rs_link_1: str | None = Field(
        description="Copy link to source MR device 1"
    )
    mr_n_voxels_1: MrNVoxels1 | None = Field(description="Number of voxels MR device 1")
    mr_n_voxels_1_more: str | None = Field(description="How many voxels? MR device 1")
    mr_voxel_position_1_1: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 1 Voxel 1"
    )
    mr_vocel_size_1_1: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 1 Voxel 1"
    )
    mr_voxel_position_1_2: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 1 Voxel 2"
    )
    mr_vocel_size_1_2: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 1 Voxel 2"
    )
    mr_voxel_position_1_3: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 1 Voxel 3"
    )
    mr_voxel_size_1_3: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 1 Voxel 3"
    )
    mr_mrs_sequence_1: MrMrsSequence1 | None = Field(
        description="Sequence name MR device 1"
    )
    mr_mrs_sequence_other_1: str | None = Field(
        description="Which sequence? MR device 1"
    )
    mr_mrs_prepro_info_1: list[MrMrsPreproInfo1] | None = Field(
        description="Preprocessing software info MR device 1 Multiple answers possible"
    )
    mr_mrs_prepro_other_1: str | None = Field(
        description="Which preprocessing software? MR device 1"
    )
    mr_data_type_1: list[MrDataType1] | None = Field(
        description="Data type MR device 1 Multiple answers possible"
    )
    mr_data_type_other_1: str | None = Field(description="Which data type? MR device 1")
    mr_acq_protocol_mrs_1: bool | None = Field(
        description="Acquisition protocol published and available? MR device 1"
    )
    mr_acq_protocol_mrs_link_1: str | None = Field(
        description="Copy link to source MR device 1"
    )
    mr_diffusion_1: str | None = Field(
        description="Number of diffusion directions (e.g. 32) MR device 1"
    )
    mr_b0_1: str | None = Field(description="Number of B0 (e.g. 1) MR device 1")
    mr_distortion_1: bool | None = Field(
        description="Distortion correction sequence MR device 1"
    )
    mr_distortion_which_1: list[MrDistortionWhich1] | None = Field(
        description="Which distortion correction sequence? MR device 1 Multiple answers possible"
    )
    mr_distortio_other_1: str | None = Field(
        description="Which other distortion correction sequence? MR device 1"
    )
    mr_acq_protocol_dti_1: bool | None = Field(
        description="Acquisition protocol published and available? MR device 1"
    )
    mr_acq_protocol_dti_link_1: str | None = Field(
        description="Copy link to source MR device 1"
    )
    mr_scanner_type_2: MrScannerType2 | None = Field(
        description="Scanner Type MR device 2"
    )
    mr_scanner_type_other_2: str | None = Field(
        description="Which Scanner Type? MR device 2"
    )
    mr_field_strength_2: MrFieldStrength2 | None = Field(
        description="Field Strenght MR device 2"
    )
    mr_field_strength_other_2: str | None = Field(
        description="Which field strength? MR device 2"
    )
    mr_software_info_2: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. MR XA31) MR device 2"
    )
    mr_software_update_2: bool | None = Field(
        description="Was the device updated within the data acquisition? MR device 2"
    )
    mr_software_update_when_2: str | None = Field(
        description="When was the device updated? MR device 2"
    )
    mr_head_coil_2: MrHeadCoil2 | None = Field(
        description="Head coil channel number MR device 2"
    )
    mr_head_coil_other_2: str | None = Field(
        description="Which other head coil channel number? MR device 2"
    )
    mr_head_coil_replace_2: bool | None = Field(
        description="Was the head coil replaced during the course of the study? MR device 2"
    )
    mr_head_coil_when_2: str | None = Field(
        description="When was the head coil replaced? MR device 2"
    )
    mr_sequences_2: list[MrSequences2] = Field(
        description="Which sequences were collected? MR device 2 Multiple answers possible"
    )
    mr_sequences_2_other: str | None = Field(
        description="Which other sequence(s)? MR device 2"
    )
    mr_n_tasks_2: MrNTasks2 = Field(description="Number of tasks MR device 2")
    mr_n_tasks_2_more: str = Field(description="How many tasks? MR device 2")
    mr_task_construct_2_1: MrTaskConstruct21 | None = Field(
        description="Task construct MR device 2\n Task 1"
    )
    mr_task_construct_other_2_1: str | None = Field(
        description="Which task construct? MR device 2\n Task 1"
    )
    mr_task_name_2_1: str | None = Field(
        description="Task name (e.g. Hariri) MR device 2\n Task 1"
    )
    mr_task_construct_2_2: MrTaskConstruct22 | None = Field(
        description="Task construct MR device 2\n Task 2"
    )
    mr_task_construct_other_2_2: str | None = Field(
        description="Which task construct? MR device 2\n Task 2"
    )
    mr_task_name_2_2: str | None = Field(
        description="Task name (e.g. Hariri) MR device 2\n Task 2"
    )
    mr_task_construct_2_3: MrTaskConstruct23 | None = Field(
        description="Task construct MR device 2\n Task 3"
    )
    mr_task_construct_other_2_3: str | None = Field(
        description="Which task construct? MR device 2\n Task 3"
    )
    mr_task_name_2_3: str | None = Field(
        description="Task name (e.g. Hariri) MR device 2\n Task 3"
    )
    mr_acq_protocol_task_2: bool | None = Field(
        description="Acquisition protocol published and available? MR device 2"
    )
    mr_acq_protocol_task_link_2: str | None = Field(
        description="Copy link to source MR device 2"
    )
    mr_resolution_t1_2: str | None = Field(
        description="Resolution (in mm3, e.g. 1x1x1) MR device 2"
    )
    mr_sequence_2: str | None = Field(description="Sequence (e.g. MPRAGE) MR device 2")
    mr_prepro_software_2: list[MrPreproSoftware2] | None = Field(
        description="Preprocessing software info MR device 2\n Multiple answers possible"
    )
    mr_prepro_software_other_2: str | None = Field(
        description="Which preprocessing software? MR device 2"
    )
    mr_acq_protocol_t1_2: bool | None = Field(
        description="Acquisition protocol published and available? MR device 2"
    )
    mr_acq_protocol_t1_link_2: str | None = Field(
        description="Copy link to source MR device 2"
    )
    mr_resolution_rs_2: str | None = Field(
        description="Resolution (in mm3, e.g. 1x1x1) MR device 2"
    )
    mr_duration_2: str | None = Field(
        description="Duration (in min, e.g. 6) MR device 2"
    )
    mr_condition_2: MrCondition2 | None = Field(description="Condition MR device 2")
    mr_condition_other_2: str | None = Field(description="Which condition? MR device 2")
    mr_rs_prepro_info_2: list[MrRsPreproInfo2] | None = Field(
        description="Preprocessing software info MR device 2\n Multiple answers possible"
    )
    mr_rs_prepro_other_2: str | None = Field(
        description="Which preprocessing software? MR device 2"
    )
    mr_acq_protocol_rs_2: bool | None = Field(
        description="Acquisition protocol published and available? MR device 2"
    )
    mr_acq_protocol_rs_link_2: str | None = Field(
        description="Copy link to source MR device 2"
    )
    mr_n_voxels_2: MrNVoxels2 | None = Field(description="Number of voxels MR device 2")
    mr_n_voxels_2_more: str | None = Field(description="How many voxels? MR device 2")
    mr_voxel_position_2_1: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 2\n Voxel 1"
    )
    mr_voxel_size_2_1: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 2\n Voxel 1"
    )
    mr_voxel_position_2_2: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 2\n Voxel 2"
    )
    mr_voxel_size_2_2: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 2\n Voxel 2"
    )
    mr_voxel_position_2_3: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 2\n Voxel 3"
    )
    mr_voxel_size_2_3: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 2\n Voxel 3"
    )
    mr_mrs_sequence_2: MrMrsSequence2 | None = Field(
        description="Sequence name MR device 2"
    )
    mr_mrs_sequence_other_2: str | None = Field(
        description="Which sequence? MR device 2"
    )
    mr_mrs_prepro_info_2: list[MrMrsPreproInfo2] | None = Field(
        description="Preprocessing software info MR device 2\n Multiple answers possible"
    )
    mr_mrs_prepro_other_2: str | None = Field(
        description="Which preprocessing software? MR device 2"
    )
    mr_data_type_2: list[MrDataType2] | None = Field(
        description="Data type MR device 2\n Multiple answers possible"
    )
    mr_data_type_other_2: str | None = Field(description="Which data type? MR device 2")
    mr_acq_protocol_mrs_2: bool | None = Field(
        description="Acquisition protocol published and available? MR device 2"
    )
    mr_acq_protocol_mrs_link_2: str | None = Field(
        description="Copy link to source MR device 2"
    )
    mr_diffusion_2: str | None = Field(
        description="Number of diffusion directions (e.g. 32) MR device 2"
    )
    mr_b0_2: str | None = Field(description="Number of B0 (e.g. 1) MR device 2")
    mr_distortion_2: bool | None = Field(
        description="Distortion correction sequence MR device 2"
    )
    mr_distortion_which_2: list[MrDistortionWhich2] | None = Field(
        description="Which distortion correction sequence? MR device 2\n Multiple answers possible"
    )
    mr_distortio_other_2: str | None = Field(
        description="Which other distortion correction sequence? MR device 2"
    )
    mr_acq_protocol_dti_2: bool | None = Field(
        description="Acquisition protocol published and available? MR device 2"
    )
    mr_acq_protocol_dti_link_2: str | None = Field(
        description="Copy link to source MR device 2"
    )
    mr_scanner_type_3: MrScannerType3 | None = Field(
        description="Scanner Type MR device 3"
    )
    mr_scanner_type_other_3: str | None = Field(
        description="Which Scanner Type? MR device 3"
    )
    mr_field_strength_3: MrFieldStrength3 | None = Field(
        description="Field Strenght MR device 3"
    )
    mr_field_strength_other_3: str | None = Field(
        description="Which field strength? MR device 3"
    )
    mr_software_info_3: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. MR XA31) MR device 3"
    )
    mr_software_update_3: bool | None = Field(
        description="Was the device updated within the data acquisition? MR device 3"
    )
    mr_software_update_when_3: str | None = Field(
        description="When was the device updated? MR device 3"
    )
    mr_head_coil_3: MrHeadCoil3 | None = Field(
        description="Head coil channel number MR device 3"
    )
    mr_head_coil_other_3: str | None = Field(
        description="Which other head coil channel number? MR device 3"
    )
    mr_head_coil_replace_3: bool | None = Field(
        description="Was the head coil replaced during the course of the study? MR device 3"
    )
    mr_head_coil_when_3: str | None = Field(
        description="When was the head coil replaced? MR device 3"
    )
    mr_sequences_3: list[MrSequences3] = Field(
        description="Which sequences were collected? MR device 3 Multiple answers possible"
    )
    mr_sequences_3_other: str | None = Field(
        description="Which other sequence(s)? MR device 3"
    )
    mr_n_tasks_3: MrNTasks3 = Field(description="Number of tasks MR device 3")
    mr_n_tasks_3_more: str = Field(description="How many tasks? MR device 3")
    mr_task_construct_3_1: MrTaskConstruct31 | None = Field(
        description="Task construct MR device 3\n Task 1"
    )
    mr_task_construct_other_3_1: str | None = Field(
        description="Which task construct? MR device 3\n Task 1"
    )
    mr_task_name_3_1: str | None = Field(
        description="Task name (e.g. Hariri) MR device 3\n Task 1"
    )
    mr_task_construct_3_2: MrTaskConstruct32 | None = Field(
        description="Task construct MR device 3\n Task 2"
    )
    mr_task_construct_other_3_2: str | None = Field(
        description="Which task construct? MR device 3\n Task 2"
    )
    mr_task_name_3_2: str | None = Field(
        description="Task name (e.g. Hariri) MR device 3\n Task 2"
    )
    mr_task_construct_3_3: MrTaskConstruct33 | None = Field(
        description="Task construct MR device 3\n Task 3"
    )
    mr_task_construct_other_3_3: str | None = Field(
        description="Which task construct? MR device 3\n Task 3"
    )
    mr_task_name_3_3: str | None = Field(
        description="Task name (e.g. Hariri) MR device 3\n Task 3"
    )
    mr_acq_protocol_task_3: bool | None = Field(
        description="Acquisition protocol published and available? MR device 3"
    )
    mr_acq_protocol_task_link_3: str | None = Field(
        description="Copy link to source MR device 3"
    )
    mr_resolution_t1_3: str | None = Field(
        description="Resolution (in mm3, e.g. 1x1x1) MR device 3"
    )
    mr_sequence_3: str | None = Field(description="Sequence (e.g. MPRAGE) MR device 3")
    mr_prepro_software_3: list[MrPreproSoftware3] | None = Field(
        description="Preprocessing software info MR device 3\n Multiple answers possible"
    )
    mr_prepro_software_other_3: str | None = Field(
        description="Which preprocessing software? MR device 3"
    )
    mr_acq_protocol_t1_3: bool | None = Field(
        description="Acquisition protocol published and available? MR device 3"
    )
    mr_acq_protocol_t1_link_3: str | None = Field(
        description="Copy link to source MR device 3"
    )
    mr_resolution_rs_3: str | None = Field(
        description="Resolution (in mm3, e.g. 1x1x1) MR device 3"
    )
    mr_duration_3: str | None = Field(
        description="Duration (in min, e.g. 6) MR device 3"
    )
    mr_condition_3: MrCondition3 | None = Field(description="Condition MR device 3")
    mr_condition_other_3: str | None = Field(description="Which condition? MR device 3")
    mr_rs_prepro_info_3: list[MrRsPreproInfo3] | None = Field(
        description="Preprocessing software info MR device 3\n Multiple answers possible"
    )
    mr_rs_prepro_other_3: str | None = Field(
        description="Which preprocessing software? MR device 3"
    )
    mr_acq_protocol_rs_3: bool | None = Field(
        description="Acquisition protocol published and available? MR device 3"
    )
    mr_acq_protocol_rs_link_3: str | None = Field(
        description="Copy link to source MR device 3"
    )
    mr_n_voxels_3: MrNVoxels3 | None = Field(description="Number of voxels MR device 3")
    mr_n_voxels_3_more: str | None = Field(description="How many voxels? MR device 3")
    mr_voxel_position_3_1: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 3\n Voxel 1"
    )
    mr_voxel_size_3_1: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 3\n Voxel 1"
    )
    mr_voxel_position_3_2: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 3\n Voxel 2"
    )
    mr_voxel_size_3_2: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 3\n Voxel 2"
    )
    mr_voxel_position_3_3: str | None = Field(
        description="Voxel position (region name, e.g. mPFC) MR device 3\n Voxel 3"
    )
    mr_voxel_size_3_3: str | None = Field(
        description="Voxel size (in mm3, e.g. 25x15x10) MR device 3\n Voxel 3"
    )
    mr_mrs_sequence_3: MrMrsSequence3 | None = Field(
        description="Sequence name MR device 3"
    )
    mr_mrs_sequence_other_3: str | None = Field(
        description="Which sequence? MR device 3"
    )
    mr_mrs_prepro_info_3: list[MrMrsPreproInfo3] | None = Field(
        description="Preprocessing software info MR device 3\n Multiple answers possible"
    )
    mr_mrs_prepro_other_3: str | None = Field(
        description="Which preprocessing software? MR device 3"
    )
    mr_data_type_3: list[MrDataType3] | None = Field(
        description="Data type MR device 3\n Multiple answers possible"
    )
    mr_data_type_other_3: str | None = Field(description="Which data type? MR device 3")
    mr_acq_protocol_mrs_3: bool | None = Field(
        description="Acquisition protocol published and available? MR device 3"
    )
    mr_acq_protocol_mrs_link_3: str | None = Field(
        description="Copy link to source MR device 3"
    )
    mr_diffusion_3: str | None = Field(
        description="Number of diffusion directions (e.g. 32) MR device 3"
    )
    mr_b0_3: str | None = Field(description="Number of B0 (e.g. 1) MR device 3")
    mr_distortion_3: bool | None = Field(
        description="Distortion correction sequence MR device 3"
    )
    mr_distortion_which_3: list[MrDistortionWhich3] | None = Field(
        description="Which distortion correction sequence? MR device 3\n Multiple answers possible"
    )
    mr_distortio_other_3: str | None = Field(
        description="Which other distortion correction sequence? MR device 3"
    )
    mr_acq_protocol_dti_3: bool | None = Field(
        description="Acquisition protocol published and available? MR device 3"
    )
    mr_acq_protocol_dti_link_3: str | None = Field(
        description="Copy link to source MR device 3"
    )
    eeg_n: EegN = Field(description="Number of used devices")
    eeg_n_more: str = Field(description="How many devices were used?")
    eeg_hardware_1: str | None = Field(
        description="Hardware information (e.g. Brain Products) EEG device 1"
    )
    eeg_software_info_1: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. Biopac) EEG device 1"
    )
    eeg_software_update_1: bool | None = Field(
        description="Was the device updated within the data acquisition? EEG device 1"
    )
    eeg_software_update_when_1: str | None = Field(
        description="When was the device updated? EEG device 1"
    )
    eeg_n_channels_1: EegNChannels1 | None = Field(
        description="Number of channels EEG device 1"
    )
    eeg_n_channels_other_1: str | None = Field(
        description="What number of channels? EEG device 1"
    )
    eeg_electrodes_1: EegElectrodes1 | None = Field(
        description="Type of electrodes EEG device 1"
    )
    eeg_electrodes_other_1: str | None = Field(
        description="Which electrodes? EEG device 1"
    )
    eeg_sampling_rate_1: str | None = Field(
        description="Sampling rate (in Hz, e.g. 1000) EEG device 1"
    )
    eeg_paradigm_1: list[EegParadigm1] | None = Field(
        description="Paradigm EEG device 1"
    )
    eeg_condition_1: EegCondition1 | None = Field(description="Condition EEG device 1")
    eeg_condition_other_1: str | None = Field(
        description="Which condition? EEG device 1"
    )
    eeg_duration_1: str | None = Field(
        description="Duration (in min, e.g. 6) EEG device 1"
    )
    eeg_n_tasks_1: EegNTasks1 = Field(description="Number of tasks EEG device 1")
    eeg_n_tasks_1_more: str = Field(description="How many tasks? EEG device 1")
    eeg_task_construct_1_1: EegTaskConstruct11 | None = Field(
        description="Task construct EEG device 1 Task 1"
    )
    eeg_task_construct_other_1_1: str | None = Field(
        description="Which task construct? EEG device 1 Task 1"
    )
    eeg_task_construct_1_2: EegTaskConstruct12 | None = Field(
        description="Task construct EEG device 1 Task 2"
    )
    eeg_task_construct_other_1_2: str | None = Field(
        description="Which task construct? EEG device 1 Task 2"
    )
    eeg_task_construct_1_3: EegTaskConstruct13 | None = Field(
        description="Task construct EEG device 1 Task 3"
    )
    eeg_task_construct_other_1_3: str | None = Field(
        description="Which task construct? EEG device 1 Task 3"
    )
    eeg_task_name_1: str | None = Field(
        description="Task name (e.g. Hariri) EEG device 1"
    )
    eeg_prepro_software_1: list[EegPreproSoftware1] | None = Field(
        description="Preprocessing software info EEG device 1 Multiple answers possible"
    )
    eeg_prepro_software_other_1: str | None = Field(
        description="Which preprocessing software? EEG device 1"
    )
    eeg_acq_protocol_1: bool | None = Field(
        description="Acquisition protocol published and available? EEG device 1"
    )
    eeg_acq_protocol_link_1: str | None = Field(
        description="Copy link to source EEG device 1"
    )
    eeg_hardware_2: str | None = Field(
        description="Hardware information (e.g. Brain Products) EEG device 2"
    )
    eeg_software_info_2: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. Biopac) EEG device 2"
    )
    eeg_software_update_2: bool | None = Field(
        description="Was the device updated within the data acquisition? EEG device 2"
    )
    eeg_software_update_when_2: str | None = Field(
        description="When was the device updated? EEG device 2"
    )
    eeg_n_channels_2: EegNChannels2 | None = Field(
        description="Number of channels EEG device 2"
    )
    eeg_n_channels_other_2: str | None = Field(
        description="What number of channels? EEG device 2"
    )
    eeg_electrodes_2: EegElectrodes2 | None = Field(
        description="Type of electrodes EEG device 2"
    )
    eeg_electrodes_other_2: str | None = Field(
        description="Which electrodes? EEG device 2"
    )
    eeg_sampling_rate_2: str | None = Field(
        description="Sampling rate (in Hz, e.g. 1000) EEG device 2"
    )
    eeg_paradigm_2: list[EegParadigm2] | None = Field(
        description="Paradigm EEG device 2"
    )
    eeg_condition_2: EegCondition2 | None = Field(description="Condition EEG device 2")
    eeg_condition_other_2: str | None = Field(
        description="Which condition? EEG device 2"
    )
    eeg_duration_2: str | None = Field(
        description="Duration (in min, e.g. 6) EEG device 2"
    )
    eeg_n_tasks_2: EegNTasks2 = Field(description="Number of tasks EEG device 2")
    eeg_n_tasks_2_more: str = Field(description="How many tasks? EEG device 2")
    eeg_task_construct_2_1: EegTaskConstruct21 | None = Field(
        description="Task construct EEG device 2\n Task 1"
    )
    eeg_task_construct_other_2_1: str | None = Field(
        description="Which task construct? EEG device 2\n Task 1"
    )
    eeg_task_construct_2_2: EegTaskConstruct22 | None = Field(
        description="Task construct EEG device 2\n Task 2"
    )
    eeg_task_construct_other_2_2: str | None = Field(
        description="Which task construct? EEG device 2\n Task 2"
    )
    eeg_task_construct_2_3: EegTaskConstruct23 | None = Field(
        description="Task construct EEG device 2\n Task 3"
    )
    eeg_task_construct_other_2_3: str | None = Field(
        description="Which task construct? EEG device 2\n Task 3"
    )
    eeg_task_name_2: str | None = Field(
        description="Task name (e.g. Hariri) EEG device 2"
    )
    eeg_prepro_software_2: list[EegPreproSoftware2] | None = Field(
        description="Preprocessing software info EEG device 2\n Multiple answers possible"
    )
    eeg_prepro_software_other_2: str | None = Field(
        description="Which preprocessing software? EEG device 2"
    )
    eeg_acq_protocol_2: bool | None = Field(
        description="Acquisition protocol published and available? EEG device 2"
    )
    eeg_acq_protocol_link_2: str | None = Field(
        description="Copy link to source EEG device 2"
    )
    eeg_hardware_3: str | None = Field(
        description="Hardware information (e.g. Brain Products) EEG device 3"
    )
    eeg_software_info_3: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. Biopac) EEG device 3"
    )
    eeg_software_update_3: bool | None = Field(
        description="Was the device updated within the data acquisition? EEG device 3"
    )
    eeg_software_update_when_3: str | None = Field(
        description="When was the device updated? EEG device 3"
    )
    eeg_n_channels_3: EegNChannels3 | None = Field(
        description="Number of channels EEG device 3"
    )
    eeg_n_channels_other_3: str | None = Field(
        description="What number of channels? EEG device 3"
    )
    eeg_electrodes_3: EegElectrodes3 | None = Field(
        description="Type of electrodes EEG device 3"
    )
    eeg_electrodes_other_3: str | None = Field(
        description="Which electrodes? EEG device 3"
    )
    eeg_sampling_rate_3: str | None = Field(
        description="Sampling rate (in Hz, e.g. 1000) EEG device 3"
    )
    eeg_paradigm_3: list[EegParadigm3] | None = Field(
        description="Paradigm EEG device 3"
    )
    eeg_condition_3: EegCondition3 | None = Field(description="Condition EEG device 3")
    eeg_condition_other_3: str | None = Field(
        description="Which condition? EEG device 3"
    )
    eeg_duration_3: str | None = Field(
        description="Duration (in min, e.g. 6) EEG device 3"
    )
    eeg_n_tasks_3: EegNTasks3 = Field(description="Nuber of tasks EEG device 3")
    eeg_n_tasks_3_more: str = Field(description="How many tasks? EEG device 3")
    eeg_task_construct_3_1: EegTaskConstruct31 | None = Field(
        description="Task construct EEG device 3\n Task 1"
    )
    eeg_task_construct_other_3_1: str | None = Field(
        description="Which task construct? EEG device 3\n Task 1"
    )
    eeg_task_construct_3_2: EegTaskConstruct32 | None = Field(
        description="Task construct EEG device 3\n Task 2"
    )
    eeg_task_construct_other_3_2: str | None = Field(
        description="Which task construct? EEG device 3\n Task 2"
    )
    eeg_task_construct_3_3: EegTaskConstruct33 | None = Field(
        description="Task construct EEG device 3\n Task 3"
    )
    eeg_task_construct_other_3_3: str | None = Field(
        description="Which task construct? EEG device 3\n Task 3"
    )
    eeg_task_name_3: str | None = Field(
        description="Task name (e.g. Hariri) EEG device 3"
    )
    eeg_prepro_software_3: list[EegPreproSoftware3] | None = Field(
        description="Preprocessing software info EEG device 3\n Multiple answers possible"
    )
    eeg_prepro_software_other_3: str | None = Field(
        description="Which preprocessing software? EEG device 3"
    )
    eeg_acq_protocol_3: bool | None = Field(
        description="Acquisition protocol published and available? EEG device 3"
    )
    eeg_acq_protocol_link_3: str | None = Field(
        description="Copy link to source EEG device 3"
    )
    fnirs_n: FnirsN = Field(description="Number of used devices")
    fnirs_n_more: str = Field(description="How many devices were used?")
    fnirs_hardware_1: str | None = Field(
        description="Hardware information (e.g. NIRx, NIRSport2) fNIRS device 1"
    )
    fnirs_software_info_1: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. NIRSLab) fNIRS device 1"
    )
    fnirs_software_update_1: bool | None = Field(
        description="Was the device updated within the data acquisition? fNIRS device 1"
    )
    fnirs_software_update_when_1: str | None = Field(
        description="When was the device updated? fNIRS device 1"
    )
    fnirs_n_channels_1: FnirsNChannels1 | None = Field(
        description="Number of channels fNIRS device 1"
    )
    fnirs_n_channels_other_1: str | None = Field(
        description="What number of channels? fNIRS device 1"
    )
    fnirs_detectors_1: bool | None = Field(
        description="Short distance detectors fNIRS device 1"
    )
    fnirs_sampling_rate_1: str | None = Field(
        description="Sampling rate (in Hz, e.g. 10) fNIRS device 1"
    )
    fnirs_paradigm_1: list[FnirsParadigm1] | None = Field(
        description="Paradigm fNIRS device 1"
    )
    fnirs_condition_1: FnirsCondition1 | None = Field(
        description="Condition fNIRS device 1"
    )
    fnirs_condition_other_1: str | None = Field(
        description="Which condition? fNIRS device 1"
    )
    fnirs_duration_1: str | None = Field(
        description="Duration (in min, e.g. 6) fNIRS device 1"
    )
    fnirs_n_tasks_1: FnirsNTasks1 = Field(description="Number of tasks fNIRS device 1")
    fnirs_n_tasks_1_more: str = Field(description="How many tasks? fNIRS device 1")
    fnirs_task_construct_1_1: FnirsTaskConstruct11 | None = Field(
        description="Task construct fNIRS device 1 Task 1"
    )
    fnirs_task_construct_other_1_1: str | None = Field(
        description="Which task construct? fNIRS device 1 Task 1"
    )
    fnirs_task_name_1_1: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 1 Task 1"
    )
    fnirs_task_construct_1_2: FnirsTaskConstruct12 | None = Field(
        description="Task construct fNIRS device 1 Task 2"
    )
    fnirs_task_construct_other_1_2: str | None = Field(
        description="Which task construct? fNIRS device 1 Task 2"
    )
    fnirs_task_name_1_2: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 1 Task 2"
    )
    fnirs_task_construct_1_3: FnirsTaskConstruct13 | None = Field(
        description="Task construct fNIRS device 1 Task 3"
    )
    fnirs_task_construct_other_1_3: str | None = Field(
        description="Which task construct? fNIRS device 1 Task 3"
    )
    fnirs_task_name_1_3: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 1 Task 3"
    )
    fnirs_prepro_software_1: list[FnirsPreproSoftware1] | None = Field(
        description="Preprocessing software info fNIRS device 1 Multiple answers possible"
    )
    fnirs_prepro_software_other_1: str | None = Field(
        description="Which preprocessing software? fNIRS device 1"
    )
    fnirs_acq_protocol_1: bool | None = Field(
        description="Acquisition protocol published and available? fNIRS device 1"
    )
    fnirs_acq_protocol_link_1: str | None = Field(
        description="Copy link to source fNIRS device 1"
    )
    fnirs_hardware_2: str | None = Field(
        description="Hardware information (e.g. NIRx, NIRSport2) fNIRS device 2"
    )
    fnirs_software_info_2: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. NIRSLab) fNIRS device 2"
    )
    fnirs_software_update_2: bool | None = Field(
        description="Was the device updated within the data acquisition? fNIRS device 2"
    )
    fnirs_software_update_when_2: str | None = Field(
        description="When was the device updated? fNIRS device 2"
    )
    fnirs_n_channels_2: FnirsNChannels2 | None = Field(
        description="Number of channels fNIRS device 2"
    )
    fnirs_n_channels_other_2: str | None = Field(
        description="What number of channels? fNIRS device 2"
    )
    fnirs_detectors_2: bool | None = Field(
        description="Short distance detectors fNIRS device 2"
    )
    fnirs_sampling_rate_2: str | None = Field(
        description="Sampling rate (in Hz, e.g. 10) fNIRS device 2"
    )
    fnirs_paradigm_2: list[FnirsParadigm2] | None = Field(
        description="Paradigm fNIRS device 2"
    )
    fnirs_condition_2: FnirsCondition2 | None = Field(
        description="Condition fNIRS device 2"
    )
    fnirs_condition_other_2: str | None = Field(
        description="Which condition? fNIRS device 2"
    )
    fnirs_duration_2: str | None = Field(
        description="Duration (in min, e.g. 6) fNIRS device 2"
    )
    fnirs_n_tasks_2: FnirsNTasks2 = Field(description="Number of tasks fNIRS device 2")
    fnirs_n_tasks_2_more: str = Field(description="How many tasks? fNIRS device 2")
    fnirs_task_construct_2_1: FnirsTaskConstruct21 | None = Field(
        description="Task construct fNIRS device 2\n Task 1"
    )
    fnirs_task_construct_other_2_1: str | None = Field(
        description="Which task construct? fNIRS device 2\n Task 1"
    )
    fnirs_task_name_2_1: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 2\n Task 1"
    )
    fnirs_task_construct_2_2: FnirsTaskConstruct22 | None = Field(
        description="Task construct fNIRS device 2\n Task 2"
    )
    fnirs_task_construct_other_2_2: str | None = Field(
        description="Which task construct? fNIRS device 2\n Task 2"
    )
    fnirs_task_name_2_2: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 2\n Task 2"
    )
    fnirs_task_construct_2_3: FnirsTaskConstruct23 | None = Field(
        description="Task construct fNIRS device 2\n Task 3"
    )
    fnirs_task_construct_other_2_3: str | None = Field(
        description="Which task construct? fNIRS device 2\n Task 3"
    )
    fnirs_task_name_2_3: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 2\n Task 3"
    )
    fnirs_prepro_software_2: list[FnirsPreproSoftware2] | None = Field(
        description="Preprocessing software info fNIRS device 2\n Multiple answers possible"
    )
    fnirs_prepro_software_other_2: str | None = Field(
        description="Which preprocessing software? fNIRS device 2"
    )
    fnirs_acq_protocol_2: bool | None = Field(
        description="Acquisition protocol published and available? fNIRS device 2"
    )
    fnirs_acq_protocol_link_2: str | None = Field(
        description="Copy link to source fNIRS device 2"
    )
    fnirs_hardware_3: str | None = Field(
        description="Hardware information (e.g. NIRx, NIRSport2) fNIRS device 3"
    )
    fnirs_software_info_3: str | None = Field(
        description="Operating System/Software Information during data acquisition (e.g. NIRSLab) fNIRS device 3"
    )
    fnirs_software_update_3: bool | None = Field(
        description="Was the device updated within the data acquisition? fNIRS device 3"
    )
    fnirs_software_update_when_3: str | None = Field(
        description="When was the device updated?\nfNIRS device 3"
    )
    fnirs_n_channels_3: FnirsNChannels3 | None = Field(
        description="Number of channels fNIRS device 3"
    )
    fnirs_n_channels_other_3: str | None = Field(
        description="What number of channels? fNIRS device 3"
    )
    fnirs_detectors_3: bool | None = Field(
        description="Short distance detectors fNIRS device 3"
    )
    fnirs_sampling_rate_3: str | None = Field(
        description="Sampling rate (in Hz, e.g. 10) fNIRS device 3"
    )
    fnirs_paradigm_3: list[FnirsParadigm3] | None = Field(
        description="Paradigm fNIRS device 3"
    )
    fnirs_condition_3: FnirsCondition3 | None = Field(
        description="Condition fNIRS device 3"
    )
    fnirs_condition_other_3: str | None = Field(
        description="Which condition? fNIRS device 3"
    )
    fnirs_duration_3: str | None = Field(
        description="Duration (in min, e.g. 6) fNIRS device 3"
    )
    fnirs_n_tasks_3: FnirsNTasks3 = Field(description="Number of tasks fNIRS device 3")
    fnirs_n_tasks_3_more: str = Field(description="How many tasks? fNIRS device 3")
    fnirs_task_construct_3_1: FnirsTaskConstruct31 | None = Field(
        description="Task construct fNIRS device 3\n Task 1"
    )
    fnirs_task_construct_other_3_1: str | None = Field(
        description="Which task construct? fNIRS device 3\n Task 1"
    )
    fnirs_task_name_3_1: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 3\n Task 1"
    )
    fnirs_task_construct_3_2: FnirsTaskConstruct32 | None = Field(
        description="Task construct fNIRS device 3\n Task 2"
    )
    fnirs_task_construct_other_3_2: str | None = Field(
        description="Which task construct? fNIRS device 3\n Task 2"
    )
    fnirs_task_name_3_2: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 3\n Task 2"
    )
    fnirs_task_construct_3_3: FnirsTaskConstruct33 | None = Field(
        description="Task construct fNIRS device 3\n Task 3"
    )
    fnirs_task_construct_other_3_3: str | None = Field(
        description="Which task construct? fNIRS device 3\n Task 3"
    )
    fnirs_task_name_3_3: str | None = Field(
        description="Task name (e.g. Hariri) fNIRS device 3\n Task 3"
    )
    fnirs_prepro_software_3: list[FnirsPreproSoftware3] | None = Field(
        description="Preprocessing software info fNIRS device 3\n Multiple answers possible"
    )
    fnirs_prepro_software_other_3: str | None = Field(
        description="Which preprocessing software? fNIRS device 3"
    )
    fnirs_acq_protocol_3: bool | None = Field(
        description="Acquisition protocol published and available? fNIRS device 3"
    )
    fnirs_acq_protocol_link_3: str | None = Field(
        description="Copy link to source fNIRS device 3"
    )
    phys_modality: list[PhysModality] | None = Field(
        description="Modality Multiple answers possible"
    )
    phys_modality_other: str | None = Field(description="Which modality?")
    imaging_comments: str | None = Field(description="3. General comments")

    _parse_data_entity_other = validator(
        "data_entity_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n = validator(
        "mr_n",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrN))

    _parse_mr_n_more = validator(
        "mr_n_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_mr_scanner_type_1 = validator(
        "mr_scanner_type_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrScannerType1))

    _parse_mr_scanner_type_other_1 = validator(
        "mr_scanner_type_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_field_strength_1 = validator(
        "mr_field_strength_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrFieldStrength1))

    _parse_mr_field_strength_other_1 = validator(
        "mr_field_strength_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_software_info_1 = validator(
        "mr_software_info_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_software_update_1 = validator(
        "mr_software_update_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_software_update_when_1 = validator(
        "mr_software_update_when_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_head_coil_1 = validator(
        "mr_head_coil_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrHeadCoil1))

    _parse_mr_head_coil_other_1 = validator(
        "mr_head_coil_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_head_coil_replace_1 = validator(
        "mr_head_coil_replace_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_head_coil_when_1 = validator(
        "mr_head_coil_when_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_sequences_1_other = validator(
        "mr_sequences_1_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n_tasks_1 = validator(
        "mr_n_tasks_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrNTasks1))

    _parse_mr_n_tasks_1_more = validator(
        "mr_n_tasks_1_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_mr_task_construct_1_1 = validator(
        "mr_task_construct_1_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct11))

    _parse_mr_task_construct_other_1_1 = validator(
        "mr_task_construct_other_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_1_1 = validator(
        "mr_task_name_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_construct_1_2 = validator(
        "mr_task_construct_1_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct12))

    _parse_mr_task_construct_other_1_2 = validator(
        "mr_task_construct_other_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_1_2 = validator(
        "mr_task_name_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_construct_1_3 = validator(
        "mr_task_construct_1_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct13))

    _parse_mr_task_construct_other_1_3 = validator(
        "mr_task_construct_other_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_1_3 = validator(
        "mr_task_name_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_task_1 = validator(
        "mr_acq_protocol_task_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_task_link_1 = validator(
        "mr_acq_protocol_task_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_resolution_t1_1 = validator(
        "mr_resolution_t1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_sequence_1 = validator(
        "mr_sequence_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_prepro_software_other_1 = validator(
        "mr_prepro_software_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_t1_1 = validator(
        "mr_acq_protocol_t1_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_t1_link_1 = validator(
        "mr_acq_protocol_t1_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_resolution_rs_1 = validator(
        "mr_resolution_rs_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_duration_1 = validator(
        "mr_duration_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_condition_1 = validator(
        "mr_condition_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrCondition1))

    _parse_mr_condition_other_1 = validator(
        "mr_condition_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_rs_prepro_other_1 = validator(
        "mr_rs_prepro_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_rs_1 = validator(
        "mr_acq_protocol_rs_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_rs_link_1 = validator(
        "mr_acq_protocol_rs_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n_voxels_1 = validator(
        "mr_n_voxels_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrNVoxels1))

    _parse_mr_n_voxels_1_more = validator(
        "mr_n_voxels_1_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_1_1 = validator(
        "mr_voxel_position_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_vocel_size_1_1 = validator(
        "mr_vocel_size_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_1_2 = validator(
        "mr_voxel_position_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_vocel_size_1_2 = validator(
        "mr_vocel_size_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_1_3 = validator(
        "mr_voxel_position_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_1_3 = validator(
        "mr_voxel_size_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_mrs_sequence_1 = validator(
        "mr_mrs_sequence_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrMrsSequence1))

    _parse_mr_mrs_sequence_other_1 = validator(
        "mr_mrs_sequence_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_mrs_prepro_other_1 = validator(
        "mr_mrs_prepro_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_data_type_other_1 = validator(
        "mr_data_type_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_mrs_1 = validator(
        "mr_acq_protocol_mrs_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_mrs_link_1 = validator(
        "mr_acq_protocol_mrs_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_diffusion_1 = validator(
        "mr_diffusion_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_b0_1 = validator(
        "mr_b0_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_distortion_1 = validator(
        "mr_distortion_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_distortio_other_1 = validator(
        "mr_distortio_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_dti_1 = validator(
        "mr_acq_protocol_dti_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_dti_link_1 = validator(
        "mr_acq_protocol_dti_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_scanner_type_2 = validator(
        "mr_scanner_type_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrScannerType2))

    _parse_mr_scanner_type_other_2 = validator(
        "mr_scanner_type_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_field_strength_2 = validator(
        "mr_field_strength_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrFieldStrength2))

    _parse_mr_field_strength_other_2 = validator(
        "mr_field_strength_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_software_info_2 = validator(
        "mr_software_info_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_software_update_2 = validator(
        "mr_software_update_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_software_update_when_2 = validator(
        "mr_software_update_when_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_head_coil_2 = validator(
        "mr_head_coil_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrHeadCoil2))

    _parse_mr_head_coil_other_2 = validator(
        "mr_head_coil_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_head_coil_replace_2 = validator(
        "mr_head_coil_replace_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_head_coil_when_2 = validator(
        "mr_head_coil_when_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_sequences_2_other = validator(
        "mr_sequences_2_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n_tasks_2 = validator(
        "mr_n_tasks_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrNTasks2))

    _parse_mr_n_tasks_2_more = validator(
        "mr_n_tasks_2_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_mr_task_construct_2_1 = validator(
        "mr_task_construct_2_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct21))

    _parse_mr_task_construct_other_2_1 = validator(
        "mr_task_construct_other_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_2_1 = validator(
        "mr_task_name_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_construct_2_2 = validator(
        "mr_task_construct_2_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct22))

    _parse_mr_task_construct_other_2_2 = validator(
        "mr_task_construct_other_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_2_2 = validator(
        "mr_task_name_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_construct_2_3 = validator(
        "mr_task_construct_2_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct23))

    _parse_mr_task_construct_other_2_3 = validator(
        "mr_task_construct_other_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_2_3 = validator(
        "mr_task_name_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_task_2 = validator(
        "mr_acq_protocol_task_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_task_link_2 = validator(
        "mr_acq_protocol_task_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_resolution_t1_2 = validator(
        "mr_resolution_t1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_sequence_2 = validator(
        "mr_sequence_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_prepro_software_other_2 = validator(
        "mr_prepro_software_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_t1_2 = validator(
        "mr_acq_protocol_t1_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_t1_link_2 = validator(
        "mr_acq_protocol_t1_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_resolution_rs_2 = validator(
        "mr_resolution_rs_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_duration_2 = validator(
        "mr_duration_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_condition_2 = validator(
        "mr_condition_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrCondition2))

    _parse_mr_condition_other_2 = validator(
        "mr_condition_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_rs_prepro_other_2 = validator(
        "mr_rs_prepro_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_rs_2 = validator(
        "mr_acq_protocol_rs_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_rs_link_2 = validator(
        "mr_acq_protocol_rs_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n_voxels_2 = validator(
        "mr_n_voxels_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrNVoxels2))

    _parse_mr_n_voxels_2_more = validator(
        "mr_n_voxels_2_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_2_1 = validator(
        "mr_voxel_position_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_2_1 = validator(
        "mr_voxel_size_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_2_2 = validator(
        "mr_voxel_position_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_2_2 = validator(
        "mr_voxel_size_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_2_3 = validator(
        "mr_voxel_position_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_2_3 = validator(
        "mr_voxel_size_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_mrs_sequence_2 = validator(
        "mr_mrs_sequence_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrMrsSequence2))

    _parse_mr_mrs_sequence_other_2 = validator(
        "mr_mrs_sequence_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_mrs_prepro_other_2 = validator(
        "mr_mrs_prepro_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_data_type_other_2 = validator(
        "mr_data_type_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_mrs_2 = validator(
        "mr_acq_protocol_mrs_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_mrs_link_2 = validator(
        "mr_acq_protocol_mrs_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_diffusion_2 = validator(
        "mr_diffusion_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_b0_2 = validator(
        "mr_b0_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_distortion_2 = validator(
        "mr_distortion_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_distortio_other_2 = validator(
        "mr_distortio_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_dti_2 = validator(
        "mr_acq_protocol_dti_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_dti_link_2 = validator(
        "mr_acq_protocol_dti_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_scanner_type_3 = validator(
        "mr_scanner_type_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrScannerType3))

    _parse_mr_scanner_type_other_3 = validator(
        "mr_scanner_type_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_field_strength_3 = validator(
        "mr_field_strength_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrFieldStrength3))

    _parse_mr_field_strength_other_3 = validator(
        "mr_field_strength_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_software_info_3 = validator(
        "mr_software_info_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_software_update_3 = validator(
        "mr_software_update_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_software_update_when_3 = validator(
        "mr_software_update_when_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_head_coil_3 = validator(
        "mr_head_coil_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrHeadCoil3))

    _parse_mr_head_coil_other_3 = validator(
        "mr_head_coil_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_head_coil_replace_3 = validator(
        "mr_head_coil_replace_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_head_coil_when_3 = validator(
        "mr_head_coil_when_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_sequences_3_other = validator(
        "mr_sequences_3_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n_tasks_3 = validator(
        "mr_n_tasks_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrNTasks3))

    _parse_mr_n_tasks_3_more = validator(
        "mr_n_tasks_3_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_mr_task_construct_3_1 = validator(
        "mr_task_construct_3_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct31))

    _parse_mr_task_construct_other_3_1 = validator(
        "mr_task_construct_other_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_3_1 = validator(
        "mr_task_name_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_construct_3_2 = validator(
        "mr_task_construct_3_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct32))

    _parse_mr_task_construct_other_3_2 = validator(
        "mr_task_construct_other_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_3_2 = validator(
        "mr_task_name_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_construct_3_3 = validator(
        "mr_task_construct_3_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrTaskConstruct33))

    _parse_mr_task_construct_other_3_3 = validator(
        "mr_task_construct_other_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_task_name_3_3 = validator(
        "mr_task_name_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_task_3 = validator(
        "mr_acq_protocol_task_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_task_link_3 = validator(
        "mr_acq_protocol_task_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_resolution_t1_3 = validator(
        "mr_resolution_t1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_sequence_3 = validator(
        "mr_sequence_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_prepro_software_other_3 = validator(
        "mr_prepro_software_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_t1_3 = validator(
        "mr_acq_protocol_t1_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_t1_link_3 = validator(
        "mr_acq_protocol_t1_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_resolution_rs_3 = validator(
        "mr_resolution_rs_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_duration_3 = validator(
        "mr_duration_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_condition_3 = validator(
        "mr_condition_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrCondition3))

    _parse_mr_condition_other_3 = validator(
        "mr_condition_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_rs_prepro_other_3 = validator(
        "mr_rs_prepro_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_rs_3 = validator(
        "mr_acq_protocol_rs_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_rs_link_3 = validator(
        "mr_acq_protocol_rs_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_n_voxels_3 = validator(
        "mr_n_voxels_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrNVoxels3))

    _parse_mr_n_voxels_3_more = validator(
        "mr_n_voxels_3_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_3_1 = validator(
        "mr_voxel_position_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_3_1 = validator(
        "mr_voxel_size_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_3_2 = validator(
        "mr_voxel_position_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_3_2 = validator(
        "mr_voxel_size_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_position_3_3 = validator(
        "mr_voxel_position_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_voxel_size_3_3 = validator(
        "mr_voxel_size_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_mrs_sequence_3 = validator(
        "mr_mrs_sequence_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(MrMrsSequence3))

    _parse_mr_mrs_sequence_other_3 = validator(
        "mr_mrs_sequence_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_mrs_prepro_other_3 = validator(
        "mr_mrs_prepro_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_data_type_other_3 = validator(
        "mr_data_type_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_mrs_3 = validator(
        "mr_acq_protocol_mrs_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_mrs_link_3 = validator(
        "mr_acq_protocol_mrs_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_diffusion_3 = validator(
        "mr_diffusion_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_b0_3 = validator(
        "mr_b0_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_distortion_3 = validator(
        "mr_distortion_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_distortio_other_3 = validator(
        "mr_distortio_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mr_acq_protocol_dti_3 = validator(
        "mr_acq_protocol_dti_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_mr_acq_protocol_dti_link_3 = validator(
        "mr_acq_protocol_dti_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n = validator(
        "eeg_n",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegN))

    _parse_eeg_n_more = validator(
        "eeg_n_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_eeg_hardware_1 = validator(
        "eeg_hardware_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_software_info_1 = validator(
        "eeg_software_info_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_software_update_1 = validator(
        "eeg_software_update_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_eeg_software_update_when_1 = validator(
        "eeg_software_update_when_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n_channels_1 = validator(
        "eeg_n_channels_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegNChannels1))

    _parse_eeg_n_channels_other_1 = validator(
        "eeg_n_channels_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_electrodes_1 = validator(
        "eeg_electrodes_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegElectrodes1))

    _parse_eeg_electrodes_other_1 = validator(
        "eeg_electrodes_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_sampling_rate_1 = validator(
        "eeg_sampling_rate_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_condition_1 = validator(
        "eeg_condition_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegCondition1))

    _parse_eeg_condition_other_1 = validator(
        "eeg_condition_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_duration_1 = validator(
        "eeg_duration_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n_tasks_1 = validator(
        "eeg_n_tasks_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegNTasks1))

    _parse_eeg_n_tasks_1_more = validator(
        "eeg_n_tasks_1_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_eeg_task_construct_1_1 = validator(
        "eeg_task_construct_1_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct11))

    _parse_eeg_task_construct_other_1_1 = validator(
        "eeg_task_construct_other_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_construct_1_2 = validator(
        "eeg_task_construct_1_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct12))

    _parse_eeg_task_construct_other_1_2 = validator(
        "eeg_task_construct_other_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_construct_1_3 = validator(
        "eeg_task_construct_1_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct13))

    _parse_eeg_task_construct_other_1_3 = validator(
        "eeg_task_construct_other_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_name_1 = validator(
        "eeg_task_name_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_prepro_software_other_1 = validator(
        "eeg_prepro_software_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_acq_protocol_1 = validator(
        "eeg_acq_protocol_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_eeg_acq_protocol_link_1 = validator(
        "eeg_acq_protocol_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_hardware_2 = validator(
        "eeg_hardware_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_software_info_2 = validator(
        "eeg_software_info_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_software_update_2 = validator(
        "eeg_software_update_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_eeg_software_update_when_2 = validator(
        "eeg_software_update_when_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n_channels_2 = validator(
        "eeg_n_channels_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegNChannels2))

    _parse_eeg_n_channels_other_2 = validator(
        "eeg_n_channels_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_electrodes_2 = validator(
        "eeg_electrodes_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegElectrodes2))

    _parse_eeg_electrodes_other_2 = validator(
        "eeg_electrodes_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_sampling_rate_2 = validator(
        "eeg_sampling_rate_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_condition_2 = validator(
        "eeg_condition_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegCondition2))

    _parse_eeg_condition_other_2 = validator(
        "eeg_condition_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_duration_2 = validator(
        "eeg_duration_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n_tasks_2 = validator(
        "eeg_n_tasks_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegNTasks2))

    _parse_eeg_n_tasks_2_more = validator(
        "eeg_n_tasks_2_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_eeg_task_construct_2_1 = validator(
        "eeg_task_construct_2_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct21))

    _parse_eeg_task_construct_other_2_1 = validator(
        "eeg_task_construct_other_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_construct_2_2 = validator(
        "eeg_task_construct_2_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct22))

    _parse_eeg_task_construct_other_2_2 = validator(
        "eeg_task_construct_other_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_construct_2_3 = validator(
        "eeg_task_construct_2_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct23))

    _parse_eeg_task_construct_other_2_3 = validator(
        "eeg_task_construct_other_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_name_2 = validator(
        "eeg_task_name_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_prepro_software_other_2 = validator(
        "eeg_prepro_software_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_acq_protocol_2 = validator(
        "eeg_acq_protocol_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_eeg_acq_protocol_link_2 = validator(
        "eeg_acq_protocol_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_hardware_3 = validator(
        "eeg_hardware_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_software_info_3 = validator(
        "eeg_software_info_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_software_update_3 = validator(
        "eeg_software_update_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_eeg_software_update_when_3 = validator(
        "eeg_software_update_when_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n_channels_3 = validator(
        "eeg_n_channels_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegNChannels3))

    _parse_eeg_n_channels_other_3 = validator(
        "eeg_n_channels_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_electrodes_3 = validator(
        "eeg_electrodes_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegElectrodes3))

    _parse_eeg_electrodes_other_3 = validator(
        "eeg_electrodes_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_sampling_rate_3 = validator(
        "eeg_sampling_rate_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_condition_3 = validator(
        "eeg_condition_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegCondition3))

    _parse_eeg_condition_other_3 = validator(
        "eeg_condition_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_duration_3 = validator(
        "eeg_duration_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_n_tasks_3 = validator(
        "eeg_n_tasks_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegNTasks3))

    _parse_eeg_n_tasks_3_more = validator(
        "eeg_n_tasks_3_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_eeg_task_construct_3_1 = validator(
        "eeg_task_construct_3_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct31))

    _parse_eeg_task_construct_other_3_1 = validator(
        "eeg_task_construct_other_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_construct_3_2 = validator(
        "eeg_task_construct_3_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct32))

    _parse_eeg_task_construct_other_3_2 = validator(
        "eeg_task_construct_other_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_construct_3_3 = validator(
        "eeg_task_construct_3_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(EegTaskConstruct33))

    _parse_eeg_task_construct_other_3_3 = validator(
        "eeg_task_construct_other_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_task_name_3 = validator(
        "eeg_task_name_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_prepro_software_other_3 = validator(
        "eeg_prepro_software_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_eeg_acq_protocol_3 = validator(
        "eeg_acq_protocol_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_eeg_acq_protocol_link_3 = validator(
        "eeg_acq_protocol_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n = validator(
        "fnirs_n",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsN))

    _parse_fnirs_n_more = validator(
        "fnirs_n_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_fnirs_hardware_1 = validator(
        "fnirs_hardware_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_software_info_1 = validator(
        "fnirs_software_info_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_software_update_1 = validator(
        "fnirs_software_update_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_software_update_when_1 = validator(
        "fnirs_software_update_when_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n_channels_1 = validator(
        "fnirs_n_channels_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsNChannels1))

    _parse_fnirs_n_channels_other_1 = validator(
        "fnirs_n_channels_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_detectors_1 = validator(
        "fnirs_detectors_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_sampling_rate_1 = validator(
        "fnirs_sampling_rate_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_condition_1 = validator(
        "fnirs_condition_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsCondition1))

    _parse_fnirs_condition_other_1 = validator(
        "fnirs_condition_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_duration_1 = validator(
        "fnirs_duration_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n_tasks_1 = validator(
        "fnirs_n_tasks_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsNTasks1))

    _parse_fnirs_n_tasks_1_more = validator(
        "fnirs_n_tasks_1_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_fnirs_task_construct_1_1 = validator(
        "fnirs_task_construct_1_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct11))

    _parse_fnirs_task_construct_other_1_1 = validator(
        "fnirs_task_construct_other_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_1_1 = validator(
        "fnirs_task_name_1_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_construct_1_2 = validator(
        "fnirs_task_construct_1_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct12))

    _parse_fnirs_task_construct_other_1_2 = validator(
        "fnirs_task_construct_other_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_1_2 = validator(
        "fnirs_task_name_1_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_construct_1_3 = validator(
        "fnirs_task_construct_1_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct13))

    _parse_fnirs_task_construct_other_1_3 = validator(
        "fnirs_task_construct_other_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_1_3 = validator(
        "fnirs_task_name_1_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_prepro_software_other_1 = validator(
        "fnirs_prepro_software_other_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_acq_protocol_1 = validator(
        "fnirs_acq_protocol_1",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_acq_protocol_link_1 = validator(
        "fnirs_acq_protocol_link_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_hardware_2 = validator(
        "fnirs_hardware_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_software_info_2 = validator(
        "fnirs_software_info_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_software_update_2 = validator(
        "fnirs_software_update_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_software_update_when_2 = validator(
        "fnirs_software_update_when_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n_channels_2 = validator(
        "fnirs_n_channels_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsNChannels2))

    _parse_fnirs_n_channels_other_2 = validator(
        "fnirs_n_channels_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_detectors_2 = validator(
        "fnirs_detectors_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_sampling_rate_2 = validator(
        "fnirs_sampling_rate_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_condition_2 = validator(
        "fnirs_condition_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsCondition2))

    _parse_fnirs_condition_other_2 = validator(
        "fnirs_condition_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_duration_2 = validator(
        "fnirs_duration_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n_tasks_2 = validator(
        "fnirs_n_tasks_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsNTasks2))

    _parse_fnirs_n_tasks_2_more = validator(
        "fnirs_n_tasks_2_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_fnirs_task_construct_2_1 = validator(
        "fnirs_task_construct_2_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct21))

    _parse_fnirs_task_construct_other_2_1 = validator(
        "fnirs_task_construct_other_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_2_1 = validator(
        "fnirs_task_name_2_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_construct_2_2 = validator(
        "fnirs_task_construct_2_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct22))

    _parse_fnirs_task_construct_other_2_2 = validator(
        "fnirs_task_construct_other_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_2_2 = validator(
        "fnirs_task_name_2_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_construct_2_3 = validator(
        "fnirs_task_construct_2_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct23))

    _parse_fnirs_task_construct_other_2_3 = validator(
        "fnirs_task_construct_other_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_2_3 = validator(
        "fnirs_task_name_2_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_prepro_software_other_2 = validator(
        "fnirs_prepro_software_other_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_acq_protocol_2 = validator(
        "fnirs_acq_protocol_2",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_acq_protocol_link_2 = validator(
        "fnirs_acq_protocol_link_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_hardware_3 = validator(
        "fnirs_hardware_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_software_info_3 = validator(
        "fnirs_software_info_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_software_update_3 = validator(
        "fnirs_software_update_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_software_update_when_3 = validator(
        "fnirs_software_update_when_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n_channels_3 = validator(
        "fnirs_n_channels_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsNChannels3))

    _parse_fnirs_n_channels_other_3 = validator(
        "fnirs_n_channels_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_detectors_3 = validator(
        "fnirs_detectors_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_sampling_rate_3 = validator(
        "fnirs_sampling_rate_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_condition_3 = validator(
        "fnirs_condition_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsCondition3))

    _parse_fnirs_condition_other_3 = validator(
        "fnirs_condition_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_duration_3 = validator(
        "fnirs_duration_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_n_tasks_3 = validator(
        "fnirs_n_tasks_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsNTasks3))

    _parse_fnirs_n_tasks_3_more = validator(
        "fnirs_n_tasks_3_more",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_fnirs_task_construct_3_1 = validator(
        "fnirs_task_construct_3_1",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct31))

    _parse_fnirs_task_construct_other_3_1 = validator(
        "fnirs_task_construct_other_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_3_1 = validator(
        "fnirs_task_name_3_1",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_construct_3_2 = validator(
        "fnirs_task_construct_3_2",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct32))

    _parse_fnirs_task_construct_other_3_2 = validator(
        "fnirs_task_construct_other_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_3_2 = validator(
        "fnirs_task_name_3_2",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_construct_3_3 = validator(
        "fnirs_task_construct_3_3",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FnirsTaskConstruct33))

    _parse_fnirs_task_construct_other_3_3 = validator(
        "fnirs_task_construct_other_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_task_name_3_3 = validator(
        "fnirs_task_name_3_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_prepro_software_other_3 = validator(
        "fnirs_prepro_software_other_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_fnirs_acq_protocol_3 = validator(
        "fnirs_acq_protocol_3",
        pre=True,
        allow_reuse=True,
    )(make_bool_validator(False))

    _parse_fnirs_acq_protocol_link_3 = validator(
        "fnirs_acq_protocol_link_3",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_phys_modality_other = validator(
        "phys_modality_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_imaging_comments = validator(
        "imaging_comments",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    @root_validator(pre=True)
    def validate_enum_lists(cls, values):
        values["data_entity"] = parse_enum_list(DataEntity, "data_entity", values)
        values["mr_sequences_1"] = parse_enum_list(
            MrSequences1, "mr_sequences_1", values
        )
        values["mr_prepro_software_1"] = parse_enum_list(
            MrPreproSoftware1, "mr_prepro_software_1", values
        )
        values["mr_rs_prepro_info_1"] = parse_enum_list(
            MrRsPreproInfo1, "mr_rs_prepro_info_1", values
        )
        values["mr_mrs_prepro_info_1"] = parse_enum_list(
            MrMrsPreproInfo1, "mr_mrs_prepro_info_1", values
        )
        values["mr_data_type_1"] = parse_enum_list(
            MrDataType1, "mr_data_type_1", values
        )
        values["mr_distortion_which_1"] = parse_enum_list(
            MrDistortionWhich1, "mr_distortion_which_1", values
        )
        values["mr_sequences_2"] = parse_enum_list(
            MrSequences2, "mr_sequences_2", values
        )
        values["mr_prepro_software_2"] = parse_enum_list(
            MrPreproSoftware2, "mr_prepro_software_2", values
        )
        values["mr_rs_prepro_info_2"] = parse_enum_list(
            MrRsPreproInfo2, "mr_rs_prepro_info_2", values
        )
        values["mr_mrs_prepro_info_2"] = parse_enum_list(
            MrMrsPreproInfo2, "mr_mrs_prepro_info_2", values
        )
        values["mr_data_type_2"] = parse_enum_list(
            MrDataType2, "mr_data_type_2", values
        )
        values["mr_distortion_which_2"] = parse_enum_list(
            MrDistortionWhich2, "mr_distortion_which_2", values
        )
        values["mr_sequences_3"] = parse_enum_list(
            MrSequences3, "mr_sequences_3", values
        )
        values["mr_prepro_software_3"] = parse_enum_list(
            MrPreproSoftware3, "mr_prepro_software_3", values
        )
        values["mr_rs_prepro_info_3"] = parse_enum_list(
            MrRsPreproInfo3, "mr_rs_prepro_info_3", values
        )
        values["mr_mrs_prepro_info_3"] = parse_enum_list(
            MrMrsPreproInfo3, "mr_mrs_prepro_info_3", values
        )
        values["mr_data_type_3"] = parse_enum_list(
            MrDataType3, "mr_data_type_3", values
        )
        values["mr_distortion_which_3"] = parse_enum_list(
            MrDistortionWhich3, "mr_distortion_which_3", values
        )
        values["eeg_paradigm_1"] = parse_enum_list(
            EegParadigm1, "eeg_paradigm_1", values
        )
        values["eeg_prepro_software_1"] = parse_enum_list(
            EegPreproSoftware1, "eeg_prepro_software_1", values
        )
        values["eeg_paradigm_2"] = parse_enum_list(
            EegParadigm2, "eeg_paradigm_2", values
        )
        values["eeg_prepro_software_2"] = parse_enum_list(
            EegPreproSoftware2, "eeg_prepro_software_2", values
        )
        values["eeg_paradigm_3"] = parse_enum_list(
            EegParadigm3, "eeg_paradigm_3", values
        )
        values["eeg_prepro_software_3"] = parse_enum_list(
            EegPreproSoftware3, "eeg_prepro_software_3", values
        )
        values["fnirs_paradigm_1"] = parse_enum_list(
            FnirsParadigm1, "fnirs_paradigm_1", values
        )
        values["fnirs_prepro_software_1"] = parse_enum_list(
            FnirsPreproSoftware1, "fnirs_prepro_software_1", values
        )
        values["fnirs_paradigm_2"] = parse_enum_list(
            FnirsParadigm2, "fnirs_paradigm_2", values
        )
        values["fnirs_prepro_software_2"] = parse_enum_list(
            FnirsPreproSoftware2, "fnirs_prepro_software_2", values
        )
        values["fnirs_paradigm_3"] = parse_enum_list(
            FnirsParadigm3, "fnirs_paradigm_3", values
        )
        values["fnirs_prepro_software_3"] = parse_enum_list(
            FnirsPreproSoftware3, "fnirs_prepro_software_3", values
        )
        values["phys_modality"] = parse_enum_list(PhysModality, "phys_modality", values)

        return values


class DzpgRedcapStudy(BaseModel):
    record_id: str | None = Field(description="Record ID")
    name_study: str = Field(description="Title/name of the study")
    dzpg_site: DzpgSite | None = Field(
        description="DZPG Site (where the study is being conducted or where contacts exist):"
    )
    description_study: str = Field(description="Short description of the study")
    title_pi: TitlePi = Field(description="Title of the PI")
    given_name_pi: str = Field(description="Given name of the PI")
    family_name_pi: str = Field(description="Family name of the PI")
    further_pi: str | None = Field(description="Further PIs")
    contact_mail: str | None = Field(description="E-Mail")
    contact_phone: str | None = Field(description="Phone")
    paper: str | None = Field(
        description="Main (or Study/Project Design) PaperIf possible with link"
    )
    website: str | None = Field(description="Website of the study")
    trial_number: str | None = Field(
        description="If you registered your study/cohort in ClinicalTrials.gov or DRKS, please enter your trial number here:"
    )
    study_status: StudyStatus = Field(description="Overall study status")
    study_status_other: str | None = Field(description="Which other study status?")
    type_intervention: list[TypeIntervention] = Field(
        description="Is it an intervetional or non-interventional study?"
    )
    type_specification_non_int: list[TypeSpecificationNonInt] = Field(
        description="Specification of study type Please indicate all descriptions that apply to your study"
    )
    specific_non_int_other: str | None = Field(description="Which other study type?")
    type_specification_int: list[TypeSpecificationInt] = Field(
        description="Specification of study type Please indicate all descriptions that apply to your study"
    )
    specific_int_other: str | None = Field(description="Which other study type?")
    setting: Setting | None = Field(description="Is it a mono- or multicentric study?")
    n_sites: str | None = Field(description="Number of sites")
    setting_country: SettingCountry | None = Field(
        description="Is it a single or multi country study?"
    )
    n_countries: str | None = Field(description="Number of countries")
    which_country: str | None = Field(description="Which country?")
    which_countries: str | None = Field(description="Which countries?")
    participants: list[Participants] | None = Field(
        description="Who are participants?  Multiple responses possible"
    )
    specific_diseases: list[SpecificDiseases] | None = Field(
        description="Which diseases? (ICD-11 classification)"
    )
    mental_disorders: list[MentalDisorders] | None = Field(
        description="Mental, behavioural or neurodevelopmental disorders (06)(ICD-11 classification)"
    )
    mental_disorders_other: str | None = Field(
        description="Which other mental, behavioural or neurodevelopmental disorder(s)?"
    )
    somatic_disorders: list[SomaticDisorders] | None = Field(
        description="Which other diseases?(ICD-11 classification)"
    )
    disease_other: str | None = Field(description="Which other disease(s)?")
    recruited: list[Recruited] | None = Field(
        description="Recruitment setting Multiple responses possible"
    )
    recruited_other: str | None = Field(description="Which other recruitment settings?")
    n_participants_target: str | None = Field(description="Target sample size")
    n_participants_obtained: str | None = Field(
        description="(Currently) Obtained sample size"
    )
    start_recruitment: str | None = Field(description="Start of recruitment (year)")
    end_recruitment: str | None = Field(description="End of recruitment (year)")
    min_age: str | None = Field(
        description="Minimum age of participants (at recruitment)"
    )
    max_age: str | None = Field(
        description="Maximum age of participants (at recruitment)"
    )
    data_sources: list[DataSources] | None = Field(
        description="Data sources for the study Multiple responses possible"
    )
    data_sources_other: str | None = Field(description="Which other data sources?")
    data_sharing: DataSharing = Field(description="Is it planned to share the data?")
    data_sharing_additional: str | None = Field(
        description="Additional information about data sharing"
    )
    planned_follow_ups: PlannedFollowUps | None = Field(
        description="Are further follow-ups planned?"
    )
    consent_recontact: ConsentRecontact | None = Field(
        description="Is there consent to re-contact participants?"
    )
    further_components: FurtherComponents | None = Field(
        description="Is the implementation of further study components possible?"
    )
    comments: str | None = Field(description="Further comments")
    imaging: DzpgRedcapStudyImaging | None = Field(description="Imaging Module")
    biobank: DzpgRedcapStudyBiobank | None = Field(description="Biobank Module")

    _parse_record_id = validator(
        "record_id",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_name_study = validator(
        "name_study",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_dzpg_site = validator(
        "dzpg_site",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(DzpgSite))

    _parse_description_study = validator(
        "description_study",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_title_pi = validator(
        "title_pi",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(TitlePi))

    _parse_given_name_pi = validator(
        "given_name_pi",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_family_name_pi = validator(
        "family_name_pi",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(True))

    _parse_further_pi = validator(
        "further_pi",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_contact_mail = validator(
        "contact_mail",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_contact_phone = validator(
        "contact_phone",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_paper = validator(
        "paper",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_website = validator(
        "website",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_trial_number = validator(
        "trial_number",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_study_status = validator(
        "study_status",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(StudyStatus))

    _parse_study_status_other = validator(
        "study_status_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_specific_non_int_other = validator(
        "specific_non_int_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_specific_int_other = validator(
        "specific_int_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_setting = validator(
        "setting",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(Setting))

    _parse_n_sites = validator(
        "n_sites",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_setting_country = validator(
        "setting_country",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(SettingCountry))

    _parse_n_countries = validator(
        "n_countries",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_which_country = validator(
        "which_country",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_which_countries = validator(
        "which_countries",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_mental_disorders_other = validator(
        "mental_disorders_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_disease_other = validator(
        "disease_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_recruited_other = validator(
        "recruited_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_n_participants_target = validator(
        "n_participants_target",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_n_participants_obtained = validator(
        "n_participants_obtained",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_start_recruitment = validator(
        "start_recruitment",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_end_recruitment = validator(
        "end_recruitment",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_min_age = validator(
        "min_age",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_max_age = validator(
        "max_age",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_data_sources_other = validator(
        "data_sources_other",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_data_sharing = validator(
        "data_sharing",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(DataSharing))

    _parse_data_sharing_additional = validator(
        "data_sharing_additional",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    _parse_planned_follow_ups = validator(
        "planned_follow_ups",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(PlannedFollowUps))

    _parse_consent_recontact = validator(
        "consent_recontact",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(ConsentRecontact))

    _parse_further_components = validator(
        "further_components",
        pre=True,
        allow_reuse=True,
    )(make_enum_validator(FurtherComponents))

    _parse_comments = validator(
        "comments",
        pre=True,
        allow_reuse=True,
    )(make_text_validator(False))

    @root_validator(pre=True)
    def validate_enum_lists(cls, values):
        values["type_intervention"] = parse_enum_list(
            TypeIntervention, "type_intervention", values
        )
        values["type_specification_non_int"] = parse_enum_list(
            TypeSpecificationNonInt, "type_specification_non_int", values
        )
        values["type_specification_int"] = parse_enum_list(
            TypeSpecificationInt, "type_specification_int", values
        )
        values["participants"] = parse_enum_list(Participants, "participants", values)
        values["specific_diseases"] = parse_enum_list(
            SpecificDiseases, "specific_diseases", values
        )
        values["mental_disorders"] = parse_enum_list(
            MentalDisorders, "mental_disorders", values
        )
        values["somatic_disorders"] = parse_enum_list(
            SomaticDisorders, "somatic_disorders", values
        )
        values["recruited"] = parse_enum_list(Recruited, "recruited", values)
        values["data_sources"] = parse_enum_list(DataSources, "data_sources", values)
        values["imaging"] = parse_optional_survey_module(DzpgRedcapStudyImaging, values)
        values["biobank"] = parse_optional_survey_module(DzpgRedcapStudyBiobank, values)

        return values
