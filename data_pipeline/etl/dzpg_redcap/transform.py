from .dzpg_redcap_study import (
    DataSharing as DzpgDataSharing,
    DzpgRedcapStudy,
    StudyStatus as DzpgStudyStatus,
)
from ..dzg import (
    create_study,
    create_value,
    Study,
    StudyStatus,
    Title,
    Contacts,
    Emails,
    Status,
    DateStart,
    Publish,
    InclusionCriteria,
    ExclusionCriteria,
)


def to_dzg_status(status: DzpgStudyStatus) -> StudyStatus:
    if status in [
        DzpgStudyStatus.AT_THE_PLANNING_STAGE,
        DzpgStudyStatus.ONGOING_I_RECRUITMENT_ONGOING_BUT_DATA_COLLECTION_NOT_YET_STARTED,
        DzpgStudyStatus.ONGOING_II_RECRUITMENT_AND_DATA_COLLECTION_ONGOING,
        DzpgStudyStatus.ONGOING_III_RECRUITMENT_COMPLETED_BUT_DATA_COLLECTION_ONGOING,
        DzpgStudyStatus.ONGOING_IV_RECRUITMENT_AND_DATA_COLLECTION_COMPLETED_BUT_DATA_QUALITY_MANAGEMENT_ONGOING,
    ]:
        return StudyStatus.active
    elif status in [
        DzpgStudyStatus.TERMINATED_RECRUITMENT_DATA_COLLECTION_DATA_AND_QUALITY_MANAGEMENT_HALTED,
        DzpgStudyStatus.COMPLETED_RECRUITMENT_DATA_COLLECTION_AND_DATA_QUALITY_MANAGEMENT_COMPLETED,
    ]:
        return StudyStatus.closed
    else:
        return StudyStatus.unknown


def to_dzg(study: DzpgRedcapStudy) -> Study:
    result = create_study(
        id=study.record_id,
        title=create_value(Title, study.name_study),
        contacts=create_value(
            Contacts,
            [f"{study.title_pi.value} {study.given_name_pi} {study.family_name_pi}"],
        ),
        emails=create_value(Emails, [study.contact_mail] if study.contact_mail else []),
        status=create_value(Status, to_dzg_status(study.study_status)),
        date_start=create_value(DateStart, study.start_recruitment),
        publish=create_value(
            Publish,
            study.data_sharing
            == DzpgDataSharing.YES_THERE_IS_A_PLAN_TO_MAKE_DATA_AVAILABLE,
        ),
        inclusion_criteria=create_value(InclusionCriteria, []),
        exclusion_criteria=create_value(ExclusionCriteria, []),
    )
    return result
