from os import getenv

import requests

REDCAP_API_KEY = getenv("DZPG_REDCAP_API_KEY")


def get_metadata():
    data = {
        "token": REDCAP_API_KEY,
        "content": "metadata",
        "format": "json",
        "returnFormat": "json",
    }
    r = requests.post("https://redcap.uk-halle.de/api/", data=data)
    r.raise_for_status()
    response: list[dict[str, str]] = r.json()

    return response


def get_records():
    data = {
        "token": REDCAP_API_KEY,
        "content": "record",
        "format": "json",
        "returnFormat": "json",
    }
    r = requests.post("https://redcap.uk-halle.de/api/", data=data)
    r.raise_for_status()
    response: list[dict[str, str]] = r.json()

    return response
