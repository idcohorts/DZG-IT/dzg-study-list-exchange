import datetime
from email.utils import parseaddr
import re
from urllib.parse import urlparse

import structlog
import dateparser
import pandas as pd

from .dzpg_redcap_study import DzpgRedcapStudy
from .api import get_records
from ..util import semicolon_separated

logger = structlog.stdlib.get_logger()


def parse_person_names(s: str) -> list[str]:
    return semicolon_separated(s)


def parse_emails(emails: str) -> list[str]:
    return list(
        filter(
            lambda email: email != "",
            map(
                lambda email: email[1],
                map(
                    lambda possible_email: parseaddr(possible_email),
                    re.split("[,;]", emails),
                ),
            ),
        )
    )


def parse_date_start(date_start: str | int | datetime.datetime) -> datetime.date | None:
    if pd.isnull(date_start) or date_start == "":
        return None

    if isinstance(date_start, datetime.datetime):
        return date_start.date()

    if isinstance(date_start, int):
        if date_start >= 1900 and date_start <= 2400:
            return datetime.date(date_start, 1, 1)
        else:
            raise Exception(f"Couldn't convert date_start: {date_start}")

    date_start = date_start.strip()
    if date_start == "":
        return None

    dt = dateparser.parse(
        date_start, locales=["en", "de"], settings={"PREFER_DAY_OF_MONTH": "first"}
    )
    if dt is None:
        raise Exception(f"Couldn't convert date_start: {date_start}")
    return dt


def is_valid_url(url: str) -> bool:
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


def parse_dzpg_redcap_api(records: list[dict[str, str]]):
    for record in records:
        try:
            study = DzpgRedcapStudy.parse_obj(record)

            if study.website and study.website.startswith("www."):
                study.website = f"https://{study.website}"

            if study.website and not is_valid_url(study.website):
                logger.warn(f"Invalid URL: {study.website}", record_id=study.record_id)
                study.website = None

            if study.contact_mail:
                study.contact_mail = parseaddr(study.contact_mail)[1] or None

            yield study
        except Exception as e:
            logger.warn(f"Parse error: {e}", record_id=record["record_id"])
