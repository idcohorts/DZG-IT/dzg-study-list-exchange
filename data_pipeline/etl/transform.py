from pathlib import Path
from .dzg_xls.parse import parse_dzg_xls, series_to_dzg_xls
from .dzpg_redcap.parse import parse_dzpg_redcap_api
from .dzpg_redcap.serialize import serialize
from collections.abc import Callable
import contextlib, sys
import pandas as pd
import numpy as np
import ujson as json
from pydantic import BaseModel
from os import path
from .dzg import Study
from .dzg_xls import to_dzg
from .nfdi4health import to_nfdi4health, prepare_data, upload
from .dzpg_redcap import to_dzg as dzpg_to_dzg, DzpgRedcapStudy
import structlog
from .clinicaltrials.download import download_studies
from .clinicaltrials.transform import to_dzg as clinicaltrials_to_dzg

EXAMPLE_ID = "XYZ1234576"

logger = structlog.get_logger()


def writer(fh):
    @contextlib.contextmanager
    def stdout():
        yield sys.stdout

    return open(fh, "w") if fh else stdout()


def write_dzg_xls_studies(outdir: str, row: pd.Series, converter):
    # this is the example id that is being used in all files. just ignore
    if row.Studienregisternummer == EXAMPLE_ID:
        return

    structlog.contextvars.bind_contextvars(row=row.name)
    try:
        study = series_to_dzg_xls(row)
    except Exception as e:
        logger.warn(f"Parse error: {e}")
        return
    try:
        dzg = converter(study)
    except Exception as e:
        logger.warn(f"Conversion error: {e}")
        return

    with open(path.join(outdir, f"{row.Studienregisternummer}.json"), "w+") as fp:
        fp.write(dzg.json(exclude_none=True))


def transform_dzg_xls_to_dzg(dzg_name, filepath, outdir):
    df = parse_dzg_xls(filepath)
    df.apply(
        lambda series: write_dzg_xls_studies(
            outdir,
            series,
            lambda dzg_xls_study: to_dzg(dzg_name, filepath, dzg_xls_study),
        ),
        axis=1,
    )


def write_dzpg_json_study(
    outdir: Path,
    study: DzpgRedcapStudy,
    converter: Callable[[DzpgRedcapStudy], BaseModel],
):
    logger = structlog.stdlib.get_logger().bind(study=study)

    try:
        dzpg = converter(study)
    except Exception as e:
        logger.warn(f"Conversion error: {e}")
        return

    with open(outdir / f"{study.record_id}.json", "w+") as fp:
        fp.write(dzpg.json(exclude_none=True))


def transform_dzpg_json_to_dzg(infile: Path, outdir: Path):
    records: list[dict[str, str]] = json.loads(infile.read_text())

    for study in parse_dzpg_redcap_api(records):
        write_dzpg_json_study(
            outdir,
            study,
            dzpg_to_dzg,
        )


def serialize_dzpg_redcap_api(infile: Path, outdir: Path):
    records: list[dict[str, str]] = json.loads(infile.read_text())

    for study in parse_dzpg_redcap_api(records):
        with open(outdir / f"{study.record_id}.json", "w+") as fp:
            json.dump(serialize(study), fp)


def transform_dzg_to_nfdi4health(filepath, outfile, pretty):
    study = Study.parse_file(filepath)
    args = {"exclude_none": True}
    if pretty:
        args["indent"] = 2
    resource = to_nfdi4health(study)
    with writer(outfile) as w:
        w.write(resource.json(**args))


def transform_clinicaltrials_expr_to_dzg(expr, outdir):
    studies = download_studies(expr)
    for id, study in studies:
        structlog.contextvars.bind_contextvars(id=id)
        try:
            dzg = clinicaltrials_to_dzg(study)
            with open(path.join(outdir, f"{dzg.id}.json"), "w+") as fp:
                fp.write(dzg.json(exclude_none=True))
        except Exception as e:
            logger.warn(f"Conversion error: {e}")


def convert_dzg_xls_to_json(sourcedir, outdir, validate_id):
    prepare_data(sourcedir, outdir, validate_id)


def upload_studies_to_csh(config_path):
    upload(config_path)
    