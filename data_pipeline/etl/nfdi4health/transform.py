from .model import *
from ..dzg import Study as DzgStudy, Person
from nameparser import HumanName
import structlog

logger = structlog.get_logger()


def find_language(study: DzgStudy):
    # no magic so far. DRK seems to be some german identifier
    if study.id.startswith("DRK"):
        return LanguageNamesFromTheISO6391List.DE__German_
    else:
        return LanguageNamesFromTheISO6391List.EN__English_


def english_title(study: DzgStudy):
    if study.id.startswith("DRK"):
        return "NO ENGLISH TITLE AVAILABLE"
    else:
        return study.title.value
       

def given_name(name: HumanName) -> str:
    given = name.first
    middle = name.middle
    if len(middle) > 0:
        given += f" {middle}"
    return given


def role_name_personal(person: Person) -> Personal:
    return Personal(
        type=Type2.Contact,
        givenName=given_name(person.name),
        familyName=person.name.last,
    )


def role(person: Person) -> Contributor:
    personal_obj = role_name_personal(person)
    return Contributor(
        email=person.email,
        nameType=NameType.Personal,
        personal=personal_obj,
    )


def bullet_point(criteria: str) -> str:
    return f"- {criteria}"


def to_nfdi4health(study: DzgStudy) -> Resource:
    id = study.id.strip()
    if len(id) == 0:
        raise Exception(f"Studienregisternummer is empty")
    print(f"Transforming file: {id}.json")
    classification = Classification(
        type=Type.Study,
        typeGeneral=None
    )
    if classification.type == Type.Study:
        nutritionalData = False
        print("No parseable info if study collects nutritional data - interpreting as false")
    else:   
        nutritionalData = None

    titles = [
        Title(text=study.title.value, language=find_language(study))
    ]
    
    title = english_title(study)
    language = LanguageNamesFromTheISO6391List.EN__English_
    # descriptions should be list of type Description, but validator throws error because a dict is expected, the following works for some reason
    descriptions = [
            {
            "text": title,
            "language": language
            }
    ]
    
    persons = list(
        filter(
            lambda person: person.name is not None and person.email is not None,
            study.persons
        )
    )
    if len(persons) != len(study.persons):
        logger.warn(
            f"study {study.id} has a mismatching count of contacts and emails. ignoring {len(study.persons)-len(persons)} additional entries. {len(persons)} entries are complete."
        )

    contributors = list(
        map(
            role,
            persons,
        )
    )
    if len(contributors) == 0:
        raise Exception(
            f"study {study.id} doesn't contain any persons. mandatory for nfdi4health"
        )

    provenance = Provenance(dataSource=DataSource.Manually_collected)
    inclusion_criteria = "\n".join(map(bullet_point, study.inclusion_criteria.value))
    exclusion_criteria = "\n".join(map(bullet_point, study.exclusion_criteria.value))
    study_eligibility_criteria = EligibilityCriteria(
        inclusionCriteria=inclusion_criteria,
        exclusionCriteria=exclusion_criteria,
    )
    study_data_sharing_plan_generally = (
        Generally.Yes__there_is_a_plan_to_make_data_available
        if study.publish
        else Generally.No__there_is_no_plan_to_make_data_available
    )
    study_design = Design(
        # https://gitlab.com/idcohorts/DZG-IT/dzg-study-list-exchange/-/issues/17
        primaryDesign=PrimaryDesign(PrimaryDesign.Interventional),
        studyType=StudyType(
            interventional=[
                InterventionalEnum.Unknown
            ]
        ),
        groupsOfDiseases=GroupsOfDiseases(
            generally=[
                GenerallyEnum.Unknown
            ]
        ),
        administrativeInformation=AdministrativeInformation(status=Status.Other),
        population=Population(
            countries=[
            CountryNamesFromTheISO31661List.Germany
            ]
        ),
        eligibilityCriteria=study_eligibility_criteria,
        subject=Subject(Subject.Unknown),
        dataSharingPlan=DataSharingPlan(
            generally=study_data_sharing_plan_generally
        ),
    )
    resource = Resource(
        identifier=study.id,
        classification=classification,
        titles=titles,
        descriptions=descriptions,
        contributors=contributors,
        provenance=provenance,
        nutritionalData=nutritionalData,
        design=study_design,
    ) 
    return resource
