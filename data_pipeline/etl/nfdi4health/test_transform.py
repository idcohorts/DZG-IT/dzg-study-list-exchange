from typing import get_type_hints
from .model import LanguageNamesFromTheISO6391List
from .transform import english_title, find_language, given_name
from ..dzg import (
    create_value,
    Study as DzgStudy,
    Title,
    Contacts,
    Emails,
    Status,
    StudyStatus,
    DateStart,
    Publish,
    InclusionCriteria,
    ExclusionCriteria,
    ExternalLinks,
)
from nameparser import HumanName


def create_study(**kwargs) -> DzgStudy:
    model_values = {
        "id": "",
        "title": create_value(Title, ""),
        "contacts": create_value(Contacts, []),
        "emails": create_value(Emails, []),
        "status": create_value(Status, StudyStatus.active),
        "date_start": create_value(DateStart, None),
        "publish": create_value(Publish, True),
        "inclusion_criteria": create_value(InclusionCriteria, []),
        "exclusion_criteria": create_value(ExclusionCriteria, []),
        "external_links": create_value(ExternalLinks, []),
    }
    for k, v in kwargs.items():
        if k == "id":
            model_values[k] = v
        else:
            model_values[k].value = v
    return DzgStudy(**model_values)


def test_english_title_drk():
    study = create_study(id="DRK1234")
    title = english_title(study)
    assert title == "NO ENGLISH TITLE AVAILABLE"


def test_english_title_the_rest():
    study = create_study(id="PENG", title="PENG")
    title = english_title(study)
    assert title == "PENG"


def test_find_language_drk():
    study = create_study(id="DRK1234")
    language = find_language(study)
    assert language == LanguageNamesFromTheISO6391List.DE__German_


def test_find_language_the_rest():
    study = create_study(id="PENG1234")
    language = find_language(study)
    assert language == LanguageNamesFromTheISO6391List.EN__English_


# set inactive since MDS version 3.3 does not require/ask for title
"""
def test_title_prof_dr():
    title = "Prof. Dr."
    model_title = personal_title(title)
    assert model_title == RoleNamePersonalTitle.Prof__Dr_


def test_title_dr():
    title = "Dr."
    model_title = personal_title(title)
    assert model_title == RoleNamePersonalTitle.Dr_


def test_title_the_rest():
    title = "Sir"
    model_title = personal_title(title)
    assert model_title == RoleNamePersonalTitle.Other
"""

def test_given_name_only_firstname():
    name = HumanName("Hans Wurst")
    given = given_name(name)
    assert given == "Hans"


def test_given_name_incl_middle():
    name = HumanName("Hans Peter Wurst")
    given = given_name(name)
    assert given == "Hans Peter"
