import requests
import os
import pathlib
from json import load, dump


def get_token(token_url, data):
    r_token = requests.post(token_url, data=data)
    r_token.raise_for_status()
    token = r_token.json()["access_token"]
    if token:
        print("Token obtained successfully")
    else:
        print("Couldn't obtain bearer token.")

    return token


def get_header(token):
    return {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Bearer " + token,
    }


def upload_request(upload_url, headers, resource):
    r = requests.post(upload_url, headers=headers, json=resource)
    print("Status code upload request: ", r.status_code)
    return r


def publish_request(publish_url, headers):
    r = requests.post(publish_url, headers=headers)
    print("Status code publish request: ", r.status_code)
    return r


def upload(config_path):
    # load config
    config_path = pathlib.Path(config_path)
    with open(config_path, "r") as f:
        config = load(f)

    # components for bearer token request
    client_id = config["CLIENT_ID"]
    secret = config["SECRET"]
    data = {
        "client_id": client_id,
        "client_secret": secret,
        "grant_type": "client_credentials",
    }
    token_url = config["TOKEN_URL"]

    # obtain bearer token
    token = get_token(token_url, data)

    # auth header
    headers = get_header(token)

    # id map
    id_map = {}
    # specify file(s) to be sent/validated
    root = pathlib.Path(__file__).parent.parent.parent.parent
    source = config["SOURCE_DIR"]
    file_list = [f for f in os.listdir(source)]
    for filename in file_list:
        current_file = root / source / filename
        with open(current_file, "r") as resource:
            json = load(resource)

        # request data
        resource = {
            "resource": json,
        }
        upload_url = config["UPLOAD_URL"]

        # upload file
        print("Upload url: ", upload_url)
        resource_id = resource["resource"]["identifier"]
        print("Resource: ", resource_id)
        response = upload_request(upload_url, headers, resource)
        content = response.json()

        # request new token if invalid or expired
        if "error" in content and content["error"] == "invalid_token":
            print("Token expired, refreshing ...")
            token = get_token(token_url, data)
            headers = get_header(token)
            response = upload_request(upload_url, headers, resource)
            content = response.json()

        # request publication of resource
        if response.status_code in [200, 201]:
            print("response: ", content)

            # publish file
            nfdi_id = content["resource"]["identifier"]
            id_map[resource_id] = nfdi_id
            publish_url = f"{config['UPLOAD_URL']}/{nfdi_id}/publish"
            print("Publish url: ", publish_url)
            publish_response = publish_request(publish_url, headers)
            print("Publish response: ", publish_response.json())

    # write id map to file
    with open("id_map.json", "w") as f:
        dump(id_map, f)
