import pandas as pd
import numpy as np
import os
import pathlib
import uuid
from json import dump
from hashlib import sha256
import datetime
import dateparser
from ..util import split_string, is_empty
from ..dzg_xls.parse import parse_publish

output_suffix = ".json"

slot_dict = {
    "Studienregisternummer" : "id",
    "Titel" : "title",
    "Ansprechpartner:in" : "contacts",
    "E-Mail Kontakt Ansprechpartner:innen" : "emails",
    "Status" : "status",
    "Start der Studie" : "date_start",
    "Veröffentlichung in einem DZG Register" : "publish",
    "Einschlusskriterien" : "inclusion_criteria",
    "Ausschlusskriterien" : "exclusion_criteria",
    "Weiterführende Links" : "external_links"
}

keys_expecting_list = ["contacts", "emails", "inclusion_criteria", "exclusion_criteria", "external_links"]

dzg_list = ["DKTK", "DZHK", "DZL", "DZD", "DZIF", "DZNE", "DZPG", "DZKJ"]

log_data = []

invalid_filename_characters = ["(", ")", "[", "]", "{", "}", ".", ",", ";", ":", "/", "\\", "|", "?", "!", "@", "#", "$", "%", "^", "&", "*", "~", "`", "+", "=", "<", ">"]

def generate_uuid():
    return str(uuid.uuid4())

def create_hash_from_title(row, filename):
    # get title and contacts from row
    title = row["title"]
    contacts = row["contacts"]
    # handle contact data (should be list), log if missing
    if pd.isnull(contacts) or not contacts:
        if title:
            msg = f"study '{title}' has no contacts - skipping"
        else:
            msg = f"study '{row['id']}' has no contacts - skipping"
        contacts = []
    else:
        msg = ""
        contacts = [entry.strip() for entry in split_string(contacts)]

    if msg:
        study_data = {"title": title, "contacts": contacts, "warning": msg, "source": filename}
        log_data.append(study_data)
        # warn on console
        print("WARNING: " + msg)
    
    # create hash for id
    if pd.isna(row['id']) or str(row['id']).strip() == "n.a.":      # hotfix: few studies have "n.a." as id
        hash_string = sha256(title.encode("utf-8")).hexdigest()
    
        # detect dzg name for id hash
        dzg_name = "Unknown"
        for dzg in dzg_list:
            if dzg in filename:
                dzg_name = dzg
                break
    
        row['id'] = f"DZG-{dzg_name}-{hash_string}"
    return row 

def check_study_id(study_id, filename):
    is_valid = True
    for char in invalid_filename_characters:
        if char in study_id:
            is_valid = False
            msg = f"Study '{study_id}' has a least one invalid character ('{char}') in its id - skipping"
            log_data.append({"title": study_id, "warning": msg, "source": filename})
            # warn on console
            print("WARNING: " + msg)
            break
    return is_valid

def unflatten_dict(key, value, file):
    if key in keys_expecting_list:
        if not value:
            return {"value": [], "source_ref": f"/source/{file}"}
        else:
            listed = [entry.strip() for entry in split_string(value)]            
            return {"value": listed, "source_ref": f"/source/{file}"}
    else:
        return {"value": value, "source_ref": f"/source/{file}"}

def parse_date_start(date_start: str | int | datetime.datetime, study_title) -> str | None:
    if pd.isnull(date_start):
        return None

    if isinstance(date_start, datetime.datetime):
        return date_start.date().strftime("%Y-%m-%d")

    if isinstance(date_start, int):
        if date_start >= 1900 and date_start <= 2400:
            return datetime.date(date_start, 1, 1).strftime("%Y-%m-%d")
        else:
            raise Exception(f"Couldn't convert date_start: {date_start}")

    date_start = date_start.strip()
    if date_start == "":
        return None
    
    # debug: handle weird inputs in DZL and DZNE (hotfix)
    if date_start in ["startet demnächst", "12.01.2024 (Ethikvotum)", "voraussichtlich Q4 2024", "geplant 08/2024", "Geplanter Beginn: 01.06.2024", "geplant", "ausstehend"]:
        msg = f"study '{study_title}': no valid input for date_start '{date_start}' - interpreting as None"
        print("WARNING: " + msg)
        return None
    
    dt = dateparser.parse(
        date_start, locales=["en", "de"], settings={"PREFER_DAY_OF_MONTH": "first"}
    )
    if dt is None:
        raise Exception(f"Couldn't convert date_start: {date_start}")
    return dt.strftime("%Y-%m-%d")

def parse_status(status):
    if status == "aktiv":
        return "active"
    elif status == "geschlossen":
        return "closed"
    else:
        return "unknown"

def prepare_file(df, filename, validate_id=False):
    print("Preparing: " + filename + "...")
    # find line starting with "Studienregisternummer"
    first_col = df[df.columns[0]]
    start_ind = first_col[first_col.astype(str).str.startswith("Studienregistern")].index[0]

    # delete obsolete rows and reset index
    df = df.iloc[start_ind:]
    df.reset_index(drop=True, inplace=True)

    # set first row as header, drop index row
    df.columns = df.iloc[0]
    df = df[1:]
    
    # set english column names 
    df.rename(columns=lambda x: x.strip(), inplace=True)
    df.rename(columns=slot_dict, inplace=True)

    # ensure there are no extra columns and external_links column is added if non-existent
    bad_columns = [column for column in df.columns if column not in slot_dict.values()]
    df = df.drop(columns=bad_columns)
    if "external_links" not in df.columns:
        df["external_links"] = ""

    # remove example rows
    df = df[~df.id.str.startswith("XYZ123", na=False)]
    df['title'] = df['title'].apply(lambda x: "" if pd.isnull(x) else x)
    df = df[~df.title.str.startswith("Mustertitel")]

    if validate_id:
        # TODO: nothing is applied here, make sure id hash is returned and contacts are turned into list
        # create hash for studies missing id, log missing contacts
        # df[df['id'].isnull()].apply(create_hash_from_title, axis=1, filename=filename)
        df = df.apply(lambda row: create_hash_from_title(row, filename), axis=1)
    else:
        # generate uuid for studies without id
        df['id'] = df['id'].apply(lambda x: generate_uuid() if pd.isnull(x) else x)

    df = df.fillna("")

    return df


def prepare_data(sourcedir, outdir, validate_id):
    file_list = [file for file in os.listdir(sourcedir) if file.endswith(".xlsx")]
    sourcedir = pathlib.Path(sourcedir)

    for file in file_list:        
        filepath = pathlib.Path.joinpath(sourcedir, file)
        df = pd.read_excel(filepath, header=None)
    
        # clean data
        data = prepare_file(df, file, validate_id)
        
        # NFDI4Health pre-format
        data_as_dict = data.to_dict(orient='records')
        for study in data_as_dict:
            # make sure dt objects are converted to strings before checking id, so data is json serializable
            study["date_start"] = parse_date_start(study["date_start"], study["title"])

            # skip study if 'contacts' is empty
            if is_empty(study['contacts']):
                continue

            # check if any dzg is in filename and add to data if so    
            for dzg in dzg_list:
                if dzg in file:
                    study["source"] = unflatten_dict(key="source", value=dzg, file=os.path.basename(__file__))
        
            # NFDI4Health pre-format
            for key in study:
                if key == "status":
                    study[key] = parse_status(study[key])
                if key == "publish":
                    study[key] = parse_publish(study[key])
                if key not in ["id", "source"]:
                    value = study[key]
                    study[key] = unflatten_dict(key, value, file)
            
            # make sure study_id has no invalid characters, otherwise skip study
            study_id = study["id"].strip().replace(" ", "_")
            is_valid_study_id = check_study_id(study_id, file)
            if not is_valid_study_id:
                continue

            # write study file        
            outfile = pathlib.Path(outdir).joinpath((study_id + output_suffix))
            with open(outfile, 'w', encoding='utf-8') as study_file:
                dump(study, study_file, ensure_ascii=False, indent=2)
    
    # write logfile
    timestamp = datetime.datetime.now().strftime("%Y-%m-%d__%H_%M_%S")
    with open(f"logfile_data-prep_{timestamp}.json", "w") as lf:
        dump(log_data, lf, ensure_ascii=False, indent=2)
