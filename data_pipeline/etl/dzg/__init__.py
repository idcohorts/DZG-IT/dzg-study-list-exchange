from .model import *
from .study import *
from .person import Person
from urllib.parse import quote


def create_study(**kwargs):
    external_links = []
    id = kwargs["id"]
    if id.startswith("NCT"):
        external_links.append(f"https://clinicaltrials.gov/ct2/show/{quote(id)}")
    elif id.startswith("DRK"):
        external_links.append(f"https://drks.de/search/de/trial/{quote(id)}")
    kwargs["external_links"] = ExternalLinks(
        value=external_links, source_ref="/source/0"
    )
    return Study(**kwargs)


def create_value(Type, value):
    return Type(source_ref="/source/0", value=value)
