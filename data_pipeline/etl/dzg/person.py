from nameparser import HumanName


class Person:
    def __init__(self, name: HumanName | None, email: str | None):
        self.name = name
        self.email = email
