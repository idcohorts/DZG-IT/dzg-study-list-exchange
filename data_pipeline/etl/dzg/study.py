from .model import Study as BaseStudy
from .parse import parse_persons
from .person import Person


class Study(BaseStudy):
    @property
    def persons(self) -> list[Person]:
        contacts = list(map(lambda v: v, self.contacts.value))
        emails = list(map(lambda v: v, self.emails.value))
        return parse_persons(contacts, emails)
