from .parse import *


def test_parse_title():
    persons = parse_person_names(["Dr. Peter Wurst"])
    assert len(persons) == 1
    assert str(persons[0]) == "Dr. Peter Wurst"
    assert persons[0].title == "Dr."


def test_parse_lowercase_title():
    persons = parse_person_names(["dr. Peter Wurst"])
    assert len(persons) == 1
    assert str(persons[0]) == "Dr. Peter Wurst"
    assert persons[0].title == "Dr."


def test_swapped_name():
    persons = parse_person_names(["Leuchte, Hanno Prof. Dr."])
    assert len(persons) == 1
    assert str(persons[0]) == "Prof. Dr. Hanno Leuchte"
    assert persons[0].title == "Prof. Dr."
    assert persons[0].first == "Hanno"
    assert persons[0].last == "Leuchte"


def test_parse_persons_nothing():
    persons = parse_persons([], [])
    assert len(persons) == 0


def test_parse_persons_one_email_one_contact():
    persons = parse_persons(["Dr. Jutta Jürgens"], ["jutta.juergens@example.com"])
    assert len(persons) == 1
    assert persons[0].name == "Dr. Jutta Jürgens"
    assert persons[0].email == "jutta.juergens@example.com"


def test_parse_persons_one_email_no_contact():
    persons = parse_persons([], ["jutta.juergens@example.com"])
    assert len(persons) == 1
    assert persons[0].name == None
    assert persons[0].email == "jutta.juergens@example.com"


def test_parse_persons_no_email_one_contact():
    persons = parse_persons(["Dr. Jutta Jürgens"], [])
    assert len(persons) == 1
    assert persons[0].name == "Dr. Jutta Jürgens"
    assert persons[0].email == None


def test_parse_persons_no_magic_so_far():
    persons = parse_persons(
        ["Dr. Jutta Jürgens", "Martin Klein"],
        ["martin.klein@example.com", "jutta1@example.org"],
    )
    assert len(persons) == 2
    # no magic name matching so far
    assert persons[0].name == "Dr. Jutta Jürgens"
    assert persons[0].email == "martin.klein@example.com"
    assert persons[1].name == "Martin Klein"
    assert persons[1].email == "jutta1@example.org"
