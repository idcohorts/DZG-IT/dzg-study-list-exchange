from nameparser import HumanName
from itertools import zip_longest

from .person import Person


def parse_person_names(p: list[str]) -> list[HumanName]:
    persons = []
    for person in p:
        p = HumanName(person)
        if str(p) != "":
            p.capitalize(force=True)
            # special case for "Hansen, Hans Dr."
            if p.title == "":
                titles = []
                if p.middle.endswith("."):
                    titles.append(p.middle)
                    p.middle = ""
                if p.suffix.endswith("."):
                    titles.append(p.suffix)
                    p.suffix = ""
                if len(titles) > 0:
                    p.title = " ".join(titles)

            persons.append(p)
    return persons


def parse_persons(names: list[str], emails: list[str]) -> list[Person]:
    names = parse_person_names(names)

    # for now make it super simple. assume things are in order. no name matching or some other magic so far
    persons = []
    for name, email in zip_longest(names, emails, fillvalue=None):
        persons.append(Person(name, email))

    return persons
