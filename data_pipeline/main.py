#!/usr/bin/env python3

import json
from pathlib import Path

import click
from .etl import (
    transform_dzg_xls_to_dzg,
    transform_dzpg_json_to_dzg,
    serialize_dzpg_redcap_api,
    transform_dzg_to_nfdi4health,
    transform_clinicaltrials_expr_to_dzg,
    convert_dzg_xls_to_json,
    upload_studies_to_csh    
)
from .etl.dzpg_redcap.api import get_records as get_dzpg_redcap_api_records
import structlog


@click.group()
@click.option("--json", is_flag=True)
def cli(json):
    if json:
        structlog.configure(
            processors=[
                structlog.contextvars.merge_contextvars,
                structlog.processors.JSONRenderer(),
            ]
        )


@cli.command()
@click.argument("dzg-name")
@click.argument("infile")
@click.argument("outdir")
def dzg(dzg_name, infile, outdir):
    """
    Converts a well formed DZG XLS file (INFILE)
    to a list of studies in DZG json format in OUTDIR
    """
    Path(outdir).mkdir(parents=True, exist_ok=True)

    transform_dzg_xls_to_dzg(dzg_name, infile, outdir)


@cli.command("dzpg-load-from-redcap")
@click.argument(
    "outfile",
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        writable=True,
        resolve_path=True,
        path_type=Path,
    ),
)
def dzpg_load_from_redcap(outfile: Path):
    """
    Queries the DZPG REDCap API and stores the results in OUTFILE.
    The result is a JSON file with the raw API response.
    Requires the `DZPG_REDCAP_API_KEY` environment variable to be set.
    """
    outfile.parent.mkdir(parents=True, exist_ok=True)
    records = get_dzpg_redcap_api_records()
    outfile.write_text(json.dumps(records))


@cli.command("dzpg")
@click.argument(
    "infile",
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        exists=True,
        readable=True,
        resolve_path=True,
        path_type=Path,
    ),
)
@click.argument(
    "outdir",
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        writable=True,
        resolve_path=True,
        path_type=Path,
    ),
)
def dzpg(infile: Path, outdir: Path):
    """
    Parses the DZPG REDCap API results from the `infile`, converts them to the DZPG format,
    and stores the resulting JSON files in `outdir`.
    """
    outdir.mkdir(parents=True, exist_ok=True)
    serialize_dzpg_redcap_api(infile, outdir)


@cli.command("dzpg-to-dzg")
@click.argument(
    "infile",
    type=click.Path(
        file_okay=True,
        dir_okay=False,
        exists=True,
        readable=True,
        resolve_path=True,
        path_type=Path,
    ),
)
@click.argument(
    "outdir",
    type=click.Path(
        file_okay=False,
        dir_okay=True,
        writable=True,
        resolve_path=True,
        path_type=Path,
    ),
)
def dzpg_to_dzg(infile: Path, outdir: Path):
    """
    Parses the DZPG REDCap API results from the `infile`, converts them to the common DZG format,
    and stores the resulting JSON files in `outdir`.
    """
    outdir.mkdir(parents=True, exist_ok=True)
    transform_dzpg_json_to_dzg(infile, outdir)


@cli.command()
@click.argument("infile")
@click.argument("outfile")
@click.option("--pretty", is_flag=True)
def nfdi4health(infile, outfile, pretty):
    """
    Converts a well formed DZG file (INFILE)
    to NFDI4Health json format. Outputs into outfile or stdout
    """
    transform_dzg_to_nfdi4health(infile, outfile, pretty)


@cli.command()
@click.argument("expr")
@click.argument("outdir")
def clinicaltrials_expr(expr, outdir):
    """
    Converts a clinicaltrials search
    to a list of studies in DZG json format in OUTDIR
    """
    transform_clinicaltrials_expr_to_dzg(expr, outdir)


@cli.command()
@click.argument("sourcedir")
@click.argument("outdir")
@click.option("--validate_id", is_flag=True)
def data_prep(sourcedir, outdir, validate_id):
    """
    Converts DZG XLS files (from SOURCEDIR)
    to well formed json files (OUTDIR), each study to single json file in OUTDIR directory
    """
    Path(outdir).mkdir(parents=True, exist_ok=True)
    convert_dzg_xls_to_json(sourcedir, outdir, validate_id)


@cli.command()
@click.argument("config_path")
def nfdi_upload(config_path):
    """
    Uploads studies (in nfdi4health json format) to NFDI4Health Study Hub (https://csh.nfdi4health.de/)
    """
    upload_studies_to_csh(config_path)
    

if __name__ == "__main__":
    cli()
