from __future__ import annotations 
from datetime import (
    datetime,
    date
)
from decimal import Decimal 
from enum import Enum 
import re
import sys
from typing import (
    Any,
    List,
    Literal,
    Dict,
    Optional,
    Union
)
from pydantic.version import VERSION  as PYDANTIC_VERSION 
if int(PYDANTIC_VERSION[0])>=2:
    from pydantic import (
        BaseModel,
        ConfigDict,
        Field,
        field_validator
    )
else:
    from pydantic import (
        BaseModel,
        Field,
        validator
    )

metamodel_version = "None"
version = "None"


class WeakRefShimBaseModel(BaseModel):
    __slots__ = '__weakref__'

class ConfiguredBaseModel(WeakRefShimBaseModel,
                validate_assignment = True,
                validate_all = True,
                underscore_attrs_are_private = True,
                extra = "forbid",
                arbitrary_types_allowed = True,
                use_enum_values = True):
    pass


class LanguageOptions(str, Enum):
    DE_LEFT_PARENTHESISGermanRIGHT_PARENTHESIS = "DE (German)"
    ES_LEFT_PARENTHESISSpanishRIGHT_PARENTHESIS = "ES (Spanish)"
    FR_LEFT_PARENTHESISFrenchRIGHT_PARENTHESIS = "FR (French)"
    Other = "Other"


class RoleNameTypeOptions(str, Enum):
    Personal = "Personal"
    Organisational = "Organisational"


class SchemeOptions(str, Enum):
    ROR = "ROR"
    GRID = "GRID"
    ISNI = "ISNI"


class TypeOptions(str, Enum):
    DOI = "DOI"
    URL = "URL"
    arXiv = "arXiv"
    EAN13 = "EAN13"
    EISSN = "EISSN"
    Handle = "Handle"
    ISBN = "ISBN"
    ISSN = "ISSN"
    ISTC = "ISTC"
    LISSN = "LISSN"
    LSID = "LSID"
    PMID = "PMID"
    PURL = "PURL"
    URN = "URN"
    w3id = "w3id"
    Other = "Other"


class RelationTypeOptions(str, Enum):
    A_is_part_of_B = "A is part of B"
    A_has_part_B = "A has part B"
    A_is_supplement_to_B = "A is supplement to B"
    A_is_supplemented_by_B = "A is supplemented by B"
    A_is_continued_by_B = "A is continued by B"
    A_continues_B = "A continues B"
    A_is_described_by_B = "A is described by B"
    A_describes_B = "A describes B"
    A_has_version_B = "A has version B"
    A_is_version_of_B = "A is version of B"
    A_is_new_version_of_B = "A is new version of B"
    A_is_previous_version_of_B = "A is previous version of B"
    A_is_variant_form_of_B = "A is variant form of B"
    A_is_original_form_of_B = "A is original form of B"
    A_is_identical_to_B = "A is identical to B"
    A_is_derived_from_B = "A is derived from B"
    A_is_source_of_B = "A is source of B"
    A_is_obsoleted_by_B = "A is obsoleted by B"
    A_obsoletes_B = "A obsoletes B"


class ResourceTypeGeneralOptions(str, Enum):
    Audiovisual = "Audiovisual"
    Book = "Book"
    Book_chapter = "Book chapter"
    Collection = "Collection"
    Computational_notebook = "Computational notebook"
    Conference_paper = "Conference paper"
    Conference_proceeding = "Conference proceeding"
    Data_paper = "Data paper"
    Dataset = "Dataset"
    Dissertation = "Dissertation"
    Event = "Event"
    Image = "Image"
    Interactive_resource = "Interactive resource"
    Journal = "Journal"
    Journal_article = "Journal article"
    Model = "Model"
    Output_management_plan = "Output management plan"
    Peer_review = "Peer review"
    Physical_object = "Physical object"
    Preprint = "Preprint"
    Report = "Report"
    Service = "Service"
    Software = "Software"
    Sound = "Sound"
    Standard = "Standard"
    Text = "Text"
    Workflow = "Workflow"
    Other = "Other"


class CountryNamesFromTheISO3166_1List(ConfiguredBaseModel):
    pass


class LanguageNamesFromTheISO639_1List(ConfiguredBaseModel):
    pass


class TODO(ConfiguredBaseModel):
    identifier: str = Field(..., description="""Identifier (ID) of the related resource assigned on this portal.""")
    relation_type: Optional[RelationTypeOptions] = Field(None, description="""Relationship between the resource being registered (A) and the related resource (B).""")


class Resource(ConfiguredBaseModel):
    collection: Optional[str] = Field(None)
    resource_identifier: str = Field(..., description="""Unique identifier of the resource used for identification within the NFDI4Health.""")
    resource_classification: str = Field(..., description="""Group of items providing Information about classification of the resource within the predefined categories.""")
    resource_titles: List[TODO] = Field(default_factory=list)
    resource_acronyms: Optional[List[TODO]] = Field(default_factory=list)
    resource_description_english: str = Field(..., description="""Group of items with an English description of the resource.""")
    resource_descriptions_non_english: Optional[List[TODO]] = Field(default_factory=list)
    resource_keywords: Optional[List[TODO]] = Field(default_factory=list)
    resource_languages: Optional[List[LanguageNamesFromTheISO639_1List]] = Field(default_factory=list)
    resource_web_page: Optional[str] = Field(None, description="""If existing, a link to the web page directly relevant to the resource.""")
    resource_non_study_details: Optional[str] = Field(None, description="""Group of items applicable only to the resources which are not a study/substudy.""")
    roles: List[TODO] = Field(default_factory=list)
    ids_alternative: Optional[List[TODO]] = Field(default_factory=list)
    ids: Optional[List[TODO]] = Field(default_factory=list)
    ids_nfdi4health: Optional[List[TODO]] = Field(default_factory=list)
    study_design: Optional[str] = Field(None, description="""Group of items providing information about the characteristics of a study or a substudy.""")
    provenance: str = Field(..., description="""Group of items providing information about provenance aspects of the data entry on the portal""")


# Update forward refs
# see https://pydantic-docs.helpmanual.io/usage/postponed_annotations/
CountryNamesFromTheISO3166_1List.update_forward_refs()
LanguageNamesFromTheISO639_1List.update_forward_refs()
TODO.update_forward_refs()
Resource.update_forward_refs()

