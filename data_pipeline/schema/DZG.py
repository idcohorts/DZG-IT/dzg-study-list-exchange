# Auto generated from DZG.yaml by pythongen.py version: 0.0.1
# Generation date: 2024-08-13T10:00:58
# Schema: Study
#
# id: https://example.org/Study
# description:
# license: https://creativecommons.org/publicdomain/zero/1.0/

import dataclasses
import re
from jsonasobj2 import JsonObj, as_dict
from typing import Optional, List, Union, Dict, ClassVar, Any
from dataclasses import dataclass
from datetime import date, datetime
from linkml_runtime.linkml_model.meta import EnumDefinition, PermissibleValue, PvFormulaOptions

from linkml_runtime.utils.slot import Slot
from linkml_runtime.utils.metamodelcore import empty_list, empty_dict, bnode
from linkml_runtime.utils.yamlutils import YAMLRoot, extended_str, extended_float, extended_int
from linkml_runtime.utils.dataclass_extensions_376 import dataclasses_init_fn_with_kwargs
from linkml_runtime.utils.formatutils import camelcase, underscore, sfx
from linkml_runtime.utils.enumerations import EnumDefinitionImpl
from rdflib import Namespace, URIRef
from linkml_runtime.utils.curienamespace import CurieNamespace
from linkml_runtime.linkml_model.types import String

metamodel_version = "1.7.0"
version = None

# Overwrite dataclasses _init_fn to add **kwargs in __init__
dataclasses._init_fn = dataclasses_init_fn_with_kwargs

# Namespaces
STUDY = CurieNamespace('Study', 'https://example.org/Study')
LINKML = CurieNamespace('linkml', 'https://w3id.org/linkml/')
DEFAULT_ = STUDY


# Types

# Class references



class StudyStatus(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = STUDY["StudyStatus"]
    class_class_curie: ClassVar[str] = "Study:StudyStatus"
    class_name: ClassVar[str] = "StudyStatus"
    class_model_uri: ClassVar[URIRef] = STUDY.StudyStatus


@dataclass
class Source(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = STUDY["Source"]
    class_class_curie: ClassVar[str] = "Study:Source"
    class_name: ClassVar[str] = "Source"
    class_model_uri: ClassVar[URIRef] = STUDY.Source

    path: str = None
    date: str = None
    url: str = None

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.path):
            self.MissingRequiredField("path")
        if not isinstance(self.path, str):
            self.path = str(self.path)

        if self._is_empty(self.date):
            self.MissingRequiredField("date")
        if not isinstance(self.date, str):
            self.date = str(self.date)

        if self._is_empty(self.url):
            self.MissingRequiredField("url")
        if not isinstance(self.url, str):
            self.url = str(self.url)

        super().__post_init__(**kwargs)


class SourceRef(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = STUDY["SourceRef"]
    class_class_curie: ClassVar[str] = "Study:SourceRef"
    class_name: ClassVar[str] = "SourceRef"
    class_model_uri: ClassVar[URIRef] = STUDY.SourceRef


@dataclass
class Study(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = STUDY["Study"]
    class_class_curie: ClassVar[str] = "Study:Study"
    class_name: ClassVar[str] = "Study"
    class_model_uri: ClassVar[URIRef] = STUDY.Study

    id: str = None
    title: str = None
    contacts: str = None
    emails: str = None
    status: str = None
    date_start: str = None
    publish: str = None
    inclusion_criteria: str = None
    exclusion_criteria: str = None
    external_links: str = None

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if self._is_empty(self.id):
            self.MissingRequiredField("id")
        if not isinstance(self.id, str):
            self.id = str(self.id)

        if self._is_empty(self.title):
            self.MissingRequiredField("title")
        if not isinstance(self.title, str):
            self.title = str(self.title)

        if self._is_empty(self.contacts):
            self.MissingRequiredField("contacts")
        if not isinstance(self.contacts, str):
            self.contacts = str(self.contacts)

        if self._is_empty(self.emails):
            self.MissingRequiredField("emails")
        if not isinstance(self.emails, str):
            self.emails = str(self.emails)

        if self._is_empty(self.status):
            self.MissingRequiredField("status")
        if not isinstance(self.status, str):
            self.status = str(self.status)

        if self._is_empty(self.date_start):
            self.MissingRequiredField("date_start")
        if not isinstance(self.date_start, str):
            self.date_start = str(self.date_start)

        if self._is_empty(self.publish):
            self.MissingRequiredField("publish")
        if not isinstance(self.publish, str):
            self.publish = str(self.publish)

        if self._is_empty(self.inclusion_criteria):
            self.MissingRequiredField("inclusion_criteria")
        if not isinstance(self.inclusion_criteria, str):
            self.inclusion_criteria = str(self.inclusion_criteria)

        if self._is_empty(self.exclusion_criteria):
            self.MissingRequiredField("exclusion_criteria")
        if not isinstance(self.exclusion_criteria, str):
            self.exclusion_criteria = str(self.exclusion_criteria)

        if self._is_empty(self.external_links):
            self.MissingRequiredField("external_links")
        if not isinstance(self.external_links, str):
            self.external_links = str(self.external_links)

        super().__post_init__(**kwargs)


@dataclass
class Container(YAMLRoot):
    _inherited_slots: ClassVar[List[str]] = []

    class_class_uri: ClassVar[URIRef] = STUDY["Container"]
    class_class_curie: ClassVar[str] = "Study:Container"
    class_name: ClassVar[str] = "Container"
    class_model_uri: ClassVar[URIRef] = STUDY.Container

    Study: Optional[Union[Union[dict, Study], List[Union[dict, Study]]]] = empty_list()

    def __post_init__(self, *_: List[str], **kwargs: Dict[str, Any]):
        if not isinstance(self.Study, list):
            self.Study = [self.Study] if self.Study is not None else []
        self.Study = [v if isinstance(v, Study) else Study(**as_dict(v)) for v in self.Study]

        super().__post_init__(**kwargs)


# Enumerations


# Slots
class slots:
    pass

slots.path = Slot(uri=STUDY.path, name="path", curie=STUDY.curie('path'),
                   model_uri=STUDY.path, domain=None, range=str)

slots.date = Slot(uri=STUDY.date, name="date", curie=STUDY.curie('date'),
                   model_uri=STUDY.date, domain=None, range=str)

slots.url = Slot(uri=STUDY.url, name="url", curie=STUDY.curie('url'),
                   model_uri=STUDY.url, domain=None, range=str)

slots.id = Slot(uri=STUDY.id, name="id", curie=STUDY.curie('id'),
                   model_uri=STUDY.id, domain=None, range=str)

slots.title = Slot(uri=STUDY.title, name="title", curie=STUDY.curie('title'),
                   model_uri=STUDY.title, domain=None, range=str)

slots.contacts = Slot(uri=STUDY.contacts, name="contacts", curie=STUDY.curie('contacts'),
                   model_uri=STUDY.contacts, domain=None, range=str)

slots.emails = Slot(uri=STUDY.emails, name="emails", curie=STUDY.curie('emails'),
                   model_uri=STUDY.emails, domain=None, range=str)

slots.status = Slot(uri=STUDY.status, name="status", curie=STUDY.curie('status'),
                   model_uri=STUDY.status, domain=None, range=str)

slots.date_start = Slot(uri=STUDY.date_start, name="date_start", curie=STUDY.curie('date_start'),
                   model_uri=STUDY.date_start, domain=None, range=str)

slots.publish = Slot(uri=STUDY.publish, name="publish", curie=STUDY.curie('publish'),
                   model_uri=STUDY.publish, domain=None, range=str)

slots.inclusion_criteria = Slot(uri=STUDY.inclusion_criteria, name="inclusion_criteria", curie=STUDY.curie('inclusion_criteria'),
                   model_uri=STUDY.inclusion_criteria, domain=None, range=str)

slots.exclusion_criteria = Slot(uri=STUDY.exclusion_criteria, name="exclusion_criteria", curie=STUDY.curie('exclusion_criteria'),
                   model_uri=STUDY.exclusion_criteria, domain=None, range=str)

slots.external_links = Slot(uri=STUDY.external_links, name="external_links", curie=STUDY.curie('external_links'),
                   model_uri=STUDY.external_links, domain=None, range=str)

slots.container__Study = Slot(uri=STUDY.Study, name="container__Study", curie=STUDY.curie('Study'),
                   model_uri=STUDY.container__Study, domain=None, range=Optional[Union[Union[dict, Study], List[Union[dict, Study]]]])

slots.Source_path = Slot(uri=STUDY.path, name="Source_path", curie=STUDY.curie('path'),
                   model_uri=STUDY.Source_path, domain=Source, range=str)

slots.Source_date = Slot(uri=STUDY.date, name="Source_date", curie=STUDY.curie('date'),
                   model_uri=STUDY.Source_date, domain=Source, range=str)

slots.Source_url = Slot(uri=STUDY.url, name="Source_url", curie=STUDY.curie('url'),
                   model_uri=STUDY.Source_url, domain=Source, range=str)

slots.Study_id = Slot(uri=STUDY.id, name="Study_id", curie=STUDY.curie('id'),
                   model_uri=STUDY.Study_id, domain=Study, range=str)

slots.Study_title = Slot(uri=STUDY.title, name="Study_title", curie=STUDY.curie('title'),
                   model_uri=STUDY.Study_title, domain=Study, range=str)

slots.Study_contacts = Slot(uri=STUDY.contacts, name="Study_contacts", curie=STUDY.curie('contacts'),
                   model_uri=STUDY.Study_contacts, domain=Study, range=str)

slots.Study_emails = Slot(uri=STUDY.emails, name="Study_emails", curie=STUDY.curie('emails'),
                   model_uri=STUDY.Study_emails, domain=Study, range=str)

slots.Study_status = Slot(uri=STUDY.status, name="Study_status", curie=STUDY.curie('status'),
                   model_uri=STUDY.Study_status, domain=Study, range=str)

slots.Study_date_start = Slot(uri=STUDY.date_start, name="Study_date_start", curie=STUDY.curie('date_start'),
                   model_uri=STUDY.Study_date_start, domain=Study, range=str)

slots.Study_publish = Slot(uri=STUDY.publish, name="Study_publish", curie=STUDY.curie('publish'),
                   model_uri=STUDY.Study_publish, domain=Study, range=str)

slots.Study_inclusion_criteria = Slot(uri=STUDY.inclusion_criteria, name="Study_inclusion_criteria", curie=STUDY.curie('inclusion_criteria'),
                   model_uri=STUDY.Study_inclusion_criteria, domain=Study, range=str)

slots.Study_exclusion_criteria = Slot(uri=STUDY.exclusion_criteria, name="Study_exclusion_criteria", curie=STUDY.curie('exclusion_criteria'),
                   model_uri=STUDY.Study_exclusion_criteria, domain=Study, range=str)

slots.Study_external_links = Slot(uri=STUDY.external_links, name="Study_external_links", curie=STUDY.curie('external_links'),
                   model_uri=STUDY.Study_external_links, domain=Study, range=str)
