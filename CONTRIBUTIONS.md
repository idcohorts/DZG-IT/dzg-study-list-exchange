We thank all contributors of this project!

- Markus Katharina Brechtel (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne / University Hospital Frankfurt)
- Andreas Streichardt
- Philipp Kaluza (Ghostroute Consulting) <consult@ghostroute.eu>
- Jannes Höke
- Anna Polovets

# AI Coding Support
The code and documentation was created with the help of AI Coding assistants:
- GitHub Copilot
- OpenAI ChatGPT
